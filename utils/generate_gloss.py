#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function
from __future__ import unicode_literals
from io import open
import argparse, os, glob, sys
from progressbar import ProgressBar, Bar, AdaptiveETA, Percentage
import epitran
import lxml.etree as ET
sys.path.append("../../morph")
from ipamorph_disam_uig import *

def replace_tokens(segments, tokensToReplace):
    
    for segment in segments:
        seg_id = segment.attrib["id"]
        ET.strip_elements(segment, 'TOKEN')
        token_texts = []
        for token in tokensToReplace[seg_id]:
            token_node = ET.Element("TOKEN")
            token_node.text = token["text"]
            token_texts.append(token["text"])
            token_ids = token["id"]
            token_node.attrib["id"] = " ".join(token_ids)
            segment.append(token_node)
        originalText = segment.xpath(".//ORIGINAL_TEXT")[0]
        originalText.text = " ".join(token_texts)

def save_xml(outputDir, basename, tree):
    outputFilename = os.path.join(outputDir, basename)
    with open(outputFilename, 'w', encoding="utf-8") as fout:
        fout.write(ET.tostring(tree, encoding='unicode', pretty_print=True))


def add_to_xml(inputDir, outputDirGloss, outputDirGlish):

    xmlParser = ET.XMLParser(remove_blank_text=True)
    filenames = glob.glob(os.path.join(inputDir, "*.ltf.xml"))
    
    bar = ProgressBar(maxval=len(filenames), widgets=[
        Bar('=', '[', ']'), ' ',
        Percentage(), ' ',
        AdaptiveETA()])
    
    for i, inputFilename in bar(enumerate(filenames)):
        
        basename = os.path.basename(inputFilename)
        
        tree = ET.parse(inputFilename, xmlParser)
        
        segments = tree.xpath(".//SEG")
        segments_gloss = {}
        segments_glish = {}

        for segment in segments:
        
            seg_id = segment.attrib["id"]
            token_texts = []
            
            for token in segment.xpath(".//TOKEN"):
                token_texts.append( {
                    "text": unicode(token.text),
                    "id": unicode(token.attrib["id"])
            })             
            
            best_results = disam_parser_uig.get_best_from_tokens(token_texts, basename)
            
            segments_gloss[seg_id] = [ {"text": t["gloss"], 
                                "id": t["id"]}       
                            for t in best_results["tokens"] ]
            
            segments_glish[seg_id] = [ {"text": t["sentence"], 
                              "id": t["id"]}       
                        for t in best_results["tokens"] ]
                        
        
        replace_tokens(segments, segments_gloss)
        save_xml(outputDirGloss, basename, tree)
        
        replace_tokens(segments, segments_glish)
        save_xml(outputDirGlish, basename, tree)
        
if __name__ == '__main__':
    argparser = argparse.ArgumentParser()
    argparser.add_argument("inputDir", help="An input tab-separated file containing a 'token' row")
    argparser.add_argument("outputDirGloss", help="A tab-separated file to hold the output.")
    argparser.add_argument("outputDirGlish", help="A tab-separated file to hold the output.")
    args = argparser.parse_args()
    add_to_xml(args.inputDir, args.outputDirGloss, args.outputDirGlish)