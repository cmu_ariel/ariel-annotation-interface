#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function
from __future__ import unicode_literals
from io import open
import argparse, os, glob
from progressbar import ProgressBar, Bar, AdaptiveETA, Percentage
import epitran
import lxml.etree as ET

def add_to_xml(inputDir, outputDir, lang):

    transliterate = epitran.Epitran(lang).transliterate
    xmlParser = ET.XMLParser(remove_blank_text=True)
    filenames = glob.glob(os.path.join(inputDir, "*.ltf.xml"))
    
    bar = ProgressBar(maxval=len(filenames), poll=1, widgets=[
        Bar('=', '[', ']'), ' ',
        Percentage(), ' ',
        AdaptiveETA()])
    
    for i, inputFilename in bar(enumerate(filenames)):
        
        basename = os.path.basename(inputFilename)
        
        tree = ET.parse(inputFilename, xmlParser)
        for segment in tree.xpath(".//SEG"):
            token_texts = []
            for token in segment.xpath(".//TOKEN"):
                ipa = transliterate(unicode(token.text))
                token.text = ipa
                token_texts.append(ipa)
            originalText = segment.xpath(".//ORIGINAL_TEXT")[0]
            originalText.text = " ".join(token_texts)
            
        outputFilename = os.path.join(outputDir, basename)
        with open(outputFilename, 'w', encoding="utf-8") as fout:
            fout.write(ET.tostring(tree, encoding='unicode', pretty_print=True))
        
    
if __name__ == '__main__':
    argparser = argparse.ArgumentParser()
    argparser.add_argument("inputDir", help="An input tab-separated file containing a 'token' row")
    argparser.add_argument("outputDir", help="A tab-separated file to hold the output.")
    argparser.add_argument("--lang", help="The language-script pair (e.g., eng-Latn).")
    args = argparser.parse_args()
    add_to_xml(args.inputDir, args.outputDir, args.lang)