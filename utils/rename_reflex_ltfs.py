#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function
from __future__ import unicode_literals
from io import open
import argparse, glob, os, json, sys
import lxml.etree as ET
from progressbar import ProgressBar, Bar, AdaptiveETA, Percentage
sys.path += ["..", "../lib"]
from lib import dbmanager as db
from copy import deepcopy
import unicodedata
    
def go(inputDir, from_lang, to_lang):

    for filename in glob.glob(os.path.join(inputDir, "*.ltf.xml")):
        
        tree = ET.parse(filename)
        
        for doc in tree.xpath(".//DOC"):
            doc.attrib["id"] = doc.attrib["id"].replace(".raw","").replace(from_lang, to_lang).replace(".","_").replace("-","_")
            
        for seg_i, seg in enumerate(tree.xpath(".//SEG")):
            seg.attrib["id"] = "segment-%s" % seg_i
        
            for tok_i, tok in enumerate(seg.xpath(".//TOKEN")):
                tok.attrib["id"] = "token-%s-%s" % (seg_i, tok_i)
                if "pos" not in tok.attrib:
                    foundWord = False
                    foundPunc = False
                    for c in unicode(tok.text):
                        cat = unicodedata.category(c)
                        if cat.startswith("P"):
                            foundPunc = True
                        else:
                            foundWord = True
                    if foundWord and foundPunc:
                        tok.attrib["pos"] = "unknown"
                    elif foundWord:
                        tok.attrib["pos"] = "word"
                    else:
                        tok.attrib["pos"] = "punct"
                    
            
        os.remove(filename)
        
        dirname = os.path.dirname(filename)
        basename = os.path.basename(filename)
        
        basename = basename.replace(from_lang, to_lang).replace(".","_").replace("-","_").replace("_ltf_xml", ".ltf.xml")
        filename = os.path.join(dirname, basename)
        
        with open(filename, "w", encoding="utf-8") as fout:
            fout.write(ET.tostring(tree, encoding="unicode", pretty_print=True))
            
    

if __name__ == '__main__':
    argparser = argparse.ArgumentParser()
    argparser.add_argument("inputDir", help="Directory of files to change (warning: destructive")
    argparser.add_argument("from_lang", help="language code to change")
    argparser.add_argument("to_lang", help="language code to change to")
    args = argparser.parse_args()
    
    go(args.inputDir, args.from_lang, args.to_lang)