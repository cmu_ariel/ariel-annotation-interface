#!/usr/bin/env python
# -*- coding: utf-8 -*-

import smtplib
import datetime
import traceback
from flask_sendmail import Message
    
class Mailer:    
    
    def __init__(self, db, mailer):
        self.db = db
        #self.smtp_address = smtp_address
        #self.smtp_port = smtp_port
        self.mailer = mailer
        
    def send_mail(self, user_id, subject, msg, reply_to="do-not-reply@lor.lti.cs.cmu.edu"):
        ''' Send an email to a registered user '''
        
        # add a topic to the subject line
        subject = "[ARIEL notifications] " + subject
        
        # get recipient's email address
        user_info = self.db.get_user_info(user_id)
        if not user_info or "email" not in user_info or not user_info["email"]:
            return { "success":False, "user_id": user_id,
                     "msg": "User %s has not registered an email address." % user_id}
        user_email = user_info["email"]
            
        try:

            msgObj = Message(subject,
                          sender=reply_to,
                          recipients=[user_email])
            msgObj.html = msg
            self.mailer.send(msgObj)
            # send the message
            #smtpserver = smtplib.SMTP(self.smtp_address, self.smtp_port)
            #smtpserver.ehlo()
            #smtpserver.starttls()
            #smtpserver.ehlo
            #header = 'To:' + user_email + '\n' + 'From: ' + reply_to  + '\n' + 'Subject:' + subject + '\n'
            #msg = header + '\n' + msg + '  \n\n'
            #smtpserver.sendmail(reply_to, user_email, msg)
            #smtpserver.close()
            return { "success":True, "msg": "" }
        except:
            traceback.print_exc()
            return { "success":False, "user_id": user_id, "msg": "Could not send email notification to %s at %s." % (user_id, user_email) }
    
    def send_task_finished_email(self, user_id, annotator_user_id, role_name, query):
        ''' Notify the user who requested a task that the task is complete '''
        subject = "The task you requested is complete"
        time = datetime.datetime.now().strftime("%I:%M%p on %B %d, %Y")
        msg = "Hi, %s.  %s completed %s for document/query %s at %s." % ( user_id, annotator_user_id, role_name, query, time)
        return self.send_mail(user_id, subject, msg)
        
        
    def send_task_notification_email(self, user_id, num_tasks, address, port):
        subject = "New tasks are available"
        link = "http://%s:%s/tasks/?user=%s" % (address, port, user_id)
        link = "<a href=\"%s\">%s</a>" % (link, link)
        if num_tasks > 1:
            msg = "Hi, %s.  There are %s new annotation tasks available for you at %s .  Thanks!" % (user_id, num_tasks, link)
        else:
            msg = "Hi, %s.  There is one new annotation task available for you at %s .  Thanks!" % (user_id, link)
        return self.send_mail(user_id, subject, msg)  

    def send_generic_task_notification_email(self, num_tasks, address, port):
        subject = "New tasks are available"
        failed_users = []
        
        for user in self.db.get_users({ "subscribed": True }):
            user_id = user["user"]
            link = "http://%s:%s/tasks/?user=%s" % (address, port, user_id)       
            link = "<a href=\"%s\">%s</a>" % (link, link)
            if num_tasks > 1:
                msg = "Hi, %s.  There are %s new annotation tasks available at %s .  Thanks!" % (user_id, num_tasks, link)
            else:
                msg = "Hi, %s.  There is one new annotation task available at %s .  Thanks!" % (user_id, link)
            result = self.send_mail(user_id, subject, msg)
            if not result["success"]:
                failed_users.append(result["user_id"])
        
        if failed_users:
            return { "success": False,
                     "msg": "Could not send mail to the following users: %s" % ", ".join(failed_users) }
        
        return { "success": True, "msg": '' }
        
        
            
    def send_subscribe_email(self, user_id, address, port):
        subject = "You are now subscribed to task notifications"
        link = "http://%s:%s/tasks/unsubscribe?user=%s" % (address, port, user_id)
        msg = "To unsubscribe, please visit %s" % link
        return self.send_mail(user_id, subject, msg)        
