#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function
from __future__ import unicode_literals
from io import open
import argparse, glob, os, json, sys
import lxml.etree as ET
from progressbar import ProgressBar, Bar, AdaptiveETA, Percentage
sys.path += ["..", "../lib"]
from lib import dbmanager as db
from copy import deepcopy

"""
Note: files to load cannot have '-' or '.' in their names
Also, combination of filename/doc_id and view_id should be unique
You can't "update" a file by trying to load a new file with the same name and view_id
"""
    
def log_error(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)

        
class TextResourceDB:
        
    def loadResources(self, inputDir):
    
        resources = []
        
        metadataBasename =  'ariel_metadata.json'
        for root, dirnames, filenames in os.walk(inputDir):
            for filename in filenames:
                if filename == metadataBasename:
                    metadataFilename = os.path.join(root, metadataBasename)
                    with open(metadataFilename, "r", encoding="utf-8") as fin:
                        metadata = json.load(fin)
                        globPattern = os.path.join(root, metadata["filename"])
                        for resourceFilename in glob.glob(globPattern):
                            if resourceFilename.endswith(metadataBasename):
                                continue
                        
                            resources.append((resourceFilename, metadata))
                                        
                        # first, remove the existing records for this resource, if there are any
                        #remove_query = {
                        #    "lang_id": metadata["lang_id"],
                        #    "res_id": metadata["res_id"],
                        #}
                        
                        #if "view_id" in metadata:
                        #    remove_query["view_id"] = metadata["view_id"]
                        #db.remove_resourceInfo(remove_query)
                        #db.remove_textitems(remove_query)
        
                        #print("Loading: Language %s, Resource %s"%(metadata["lang_id"], metadata["res_id"]))
                        
        bar = ProgressBar(maxval=len(resources), poll=1, widgets=[
                Bar('=', '[', ']'), ' ',
                Percentage(), ' ',
                AdaptiveETA()])

        for resourceFilename, metadata in bar(resources):
            self.loadResource(resourceFilename, metadata)
        
        db.index_textitems()
        db.index_resource_info()
        
        
    def loadResource(self, filename, metadata):
    
        format = metadata["format"]
        if format == 'ltf':
            self.parseInputLTF(filename, metadata)
        elif format == 'ltf_output':
            self.parseOutputLTF(filename, metadata)
        elif format == 'ltf_alignment':
            self.parseAlignmentLTF(filename, metadata)
        elif format == 'json_keywords':
            self.parseJSONKeywords(filename, metadata)
        elif format in ['laf_ner', 'laf_ner_full', 'laf_ner_simple']:
            self.parseSimpleEntityLAF(filename, metadata)
        elif format == 'tac_ner':
            self.parseNERtac(filename, metadata)
        elif format == 'conll_ner':
            self.parseNERconll(filename, metadata)
        elif format == 'conll_morph':
            self.parseNERconll(filename, metadata, morph=True)
        elif format == 'laf_ner_reflex':
            self.parseReflexEntityLAF(filename, metadata)
        elif format == 'sf_json':
            self.parseSfJson(filename, metadata)
            
            
            
            
    def parseAlignmentLTF(self, filename, metadata):
        align_tree = ET.parse(filename)
        alignments_node = align_tree.xpath("/alignments")[0]
        source_doc_id = alignments_node.attrib["source_id"]
        source_lang = source_doc_id[:3].lower()
        target_doc_id = alignments_node.attrib["translation_id"]
        target_lang = target_doc_id[:3].lower()
    
        for view_id in [ "original", "translation" ]:
        
            lang_id = metadata["lang_id"]
            res_id = metadata["res_id"]
            res_type = metadata["res_type"] if "res_type" in metadata else res_id
            res_type_name = metadata["res_type_name"] if "res_type_name" in metadata else res_type.replace("_"," ").title()
            view_id = view_id
            res_name = metadata["res_name"] if "res_name" in metadata else res_id.replace("_"," ").title()
            view_name = metadata["view_name"] if "view_name" in metadata else view_id.replace("_"," ").title()
            view_priority = 0.0 if view_id == "original" else 10.0
            pinned = metadata["pinned"] if "pinned" in metadata else False
            token_align = view_id == "original"

            resourceInfo = {
                "lang_id": lang_id,
                "res_id": res_id,
                "res_name": res_name,
                "res_type": res_type,
                "res_type_name": res_type_name,
                "view_id": view_id,
                "view_name": view_name,
                "doc_id": target_doc_id,
                "pinned": pinned, 
                "view_priority": view_priority }
            
            if db.get_resourceInfo(resourceInfo):
                continue
                
            db.add_resourceInfo(resourceInfo)            

            
        source_path = os.path.join(os.path.dirname(filename), "..", source_lang, "ltf", source_doc_id + ".ltf.xml")
        target_path = os.path.join(os.path.dirname(filename), "..", target_lang, "ltf", target_doc_id + ".ltf.xml")
        
        source_tree = ET.parse(source_path)
        target_tree = ET.parse(target_path)
        
        source_segments = {}
        target_segments = {}
        
        for source_segment in source_tree.xpath(".//SEG"):
            seg_id = source_segment.xpath("@id")[0]
            source_segments[seg_id] = source_segment
            
        for target_segment in target_tree.xpath(".//SEG"):
            seg_id = target_segment.xpath("@id")[0]
            target_segments[seg_id] = target_segment            
            
        tokens_to_add = []
        
        for alignment in align_tree.xpath(".//alignment"):
            target_seg_ids = alignment.xpath(".//translation/@segments")[0].split()
            source_seg_ids = alignment.xpath(".//source/@segments")[0].split()
            for target_seg_id in target_seg_ids:
                target_seg_offset = int(target_seg_id.split("-")[-1])
                target_seg = target_segments[target_seg_id]
                for target_tok_offset, target_t in enumerate(target_seg.xpath(".//TOKEN")):
                    text = target_t.text if target_t.text else ' '
                    tok_ids = target_t.attrib["id"].split()
                    start_char = int(target_t.attrib["start_char"]) if "start_char" in target_t.attrib else -1
                    end_char = int(target_t.attrib["end_char"]) if "end_char" in target_t.attrib else -1
                    
                    token = { "doc_id": target_doc_id, 
                              "seg_id": target_seg_id, "seg_offset": target_seg_offset, "view_id": "original", 
                              "tok_ids": tok_ids, "tok_offset": target_tok_offset, "start_char": start_char,
                              "end_char": end_char, "text": text, "token_align": True }
                    tokens_to_add.append(token)
                    
                for source_seg_id in source_seg_ids:
                    source_seg = source_segments[source_seg_id]
                    for source_tok_offset, source_t in enumerate(source_seg.xpath(".//TOKEN")):
                        text = source_t.text if source_t.text else ' '
                        tok_ids = source_t.attrib["id"].split()
                        start_char = int(source_t.attrib["start_char"]) if "start_char" in source_t.attrib else -1
                        end_char = int(source_t.attrib["end_char"]) if "end_char" in source_t.attrib else -1
                            
                        token = { "doc_id": target_doc_id, 
                                  "seg_id": target_seg_id, "seg_offset": target_seg_offset, "view_id": "translation", 
                                  "tok_ids": tok_ids, "tok_offset": source_tok_offset, "start_char": start_char,
                                  "end_char": end_char, "text": text, "token_align": False }
                                  
                        tokens_to_add.append(token)
                            
        if tokens_to_add:
            db.add_textitems(tokens_to_add)               
        
                
            
            
    def parseReflexEntityLAF(self, filename, metadata):
        lang_id = metadata["lang_id"]
        res_id = metadata["res_id"]
        res_type = metadata["res_type"] if "res_type" in metadata else res_id
        res_type_name = metadata["res_type_name"] if "res_type_name" in metadata else res_type.replace("_"," ").title()
        view_id = metadata["view_id"]
        res_name = metadata["res_name"] if "res_name" in metadata else res_id.replace("_"," ").title()
        view_name = metadata["view_name"] if "view_name" in metadata else view_id.replace("_"," ").title()
        view_priority = 0.0
        pinned = metadata["pinned"] if "pinned" in metadata else False
        token_align = False       
    
        try:
            tree = ET.parse(filename)
            annotations = []
        except:
            log_error("Cannot parse %s" % filename)
            return
        
        for docNode in tree.xpath(".//DOC"):
            doc_id = docNode.xpath("@id")[0]
            doc_id = doc_id.replace("-","_").replace(".","_")
               
            resourceInfo = {
                "lang_id": lang_id,
                "res_id": res_id,
                "res_name": res_name,
                "res_type": res_type,
                "res_type_name": res_type_name,
                "view_id": view_id,
                "view_name": view_name,
                "doc_id": doc_id }
                
                
            if db.get_resourceInfo(resourceInfo):
                continue
                
            db.add_resourceInfo(resourceInfo)
            
            tokens = db.get_textitems({"doc_id":doc_id, "view_id":"original"})

            for annotationNode in docNode.xpath(".//ANNOTATION"):
                start_char = int(annotationNode.xpath(".//EXTENT/@start_char")[0])
                end_char = int(annotationNode.xpath(".//EXTENT/@end_char")[0])
                text = annotationNode.xpath(".//EXTENT/text()")[0]
                tag = annotationNode.xpath(".//TAG/text()")[0]
                if tag == 'LOC':
                    tag = 'GPE'
                if tag in ['TIME','TTL']:
                    continue

                seg_id = None
                tok_ids = []
                for token in tokens:
                    if token["start_char"] >= start_char and token["end_char"] <= end_char:
                        seg_id = token["seg_id"]
                        for tok_id in token["tok_ids"]:
                            if tok_id not in tok_ids:
                                tok_ids.append(tok_id)
        
                if not tok_ids:
                    print("Warning: Tokens for %s:%s-%s not found!" % (doc_id,start_char,end_char))
                    continue
                
                annotation = {
                    "user": "NERLookup",
                    "annotation_type": "ner",
                    "annotation_label": tag,
                    "doc_id": doc_id,
                    "seg_id": seg_id,
                    "tok_id": tok_ids
                }
        
                annotations.append(annotation)

            if annotations:
                db.add_annotations(annotations)
            
    
    def parseSfJson(self, filename, metadata):
        lang_id = metadata["lang_id"]
        res_id = metadata["res_id"]
        res_type = metadata["res_type"] if "res_type" in metadata else res_id
        res_type_name = metadata["res_type_name"] if "res_type_name" in metadata else res_type.replace("_"," ").title()
        view_id = metadata["view_id"]
        res_name = metadata["res_name"] if "res_name" in metadata else res_id.replace("_"," ").title()
        view_name = metadata["view_name"] if "view_name" in metadata else view_id.replace("_"," ").title()
        view_priority = 0.0
        pinned = metadata["pinned"] if "pinned" in metadata else False
        token_align = False        
        resourceInfo = {
            "lang_id": lang_id,
            "res_id": res_id,
            "res_name": res_name,
            "res_type": res_type,
            "res_type_name": res_type_name,
            "view_id": view_id,
            "view_name": view_name,
            "doc_id": ""
        }
    
        doc_ids_added = {}
        tokens_cache = {}
        with open(filename, 'r', encoding="utf-8") as fin:

            annotations = []
            
            sfs = json.load(fin)
            for sf in sfs:
            
                doc_id = sf["DocumentID"]
                    
                resourceInfo["doc_id"] = doc_id
                if doc_ids_added.get(doc_id, False):
                    continue
                if doc_id not in doc_ids_added:
                    added = db.get_resourceInfo(resourceInfo)
                    if added:
                        doc_ids_added[doc_id] = True
                        continue
                    else:
                        doc_ids_added[doc_id] = False
    
                tokens = tokens_cache.setdefault(doc_id, db.get_textitems({"doc_id":doc_id, "view_id":"original", "seg_id": sf["SegmentID"]}))

                if "Keyword" in sf:
                    for keyword in sf["Keyword"]:
                    
                        start_char = keyword["start_char"]
                        end_char = keyword["end_char"]
                        
                        tok_ids = []
                        for token in tokens:
                            if token["start_char"] >= start_char and token["end_char"] <= end_char:
                                for tok_id in token["tok_ids"]:
                                    if tok_id not in tok_ids:
                                        tok_ids.append(tok_id)
                
                        if not tok_ids:
                            print("Warning: Tokens for %s:%s-%s not found!" % (doc_id,start_char,end_char))
                            continue
                        
                        annotations.append({
                            "doc_id": doc_id,
                            "seg_id": sf["SegmentID"],
                            "tok_id": tok_ids,
                            "annotation_type": "sf",
                            "annotation_label": sf["Type"],
                            "user": "cp1_sf",
                        })
                else:
                    tok_ids = set()
                    for token in tokens:
                        for tok_id in token["tok_ids"]:
                            tok_ids.add(tok_id)
            
                    annotations.append({
                        "doc_id": doc_id,
                        "seg_id": sf["SegmentID"],
                        "tok_id": list(tok_ids),
                        "annotation_type": "sf",
                        "annotation_label": sf["Type"],
                        "user": "cp1_sf",
                    })

            if annotations:
                db.add_annotations(annotations)

            for doc_id, added in doc_ids_added.iteritems():
                if added:
                    continue
                resourceInfo["doc_id"] = doc_id
                db.add_resourceInfo(resourceInfo)
            
    
    def parseSimpleEntityLAF(self, filename, metadata):
        lang_id = metadata["lang_id"]
        res_id = metadata["res_id"]
        res_type = metadata["res_type"] if "res_type" in metadata else res_id
        res_type_name = metadata["res_type_name"] if "res_type_name" in metadata else res_type.replace("_"," ").title()
        view_id = metadata["view_id"]
        res_name = metadata["res_name"] if "res_name" in metadata else res_id.replace("_"," ").title()
        view_name = metadata["view_name"] if "view_name" in metadata else view_id.replace("_"," ").title()
        view_priority = 0.0
        pinned = metadata["pinned"] if "pinned" in metadata else False
        token_align = False       
    
        try:
            tree = ET.parse(filename)
            annotations = []
        except:
            log_error("Cannot parse %s" % filename)
            return
        
        for docNode in tree.xpath(".//DOC"):
            doc_id = docNode.xpath("@id")[0]
            doc_id = doc_id.split(".")[0]
               
            resourceInfo = {
                "lang_id": lang_id,
                "res_id": res_id,
                "res_name": res_name,
                "res_type": res_type,
                "res_type_name": res_type_name,
                "view_id": view_id,
                "view_name": view_name,
                "doc_id": doc_id }
                
                
            if db.get_resourceInfo(resourceInfo):
                continue
                
            db.add_resourceInfo(resourceInfo)
            
            tokens = db.get_textitems({"doc_id":doc_id, "view_id":"original"})

            for annotationNode in docNode.xpath(".//ANNOTATION"):
                category = annotationNode.xpath(".//CATEGORY/text()")
                if category and category[0] != "MENTION":
                    continue
                start_char = int(annotationNode.xpath(".//EXTENT/@start_char")[0])
                end_char = int(annotationNode.xpath(".//EXTENT/@end_char")[0])
                # text = annotationNode.xpath(".//EXTENT/text()")[0]
                tag = annotationNode.xpath(".//TAG/text()")[0]
                if tag not in ["GPE", "LOC", "PER", "ORG"]:
                    continue
                
                seg_id = None
                tok_ids = []
                for token in tokens:
                    if token["start_char"] >= start_char and token["end_char"] <= end_char:
                        seg_id = token["seg_id"]
                        for tok_id in token["tok_ids"]:
                            if tok_id not in tok_ids:
                                tok_ids.append(tok_id)
        
                if not tok_ids:
                    print("Warning: Tokens for %s:%s-%s not found!" % (doc_id,start_char,end_char))
                    continue
                
                annotation = {
                    "user": "ldc",
                    "annotation_type": "ner",
                    "annotation_label": tag,
                    "doc_id": doc_id,
                    "seg_id": seg_id,
                    "tok_id": tok_ids
                }
        
                annotations.append(annotation)

            if annotations:
                db.add_annotations(annotations)
            
    def parseNERtac(self, filename, metadata):
        lang_id = metadata["lang_id"]
        res_id = metadata["res_id"]
        res_type = metadata["res_type"] if "res_type" in metadata else res_id
        res_type_name = metadata["res_type_name"] if "res_type_name" in metadata else res_type.replace("_"," ").title()
        view_id = metadata["view_id"]
        res_name = metadata["res_name"] if "res_name" in metadata else res_id.replace("_"," ").title()
        view_name = metadata["view_name"] if "view_name" in metadata else view_id.replace("_"," ").title()
        view_priority = 0.0
        pinned = metadata["pinned"] if "pinned" in metadata else False
        token_align = False       
    
        resourceInfo = {
            "lang_id": lang_id,
            "res_id": res_id,
            "res_name": res_name,
            "res_type": res_type,
            "res_type_name": res_type_name,
            "view_id": view_id,
            "view_name": view_name,
            "doc_id": ""
        }

        doc_ids_added = {}
        annotations = []
        prev_doc_id = None
        tokens = []
        with open(filename, 'r', encoding="utf-8") as fin:
            lines = [line.rstrip('\n') for line in fin]

            bar = ProgressBar(maxval=len(lines), poll=1, widgets=[
                    Bar('=', '[', ']'), ' ',
                    Percentage(), ' ',
                    AdaptiveETA()])

            for line in bar(lines):
                fields = line.split('\t')
                doc_id, offsets = fields[3].split(':')
                start_char, end_char = tuple(map(int, offsets.split('-')))
                tag = fields[5]
                if tag not in ["GPE", "LOC", "PER", "ORG"]:
                    continue

                resourceInfo["doc_id"] = doc_id
                if doc_ids_added.get(doc_id, False):
                    continue
                if doc_id not in doc_ids_added:
                    added = db.get_resourceInfo(resourceInfo)
                    if added:
                        doc_ids_added[doc_id] = True
                        continue
                    else:
                        doc_ids_added[doc_id] = False
                
                # assumes the file is sorted by docID
                if doc_id != prev_doc_id:
                    tokens = db.get_textitems({"doc_id":doc_id, "view_id":"original"})
                    prev_doc_id = doc_id
                seg_id = None
                tok_ids = []
                for token in tokens:
                    if token["start_char"] >= start_char and token["end_char"] <= end_char:
                        seg_id = token["seg_id"]
                        for tok_id in token["tok_ids"]:
                            if tok_id not in tok_ids:
                                tok_ids.append(tok_id)
        
                if not tok_ids:
                    print("Warning: Tokens for %s:%s-%s not found!" % (doc_id,start_char,end_char))
                    continue
                
                annotation = {
                    "user": metadata.get("user", "aditi2"),
                    "annotation_type": "ner",
                    "annotation_label": tag,
                    "doc_id": doc_id,
                    "seg_id": seg_id,
                    "tok_id": tok_ids
                }
                annotations.append(annotation)

            if annotations:
                db.add_annotations(annotations)

            for doc_id, added in doc_ids_added.iteritems():
                if added:
                    continue
                resourceInfo["doc_id"] = doc_id
                db.add_resourceInfo(resourceInfo)


    def parseNERconll(self, filename, metadata, morph=False):
        lang_id = metadata["lang_id"]
        res_id = metadata["res_id"]
        res_type = metadata["res_type"] if "res_type" in metadata else res_id
        res_type_name = metadata["res_type_name"] if "res_type_name" in metadata else res_type.replace("_"," ").title()
        view_id = metadata["view_id"]
        res_name = metadata["res_name"] if "res_name" in metadata else res_id.replace("_"," ").title()
        view_name = metadata["view_name"] if "view_name" in metadata else view_id.replace("_"," ").title()
        view_priority = metadata["view_priority"] if "view_priority" in metadata else 0.0
        pinned = metadata["pinned"] if "pinned" in metadata else False
        token_align = metadata["token_align"] if "token_align" in metadata else True

        doc_id = os.path.basename(filename).replace('.', '')
        # print("Loading NER annotations for %s" % doc_id)
    
        resourceInfo = {
            "lang_id": lang_id,
            "res_id": res_id,
            "res_name": res_name,
            "res_type": res_type,
            "res_type_name": res_type_name,
            "view_id": view_id,
            "view_name": view_name,
            "doc_id": doc_id,
            "pinned": pinned, 
            "view_priority": view_priority
        }

        if db.get_resourceInfo(resourceInfo):
            return
        db.add_resourceInfo(resourceInfo)
        
        annotations = []
        tokensToAdd = []
        tok_offset, seg_offset, char_count = 0, 0, 0

        with open(filename, 'r', encoding="utf-8") as fin:
            lines = [line.strip() for line in fin]

            # bar = ProgressBar(maxval=len(lines), poll=1, widgets=[
                    # Bar('=', '[', ']'), ' ',
                    # Percentage(), ' ',
                    # AdaptiveETA()])

            tag = ""
            tok_ids = []
            # for line in bar(lines):
            for line in lines:
                if not line:
                    seg_offset += 1
                    tok_offset = 0
                    continue

                # fields = line.split('\t')
                fields = line.split()
                text, iob_tag = fields[0], fields[1]
                if iob_tag[:2] not in ["B-", "I-"] or iob_tag[2:] not in ["GPE", "LOC", "PER", "ORG", "UNK"]:
                    iob_tag = "O"

                if tok_ids and iob_tag[0] in ['O', 'B']:
                    annotation = {
                        "user": metadata.get("user", "aditi"),
                        "annotation_type": "ner",
                        "annotation_label": tag,
                        "doc_id": doc_id,
                        "seg_id": seg_id,
                        "tok_id": tok_ids
                    }
                    if morph:
                        annotation["annotation_type"] = "morph"
                    annotations.append(annotation)
                    tag = ""
                    tok_ids = []

                seg_id = "segment-%d" % seg_offset
                tok_id = "token-%d-%d" % (seg_offset, tok_offset)
                token = { "doc_id": doc_id,
                          "seg_id": seg_id, "seg_offset": seg_offset, "view_id": view_id,
                          "tok_ids": [tok_id], "tok_offset": tok_offset, "start_char": char_count + 1,
                          "end_char": char_count + len(text), "text": text, "token_align": token_align }
                tokensToAdd.append(token)
                tok_offset += 1
                char_count += len(text) + 1

                if iob_tag[0] in ['I', 'B']:
                    if iob_tag[0] == 'B':
                        tag = iob_tag[2:]
                    elif iob_tag[0] == 'I':
                        assert(iob_tag[2:] == tag)
                    tok_ids.append(tok_id)

            if tok_ids:
                annotation = {
                    "user": metadata.get("user", "default_user"),
                    "annotation_type": "ner",
                    "annotation_label": tag,
                    "doc_id": doc_id,
                    "seg_id": seg_id,
                    "tok_id": tok_ids
                }
                if morph:
                    annotation["annotation_type"] = "morph"
                annotations.append(annotation)

            # print("No. of segments: %d" % seg_offset)
            # print("No. of tokens: %d" % len(tokensToAdd))
            # print("No. of annotations: %d" % len(annotations))

        if tokensToAdd:
            db.add_textitems(tokensToAdd)
        if annotations:
            db.add_annotations(annotations)
                

    def parseJSONKeywords(self, filename, metadata):
        lang_id = metadata["lang_id"]
        res_id = metadata["res_id"]
        res_type = metadata["res_type"] if "res_type" in metadata else res_id
        res_type_name = metadata["res_type_name"] if "res_type_name" in metadata else res_type.replace("_"," ").title()
        view_id = metadata["view_id"]
        res_name = metadata["res_name"] if "res_name" in metadata else res_id.replace("_"," ").title()
        view_name = metadata["view_name"] if "view_name" in metadata else view_id.replace("_"," ").title()
        view_priority = 0.0
        pinned = metadata["pinned"] if "pinned" in metadata else False
        token_align = False
        
        objs = []
        with open(filename, 'r', encoding="utf-8") as fin:

            obj = json.load(fin)
            resourceInfo = {
                "lang_id": lang_id,
                "res_id": res_id,
                "res_name": res_name,
                "res_type": res_type,
                "res_type_name": res_type_name,
                "view_id": view_id,
                "view_name": view_name,
                "doc_id": obj["doc_id"] }
                
            if db.get_resourceInfo(resourceInfo):
                return
                
            db.add_resourceInfo(resourceInfo)
        
            objs.append(obj)
            
        db.add_document_keywords(objs)
    
    
    def parseInputLTF(self, filename, metadata):
        lang_id = metadata["lang_id"]
        res_id = metadata["res_id"]
        res_type = metadata["res_type"] if "res_type" in metadata else res_id
        res_type_name = metadata["res_type_name"] if "res_type_name" in metadata else res_type.replace("_"," ").title()
        view_id = metadata["view_id"]
        res_name = metadata["res_name"] if "res_name" in metadata else res_id.replace("_"," ").title()
        view_name = metadata["view_name"] if "view_name" in metadata else view_id.replace("_"," ").title()
        view_priority = metadata["view_priority"] if "view_priority" in metadata else 5.0
        pinned = metadata["pinned"] if "pinned" in metadata else False
        token_align = metadata["token_align"] if "token_align" in metadata else True

        
        tree = ET.parse(filename)
        doc_id = tree.xpath(".//DOC/@id")[0]
        doc_id = doc_id.split(".")[0]
        
        resourceInfo = {
            "lang_id": lang_id,
            "res_id": res_id,
            "res_name": res_name,
            "res_type": res_type,
            "res_type_name": res_type_name,
            "view_id": view_id,
            "view_name": view_name,
            "doc_id": doc_id,
            "pinned": pinned, 
            "view_priority": view_priority }
            
        if db.get_resourceInfo(resourceInfo):
            return
            
        db.add_resourceInfo(resourceInfo)
        
        documentResult = deepcopy(resourceInfo)
        documentResult["segments"] = []
        
        tokensToAdd = []
        
        for seg_offset, segment in enumerate(tree.xpath(".//SEG")):
            seg_id = segment.attrib["id"]
            
            segmentResult = { "seg_id": seg_id, "tokens": [] }
            
            tokens = segment.xpath(".//TOKEN")
            
            for tok_offset, t in enumerate(tokens):
                text = t.text if t.text else ' '
                tok_ids = t.attrib["id"].split()
                #if not token_align:
                #    tok_ids = [ ("%s-%s" % (id, view_id)) for id in tok_ids ]
                start_char = int(t.attrib["start_char"]) if "start_char" in t.attrib else -1
                end_char = int(t.attrib["end_char"]) if "end_char" in t.attrib else -1
                
                token = { "doc_id": doc_id, 
                          "seg_id": seg_id, "seg_offset": seg_offset, "view_id": view_id, 
                          "tok_ids": tok_ids, "tok_offset": tok_offset, "start_char": start_char,
                          "end_char": end_char, "text": text, "token_align": token_align }
                tokensToAdd.append(token)
                
                tokenResult = {
                    "tok_ids": tok_ids, 
                    "tok_offset": tok_offset, 
                    "start_char": start_char,
                    "end_char": end_char,
                    "text": text,
                    "token_align": token_align
                }
                segmentResult["tokens"].append(tokenResult)
        
            documentResult["segments"].append(segmentResult)
            
        if tokensToAdd:
            db.add_textitems(tokensToAdd)
    
        #db.add_document(documentResult)
        
        
    def parseOutputLTF(self, filename, metadata):
        lang_id = metadata["lang_id"]
        res_id = metadata["res_id"]
        res_type = metadata["res_type"] if "res_type" in metadata else res_id
        res_type_name = metadata["res_type_name"] if "res_type_name" in metadata else res_type.replace("_"," ").title()
        view_id = metadata["view_id"]
        res_name = metadata["res_name"] if "res_name" in metadata else res_id.replace("_"," ").title()
        view_name = metadata["view_name"] if "view_name" in metadata else view_id.replace("_"," ").title()
        view_priority = metadata["view_priority"] if "view_priority" in metadata else 5.0
        pinned = metadata["pinned"] if "pinned" in metadata else False


        tree = ET.parse(filename)
        doc_id = tree.xpath(".//DOC/@id")[0]
        doc_id = doc_id.split(".")[0]
        
        resourceInfo = {
            "lang_id": lang_id,
            "res_id": res_id,
            "res_name": res_name,
            "res_type": res_type,
            "res_type_name": res_type_name,
            "view_id": view_id,
            "view_name": view_name,
            "doc_id": doc_id,
            "pinned": pinned, 
            "view_priority": view_priority
        }
        
        
        if db.get_resourceInfo(resourceInfo):
            return
            
        db.add_resourceInfo(resourceInfo)
        
        tokensToAdd = []
        
        for seg_offset, segment in enumerate(tree.xpath(".//SEG")):
            seg_id = segment.attrib["id"]
        
            text = segment.text if segment.text else ' '
            tok_ids = [ "token-0-all-%s" % view_id ]
            tok_offset = 0
            start_char = 0
            end_char = len(text)
            
            token = { "lang_id": lang_id, "res_id": res_id, 
                      "view_id": view_id, "doc_id": doc_id, 
                      "seg_id": seg_id, "seg_offset": seg_offset, "tok_ids": tok_ids, "tok_offset": tok_offset, "start_char": start_char,
                      "end_char": end_char, "text": text, "token_align": false }
            tokensToAdd.append(token)
        
        if tokensToAdd:
            db.add_textitems(tokensToAdd)
    
    def unloadResource(self, res_id):
        db.remove_resource({"res_id":res_id})
    
    
if __name__ == '__main__':
    argparser = argparse.ArgumentParser()
    argparser.add_argument("inputDir", help="Root of tree to walk for resource files")
    argparser.add_argument("--db_ip", default="127.0.0.1", help="database IP")
    argparser.add_argument("--db_port", type=int, default=27017, help="database port")
    argparser.add_argument("--db_name", default="lorelei_viewer", help="database name")
    argparser.add_argument("--remove", default="", help="resource ID for deletion")
    args = argparser.parse_args()
    
    
    db.init(args.db_ip, args.db_port, args.db_name)
    textDB = TextResourceDB()
    
    if args.remove:
        textDB.unloadResource(args.remove)
    else:
        textDB.loadResources(args.inputDir)        
    
    
