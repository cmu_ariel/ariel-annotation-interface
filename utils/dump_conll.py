#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function
from __future__ import unicode_literals
from io import open
import argparse, glob, os, json, sys
import lxml.etree as ET
sys.path += ["..", "../lib"]
from lib import dbmanager as db
from collections import Counter
import re

from progressbar import ProgressBar, Bar, AdaptiveETA, Percentage


DEFAULT_USERS = ["ldc", "default", "none", "lookup_ner", ""]
    
def log_error(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)

def ensure_dir(mydir):
    if not os.path.isdir(mydir):
        os.makedirs(mydir)
    
def get_doc_ids(res_id):
    return set(r["doc_id"] for r in db.resourceInfoDB.find({"res_id":res_id}))

def tokens_identical(token1, token2):
    uniq_index = {"text":1, "view_id":1, "start_char":1, "tok_offset":1, "end_char":1, "seg_offset":1, "tok_ids":1, "seg_id":1, "doc_id":1}
    for key in uniq_index:
        if key not in token1 or key not in token2:
            return False
        if token1[key] != token2[key]:
            return False
    return True

def dump_ner(res_id, outputDir, include_users, auto_users, morph=False):

    doc_ids = get_doc_ids(res_id)

    bar = ProgressBar(maxval=len(doc_ids), poll=1, widgets=[
        Bar('=', '[', ']'), ' ',
        Percentage(), ' ',
        AdaptiveETA()])


    for doc_index, doc_id in enumerate(bar(doc_ids)):
    
        usersFoundInDoc = set()
        tsv_rows = []
        tokens = db.get_textitems({"doc_id":doc_id, "view_id":"original"})
        tokens.sort(key=lambda x: (x["seg_offset"], x["tok_offset"]))

        ner_annos = {}
        for ann in list(db.annotations.find({"doc_id":doc_id, "annotation_type":"morph" if morph else "ner"})):
            if include_users and ann["user"] not in (auto_users | include_users):
                continue
            for tok_id in ann["tok_id"]:
                # if there are overlapping annotations, auto users shouldn't overwrite real ones
                if (ann["seg_id"], tok_id) in ner_annos and ann["user"] in auto_users:
                        continue
                prefix = "B-" if tok_id == ann["tok_id"][0] else "I-"
                ner_annos[(ann["seg_id"], tok_id)] = {
                    "user": ann["user"],
                    "timestamp": ann["_id"].generation_time,
                    "tag": prefix + ann["annotation_label"]
                }
        
        prev_token = {"seg_offset": -1}
        for token in tokens:
            if tokens_identical(token, prev_token):
                # temporary fix to a bug that allows multiple copies of the same token in the db
                continue
            if token["seg_offset"] != prev_token["seg_offset"]:
                tsv_rows.append([])
            ann = ner_annos.get((token["seg_id"], token["tok_ids"][0]), {"tag": "O", "user": "", "timestamp": ""})
            usersFoundInDoc.add(ann["user"])
            tsv_rows.append([token["text"], ann["tag"], ann["user"], unicode(ann["timestamp"])])
            prev_token = token

        if usersFoundInDoc <= auto_users:
            continue
            
        if include_users and not (usersFoundInDoc & include_users):
            continue

        outputFilename = os.path.join(outputDir, "%s.conll" % doc_id)
        with open(outputFilename, "w", encoding="utf-8") as fout:
            for row in tsv_rows:
                fout.write("\t".join(row) + "\n")
                

if __name__ == '__main__':
    argparser = argparse.ArgumentParser()
    argparser.add_argument("outputDir", help="Directory to store the outputs")
    argparser.add_argument("resourceID", help="ID of the resource to extract EDL annotations for")
    argparser.add_argument("--db_ip", default="127.0.0.1", help="database IP")
    argparser.add_argument("--db_port", type=int, default=27017, help="database port")
    argparser.add_argument("--db_name", default="lorelei_viewer", help="database name")
    argparser.add_argument("--include_users", nargs='+', default=[], help="export docs annotated by these users")
    argparser.add_argument("--auto_users", nargs='+', default=DEFAULT_USERS, help="ignore docs with only auto-users")
    argparser.add_argument("--morph", action="store_true")
    args = argparser.parse_args()
    
    db.init(args.db_ip, args.db_port, args.db_name)
    ensure_dir(args.outputDir)

    dump_ner(args.resourceID, args.outputDir, set(args.include_users), set(args.auto_users + DEFAULT_USERS), morph=args.morph)
