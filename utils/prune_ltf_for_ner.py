#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function
from __future__ import unicode_literals
from io import open
import argparse, glob, os, json, sys
import lxml.etree as ET
sys.path += ["..", "../lib"]

def prune(lafDir, ltfDir):

    #lastSegs = {}

    lafFilenames = glob.glob(os.path.join(lafDir, "*.laf.xml"))
    for lafFilename in lafFilenames:
        lafRoot = ET.parse(lafFilename)
        doc_id = lafRoot.xpath(".//DOC/@id")[0]
        max_seg = 0
        for seg_id in lafRoot.xpath(".//ANNOTATION/@seg_id"):
            seg_id = int(seg_id.split("-")[-1])
            max_seg = max(max_seg, seg_id)
        #lastSegs[doc_id] = max_seg
        
        ltfFilename = os.path.join(ltfDir, doc_id + ".ltf.xml")
        if not os.path.exists(ltfFilename):
            print("Warning: Cannot find LTF file %s" % ltfFilename)
            continue
            
        ltfRoot = ET.parse(ltfFilename)
        for segNode in ltfRoot.xpath(".//SEG"):
            seg_id = segNode.xpath("@id")[0]
            seg_id = int(seg_id.split("-")[-1])
            if seg_id > max_seg:
                print("Pruning segment %s from %s" % (seg_id, doc_id))
                segNode.getparent().remove(segNode)
                
        outputFilename = os.path.join(lafDir, doc_id + ".ltf.xml")
        with open(outputFilename, "w", encoding="utf-8") as fout:
            fout.write(ET.tostring(ltfRoot, encoding="unicode", pretty_print=True))
        
        
        
            
        


if __name__ == '__main__':
    argparser = argparse.ArgumentParser()
    argparser.add_argument("lafDir", help="Directory containing LAFs, will also store the outputs")
    argparser.add_argument("ltfDir", help="Directory containing LTFs")
    args = argparser.parse_args()
    prune(args.lafDir, args.ltfDir)
