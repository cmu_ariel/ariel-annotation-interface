#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse, os, sys
sys.path += ["..", "../lib"]
from lib import dbmanager as db

if __name__ == '__main__':
    argparser = argparse.ArgumentParser()
    argparser.add_argument("filelist", help="file containing docIDs (one per line)")
    argparser.add_argument("assign_to", help="username to assign to")
    argparser.add_argument("assigned_by", help="assigning user")
    argparser.add_argument("annotator_role", help="ner_annotator etc.")
    argparser.add_argument("lang_id", help="language ID")
    argparser.add_argument("res_id", help="resource ID")
    argparser.add_argument("--sandboxed", action="store_true", help="assigning user")
    argparser.add_argument("--db_ip", default="127.0.0.1", help="database IP")
    argparser.add_argument("--db_port", type=int, default=27017, help="database port")
    argparser.add_argument("--db_name", default="lorelei_viewer", help="database name")
    args = argparser.parse_args()

    db.init(args.db_ip, args.db_port, args.db_name)
    
    with open(args.filelist) as f:
        docIDs = [l.strip() for l in f.readlines() if l.strip()]

    for docID in docIDs:
        db.add_task({
            "user": args.assign_to,
            "assigned_by": args.assigned_by,
            "role": args.annotator_role,
            "lang_id": args.lang_id,
            "res_id": args.res_id,
            "items": [{"doc": docID}],
            "sandboxed": args.sandboxed
        })
