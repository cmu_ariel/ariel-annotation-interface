#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse, os, glob, sys
from progressbar import ProgressBar, Bar, AdaptiveETA, Percentage
import lxml.etree as ET
sinmorph_path="/home/zsheikh/LoReHLT18/shared/sinmorph-v4/"
sys.path.append(sinmorph_path)
import sinmorph

analyzer = sinmorph.SinMorph(fst = sinmorph_path + 'sinmorph.fst')



def ensure_dir(mydir):
    if not os.path.isdir(mydir):
        os.makedirs(mydir)


def preload_lemmas(inputDir, outputDir):

    print("Preloading lemmas...")
    tokens = set()
    xmlParser = ET.XMLParser(remove_blank_text=True)
    filenames = glob.glob(os.path.join(inputDir, "*.ltf.xml"))
    
    bar = ProgressBar(maxval=len(filenames), poll=1, widgets=[
        Bar('=', '[', ']'), ' ',
        Percentage(), ' ',
        AdaptiveETA()])
    
    for i, inputFilename in bar(enumerate(filenames)):
        
        basename = os.path.basename(inputFilename)
        
        tree = ET.parse(inputFilename, xmlParser)
        for segment in tree.xpath(".//SEG"):
            token_texts = []
            for token in segment.xpath(".//TOKEN"):
                # text = unicode(token.text)
                text = token.text
                if not text:
                    continue
                for subtext in text.split():
                    tokens.add(subtext)
    tokens_list = list(tokens)
    lemmas_list = analyzer.get_lemmas(tokens_list)
    return dict(zip(tokens_list, lemmas_list))
            
        
def add_to_xml(inputDir, outputDir, cached_lemmas):

    print("Writing output files...")
    xmlParser = ET.XMLParser(remove_blank_text=True)
    filenames = glob.glob(os.path.join(inputDir, "*.ltf.xml"))
    
    bar = ProgressBar(maxval=len(filenames), poll=1, widgets=[
        Bar('=', '[', ']'), ' ',
        Percentage(), ' ',
        AdaptiveETA()])
    
    for i, inputFilename in bar(enumerate(filenames)):
        
        basename = os.path.basename(inputFilename)
        
        tree = ET.parse(inputFilename, xmlParser)
        for segment in tree.xpath(".//SEG"):
            token_texts = []
            for token in segment.xpath(".//TOKEN"):
                # text = unicode(token.text)
                text = token.text
                if not text:
                    print("Warning: %s-%s-%s is empty: %s" % (basename, segment.id, token.id, text))
                    continue
                lemmas = []
                for subtext in text.split():
                    lemmas.append(cached_lemmas[subtext])
                lemma = " ".join(lemmas)
                token.text = lemma
                token_texts.append(lemma)
            originalText = segment.xpath(".//ORIGINAL_TEXT")
            if originalText:
                originalText[0].text = " ".join(token_texts)
            
        outputFilename = os.path.join(outputDir, basename)
        if (os.path.exists(outputFilename)):
            sys.exit("Can't write to %s, it already exists!" % outputFilename)
        with open(outputFilename, 'w', encoding="utf-8") as fout:
            fout.write(ET.tostring(tree, encoding='unicode', pretty_print=True))
        
    
if __name__ == '__main__':
    argparser = argparse.ArgumentParser(description="convert LTF files to their lemmatized versions")
    argparser.add_argument("inputDir", help="input LTF dir")
    argparser.add_argument("outputDir", help="output LTF dir")
    args = argparser.parse_args()
    ensure_dir(args.outputDir)
    cached_lemmas = preload_lemmas(args.inputDir, args.outputDir)
    add_to_xml(args.inputDir, args.outputDir, cached_lemmas)
