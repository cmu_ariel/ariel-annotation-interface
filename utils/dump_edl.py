#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function
from __future__ import unicode_literals
from io import open
import argparse, glob, os, json, sys
import lxml.etree as ET
from progressbar import ProgressBar, Bar, AdaptiveETA, Percentage
sys.path += ["..", "../lib"]
from lib import dbmanager as db

    
def log_error(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)

def ensure_dir(mydir):
    if not os.path.isdir(mydir):
        os.makedirs(mydir)
        
    
def get_doc_ids(res_id):
    return set(r["doc_id"] for r in db.resourceInfoDB.find({"res_id":res_id}))
    
def dump_edl(res_id, outputDir):
    for doc_id in get_doc_ids(res_id):
    
        ner_annos = {}
        for ner_anno in db.annotations.find({"doc_id":doc_id, "annotation_type":"ner"}):
            result = (ner_anno["doc_id"], ner_anno["seg_id"], " ".join(ner_anno["tok_id"]))
            ner_annos[result] = ner_anno["annotation_label"]
        
        results = []
        for edl_anno in db.entityLinks.find({"doc_id":doc_id}):
            wanted_keys = [ "doc_id", "seg_id", "tok_id", "entity_link" ]
            result = { k:edl_anno[k] for k in edl_anno if k in wanted_keys } 
            anno_for_comparison = ( result["doc_id"], result["seg_id"], " ".join(result["tok_id"]) )
            if anno_for_comparison not in ner_annos:
                print("Warning: NER/EDL mismatch at %s, %s, %s" % anno_for_comparison)
                continue
            result["type"] = ner_annos[anno_for_comparison]
            
            # find the actual tokens, for convenience
            token_texts = []
            tokens = db.get_textitems({"doc_id":doc_id, "seg_id":edl_anno["seg_id"], "tok_ids":edl_anno["tok_id"], "view_id":"original"})
            tokens.sort(key=lambda x:x["tok_offset"])
            for token in tokens:
                token_texts.append(token["text"])
            result["text"] = " ".join(token_texts)
            results.append(result)
         
        if not results:
            continue
        outputFilename = os.path.join(outputDir, "%s.edl.json" % doc_id)
        with open(outputFilename, "w", encoding="utf-8") as fout:
            text = json.dumps(results, ensure_ascii=False, indent=4)
            fout.write(unicode(text))
            

if __name__ == '__main__':
    argparser = argparse.ArgumentParser()
    argparser.add_argument("outputDir", help="Directory to store the outputs")
    argparser.add_argument("resourceID", help="ID of the resource to extract EDL annotations for")
    argparser.add_argument("--db_ip", default="127.0.0.1", help="database IP")
    argparser.add_argument("--db_port", type=int, default=27017, help="database port")
    argparser.add_argument("--db_name", default="lorelei_viewer", help="database name")
    args = argparser.parse_args()
    
    db.init(args.db_ip, args.db_port, args.db_name)
    ensure_dir(args.outputDir)
    
    dump_edl(args.resourceID, args.outputDir)
