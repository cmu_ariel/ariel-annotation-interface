#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function
from __future__ import unicode_literals
from io import open
import argparse, os, glob, sys
from progressbar import ProgressBar, Bar, AdaptiveETA, Percentage
sys.path.append("/home/data/LoReHLT17/internal/Morph/Orm/v2")
from orm_morph import best_parse
import lxml.etree as ET

def ensure_dir(mydir):
    if not os.path.isdir(mydir):
        os.makedirs(mydir)
        
def add_to_xml(inputDir, outputDir):

    xmlParser = ET.XMLParser(remove_blank_text=True)
    filenames = glob.glob(os.path.join(inputDir, "*.ltf.xml"))
    
    bar = ProgressBar(maxval=len(filenames), poll=1, widgets=[
        Bar('=', '[', ']'), ' ',
        Percentage(), ' ',
        AdaptiveETA()])
    
    for i, inputFilename in bar(enumerate(filenames)):
        
        basename = os.path.basename(inputFilename)
        
        tree = ET.parse(inputFilename, xmlParser)
        for segment in tree.xpath(".//SEG"):
            token_texts = []
            for token in segment.xpath(".//TOKEN"):
                text = unicode(token.text)
                if not text:
                    print("Warning: %s-%s-%s is empty: %s" % (basename, segment.id, token.id, text))
                    continue
                lemmas = []
                for subtext in text.split():
                    lemmas.append(best_parse(subtext, "natural"))
                lemma = " ".join(lemmas)
                token.text = lemma
                token_texts.append(lemma)
            originalText = segment.xpath(".//ORIGINAL_TEXT")
            if originalText:
                originalText[0].text = " ".join(token_texts)
            
        outputFilename = os.path.join(outputDir, basename)
        with open(outputFilename, 'w', encoding="utf-8") as fout:
            fout.write(ET.tostring(tree, encoding='unicode', pretty_print=True))
        
    
if __name__ == '__main__':
    argparser = argparse.ArgumentParser()
    argparser.add_argument("inputDir", help="An input tab-separated file containing a 'token' row")
    argparser.add_argument("outputDir", help="A tab-separated file to hold the output.")
    args = argparser.parse_args()
    ensure_dir(args.outputDir)
    add_to_xml(args.inputDir, args.outputDir)