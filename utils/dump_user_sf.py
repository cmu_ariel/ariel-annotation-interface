#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from io import open
import argparse, glob, os, json, sys
import lxml.etree as ET
sys.path += ["..", "../lib"]
from lib import dbmanager as db
from copy import deepcopy


def dump_user_sf(user):
    final_sfs = []
    mentions, canonical_mentions = {}, {}
    # get all SF type and status annotations from db
    sf_annos = {"sf": {}, "sf_need": {}, "sf_relief": {}, "sf_urgency": {}}
    for ann_type in sf_annos:
        for sf in db.annotations.find({"annotation_type": ann_type, "user": user}):
            tok_span = (sf["doc_id"], sf["seg_id"], " ".join(sf["tok_id"]))
            sf_annos[ann_type][tok_span] = sf["annotation_label"]
    print len(sf_annos["sf"])
    # go through each SF "type" annotation
    for tok_span in sf_annos["sf"]:
        doc_id, seg_id, tok_id = tok_span
        tok_id = tok_id.split()
        # reconstruct text from token IDs
        tokens = [db.textitems.find_one({
            "doc_id": doc_id, 
            "seg_id": seg_id, 
            "view_id": "original", 
            "tok_ids": [token]
        })["text"] for token in tok_id]
        # create base SF without PlaceMention
        sf_base = {
            "DocumentID": doc_id,
            "SegmentID": seg_id,
            "Type": sf_annos["sf"][tok_span],
            "Text": " ".join(tokens),
            "Status": {
                # WARNING: default values used if status annotations not found
                "Need": sf_annos["sf_need"].get(tok_span, "current"),
                "Relief": sf_annos["sf_relief"].get(tok_span, "insufficient"),
                "Urgent": sf_annos["sf_urgency"].get(tok_span, "urgent")
            }
        }
        # for each location linked to the token span, create a new SF with PlaceMention
        sf_locs = db.annotations.find({"annotation_type" : "sf_location", "doc_id": doc_id, "seg_id": seg_id, "tok_id": tok_id})
        if sf_locs.count() == 0:
            final_sfs.append(sf_base)
        for sf_loc in sf_locs:
            sf = deepcopy(sf_base)
            entity_key = (doc_id, sf_loc["entity_link"])
            # go through all mentions of the linked location entity in the same document
            for mention in db.annotations.find({"annotation_type": "edl", "entity_link": sf_loc["entity_link"], "doc_id": doc_id}):
                # get start, end character offsets of token span
                start = db.textitems.find_one({
                    "doc_id": doc_id,
                    "seg_id": mention["seg_id"],
                    "view_id": "original",
                    "tok_ids": [mention["tok_id"][0]]
                })["start_char"]
                end = db.textitems.find_one({
                    "doc_id": doc_id,
                    "seg_id": mention["seg_id"],
                    "view_id": "original",
                    "tok_ids": [mention["tok_id"][-1]]
                })["end_char"]
                # arbitrarily choose the first mention as the canonical form of the entity
                if entity_key not in canonical_mentions:
                    # get the entity type (GPE or LOC) from its NER annotation
                    ner_ann = db.annotations.find_one({
                        "annotation_type": "ner",
                        "doc_id": doc_id,
                        "seg_id": mention["seg_id"],
                        "tok_id": mention["tok_id"],
                        "user": user
                    })
                    entity_type = ner_ann["annotation_label"]
                    canonical_mentions[entity_key] = {"offsets": [start, end], "entity_type": entity_type}
                # link each mention to the canonical form of the entity 
                mentions[','.join([doc_id, str(start), str(end)])] = canonical_mentions[entity_key]["offsets"]
            # put the canonical entity in PlaceMention rather than the linked mention
            sf["PlaceMention"] = {
                "Start": canonical_mentions[entity_key]["offsets"][0],
                "End": canonical_mentions[entity_key]["offsets"][1],
                "EntityType": canonical_mentions[entity_key]["entity_type"]
            }
            final_sfs.append(sf)
    return final_sfs, mentions

def fix_labels(sfs):
    label_map = {
        "util": "utils",
        "terror": "terrorism",
        "unrest": "crimeviolence"
    }
    for sf in sfs:
        sf["Type"] = label_map.get(sf["Type"], sf["Type"])
        if "Status" in sf:
            sf["Status"]["Urgent"] = True if sf["Status"]["Urgent"] == "urgent" else False
    return sfs
            
if __name__ == '__main__':
    argparser = argparse.ArgumentParser()
    argparser.add_argument("username", help="user name of annotator")
    argparser.add_argument("goldstandard", help="output file name for goldstandard json")
    argparser.add_argument("mentions", help="output file name for mentions json")
    argparser.add_argument("--db_ip", default="127.0.0.1", help="database IP")
    argparser.add_argument("--db_port", type=int, default=27017, help="database port")
    argparser.add_argument("--db_name", default="lorelei_viewer", help="database name")
    args = argparser.parse_args()

    db.init(args.db_ip, args.db_port, args.db_name)
    results, mentions = dump_user_sf(args.username)
    with open(args.goldstandard, "w", encoding="utf-8") as fout:
        text = json.dumps(fix_labels(results), ensure_ascii=False, indent=4, sort_keys=True)
        fout.write(unicode(text))
    with open(args.mentions, "w", encoding="utf-8") as fout:
        text = json.dumps(mentions, ensure_ascii=False, indent=4, sort_keys=True)
        fout.write(unicode(text))
