#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function
from __future__ import unicode_literals
from io import open
import argparse, os, glob
import lxml.etree as ET

def convert(inputDir, outputFile):

    filenames = glob.glob(os.path.join(inputDir, "*.ltf.xml")) 
    with open(outputFile, "w", encoding="utf-8") as fout:
        for filename in filenames:
            tree = ET.parse(filename)
            for seg_node in tree.xpath(".//SEG"):
                tokens = []
                for tok_node in tree.xpath(".//TOKEN"):
                    tokens.append(tok_node.text)
                fout.write("%s\n" % " ".join(tokens))

if __name__ == '__main__':
    argparser = argparse.ArgumentParser()
    argparser.add_argument("inputDir", help="LLF input files dir")
    argparser.add_argument("outputFilename", help="LLF output files dir")
    args = argparser.parse_args()
    convert(args.inputDir, args.outputFilename)