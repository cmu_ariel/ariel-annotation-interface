#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import sys
import argparse
from lib import dbmanager as db
from glob import glob
from lxml import etree
from bisect import bisect_left, bisect_right


def get_args():
    argparser = argparse.ArgumentParser()
    argparser.add_argument("--user", default="LDC", help="your username")
    argparser.add_argument("--laf_dir", required=True, help="dir containing LDC's laf.xml files")
    argparser.add_argument("--ltf_dir", required=True, help="dir containing LDC's laf.xml files")
    argparser.add_argument("--db_ip", default="127.0.0.1", help="database IP")
    argparser.add_argument("--db_port", type=int, default=27017, help="database port")
    argparser.add_argument("--db_name", default="lorelei_viewer", help="database name")
    return argparser.parse_args()

def init_db(db_ip, db_port, db_name):
    db.init(db_ip, db_port, db_name)

def add_annotation(ann):
    # print(ann)
    if ann:
        db.add_annotation(ann)

def ltf_token_offsets(fpath):
    tree = etree.parse(fpath)
    root = tree.getroot()
    text = root.find("DOC").find("TEXT")
    offsets = []
    for seg in text.iter("SEG"):
        seg_id = seg.attrib["id"]
        for token in seg.iter("TOKEN"):
            data = {
                "start_char": int(token.attrib["start_char"]),
                "end_char": int(token.attrib["end_char"]),
                "seg_id": seg_id,
                "token_id": token.attrib["id"],
                "text": token.text
            }
            offsets.append(data)
    return sorted(offsets, key=lambda x: x["start_char"])

def get_seg_tok_ids(offsets, start_char, end_char):
    start_chars = [x["start_char"] for x in offsets]
    end_chars = [x["end_char"] for x in offsets]
    i = bisect_left(start_chars, start_char)
    assert(offsets[i]["start_char"] == start_char)
    j = bisect_left(end_chars, end_char)
    assert(offsets[j]["end_char"] == end_char)
    seg_id = offsets[i]["seg_id"]
    return seg_id, [offsets[n]["token_id"] for n in range(i, j+1)], [offsets[n]["text"] for n in range(i, j+1)]


def main(args):
    init_db(args.db_ip, args.db_port, args.db_name)
    ann = None
    for fpath in glob(os.path.join(args.laf_dir, "*.laf.xml")):
        fname = os.path.basename(fpath)
        offsets = ltf_token_offsets(os.path.join(args.ltf_dir, fname[:-7] + "ltf.xml"))
        tree = etree.parse(fpath)
        root = tree.getroot()
        doc = root.find("DOC")
        docid = doc.attrib["id"]
        for annotation in doc.iter("ANNOTATION"):
            tag = annotation.find("TAG").text.strip().upper()
            extent = annotation.find("EXTENT")
            start_char = int(extent.attrib["start_char"])
            end_char = int(extent.attrib["end_char"])
            seg_id, tok_ids, text2 = get_seg_tok_ids(offsets, start_char, end_char)
            # print extent.text, text2[0], text2[-1]
            assert(extent.text.startswith(text2[0]))
            assert(extent.text.endswith(text2[-1]))
            ann = {
                "doc_id" : docid,
                "user" : args.user,
                "seg_id" : seg_id, 
                "tok_id" : tok_ids,
                "annotation_type" : "ner",
                "annotation_label" : tag
            }
            add_annotation(ann)

if __name__ == "__main__":
    main(get_args())
