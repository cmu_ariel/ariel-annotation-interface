#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import argparse
from lib import dbmanager as db


def get_args():
    argparser = argparse.ArgumentParser()
    argparser.add_argument("--user", default="conll_import", help="your username")
    argparser.add_argument("--db_ip", default="127.0.0.1", help="database IP")
    argparser.add_argument("--db_port", type=int, default=27017, help="database port")
    argparser.add_argument("--db_name", default="lorelei_viewer", help="database name")
    return argparser.parse_args()

def init_db(db_ip, db_port, db_name):
    db.init(db_ip, db_port, db_name)

def add_annotation(ann):
    if ann:
        db.add_annotation(ann)

def main(args):
    init_db(args.db_ip, args.db_port, args.db_name)
    ann = None
    for line in sys.stdin:
        columns = line.strip().split('\t')
        if len(columns) < 11:
            add_annotation(ann)
            ann = None
            continue
        tokenID, segID, docID = columns[1:4]
        ner_prefix = columns[10].split('-')[0]
        if ner_prefix == "B":
            add_annotation(ann)
            ann = None
            ann = {
                "doc_id" : docID,
                "user" : args.user,
                "seg_id" : segID, 
                "tok_id" : [tokenID],
                "annotation_type" : "ner",
                "annotation_label" : columns[10].split('-')[1]
            }
        elif ner_prefix == "I":
            if not ann:
                sys.exit("Bug")
            ann["tok_id"].append(tokenID)
        elif ner_prefix == "O":
            add_annotation(ann)
            ann = None
    add_annotation(ann)

if __name__ == "__main__":
    main(get_args())
