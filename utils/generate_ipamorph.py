#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function
from __future__ import unicode_literals
from io import open
import argparse, os, glob, sys
from progressbar import ProgressBar, Bar, AdaptiveETA, Percentage
import epitran
import lxml.etree as ET
sys.path.append("../morph")
from ipamorph_disam_uig import *

def replace_tokens(segments, tokensToReplace):
    
    for segment in segments:
        seg_id = segment.attrib["id"]
        ET.strip_elements(segment, 'TOKEN')
        token_texts = []
        for token in tokensToReplace[seg_id]:
            token_node = ET.Element("TOKEN")
            token_node.text = token["text"]
            token_texts.append(token["text"])
            token_ids = token["id"]
            token_node.attrib["id"] = " ".join(token_ids)
            segment.append(token_node)
        originalText = segment.xpath(".//ORIGINAL_TEXT")[0]
        originalText.text = " ".join(token_texts)

def save_xml(outputDir, basename, tree):
    outputFilename = os.path.join(outputDir, basename)
    with open(outputFilename, 'w', encoding="utf-8") as fout:
        fout.write(ET.tostring(tree, encoding='unicode', pretty_print=True))


def add_to_xml(inputDir, outputDir):

    xmlParser = ET.XMLParser(remove_blank_text=True)
    filenames = glob.glob(os.path.join(inputDir, "*.ltf.xml"))
    
    bar = ProgressBar(maxval=len(filenames), widgets=[
        Bar('=', '[', ']'), ' ',
        Percentage(), ' ',
        AdaptiveETA()])
    
    for inputFilename in bar(filenames):
        
        basename = os.path.basename(inputFilename)
        
        tree = ET.parse(inputFilename, xmlParser)
        
        segments = tree.xpath(".//SEG")
        segments_lemma = {}

        for segment in segments:
        
            seg_id = segment.attrib["id"]
            lemmas = []
            
            for token in segment.xpath(".//TOKEN"):
                text = unicode(token.text)
                lemma, gloss, definition, natural, prob = disam_parser_uig.get_gloss_natural(text)[0]
                lemmas.append( {
                    "text": lemma,
                    "id": [unicode(token.attrib["id"])]
                })             
                              
            segments_lemma[seg_id] = lemmas
                        
        
        replace_tokens(segments, segments_lemma)
        save_xml(outputDir, basename, tree)

        
if __name__ == '__main__':
    argparser = argparse.ArgumentParser()
    argparser.add_argument("inputDir", help="Directory containing the input")
    argparser.add_argument("outputDir", help="Directory to hold the output.")
    args = argparser.parse_args()
    add_to_xml(args.inputDir, args.outputDir)