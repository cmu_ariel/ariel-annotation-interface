#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function
from __future__ import unicode_literals
from io import open
import argparse, glob, os, json, sys
import lxml.etree as ET
sys.path += ["..", "../lib"]
from lib import dbmanager as db
from collections import Counter

from progressbar import ProgressBar, Bar, AdaptiveETA, Percentage
    
def log_error(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)

def ensure_dir(mydir):
    if not os.path.isdir(mydir):
        os.makedirs(mydir)


def make_ltf(input_dir, output_dir, doc_id, seg_id):

    new_doc_id = "%s_%s" % (doc_id, seg_id)
    inputFilename = os.path.join(input_dir, doc_id + ".ltf.xml")
    outputFilename = os.path.join(output_dir, new_doc_id + ".ltf.xml")
    
    full_seg_id = "segment-" + seg_id
    
    tree = ET.parse(inputFilename)
    doc = tree.xpath(".//DOC")[0]
    doc.attrib["id"] = new_doc_id
    text = tree.xpath(".//TEXT")[0]
        
    for seg in list(text):
        if seg_id != "*" and seg.attrib["id"] != full_seg_id:
            text.remove(seg)
            
    with open(outputFilename, "w", encoding="utf-8") as fout:
        fout.write('<?xml version="1.0" encoding="UTF-8"?>' + "\n")
        fout.write(ET.tostring(tree, encoding="unicode", pretty_print=True))
    

def make_laf(output_dir, doc_id, seg_id):

    new_doc_id = "%s_%s" % (doc_id, seg_id)
    full_seg_id = "segment-" + seg_id
    outputFilename = os.path.join(output_dir, "%s.laf.xml" % new_doc_id)
    
    ner_root = ET.Element("LCTL_ANNOTATIONS", lang="amh")
    doc_node = ET.Element("DOC", id=new_doc_id, lang="amh")
    ner_root.append(doc_node)
        
    ner_annos = list(db.annotations.find({"doc_id":doc_id, "seg_id":full_seg_id, "annotation_type":"ner"}))
            
    for ner_index, ner_anno in enumerate(ner_annos):

        if "user_type" not in ner_anno or ner_anno["user_type"] != "human":
            continue
            
        token_texts = []
        tokens = db.get_textitems({"doc_id":doc_id, "seg_id":ner_anno["seg_id"], "tok_ids":ner_anno["tok_id"], "view_id":"original"})
        tokens.sort(key=lambda x:x["tok_offset"])
        offsetsFound = set()
        for token in tokens:
            if token["tok_offset"] in offsetsFound:
                continue
            token_texts.append(token["text"])
            offsetsFound.add(token["tok_offset"])
        
        if not tokens:
            print("Could not find tokens in %s %s" % (doc_id, ner_anno["seg_id"]))
            continue

        start_char = tokens[0]["start_char"]
        end_char = tokens[-1]["end_char"]
        
        id = "em-%s-%03d" % (new_doc_id, ner_index)
        anno = ET.Element("ANNOTATION", id=id, task="NE", seg_id=ner_anno["seg_id"])
        
        extent = ET.Element("EXTENT")
        extent.attrib["start_char"] = str(start_char)
        extent.attrib["end_char"] = str(end_char)
        extent.text = " ".join(token_texts)
        anno.append(extent)
        tag = ET.Element("TAG")
        tag.text = ner_anno["annotation_label"]
        anno.append(tag)
        doc_node.append(anno)

    with open(outputFilename, "w", encoding="utf-8") as fout:
        fout.write('<?xml version="1.0" encoding="UTF-8"?>' + "\n")
        fout.write('<!DOCTYPE LCTL_ANNOTATIONS SYSTEM "laf.v1.2.dtd">' + "\n")
        fout.write(ET.tostring(ner_root, encoding="unicode", pretty_print=True))
            
def get_annotated_segments(input_dir, output_dir):

    tasks = list(db.tasks.find({"role":"ner_annotator", "finished":True}))
    
    
    bar = ProgressBar(maxval=len(tasks), poll=1, widgets=[
        Bar('=', '[', ']'), ' ',
        Percentage(), ' ',
        AdaptiveETA()])
        
    for task in tasks:
        for item in task["items"]:
            doc_id = item["doc"]
            if "." in doc_id:
                continue
            seg_id = item["seg"]
            #tokens = db.textitems.find({"doc_id":doc_id, "seg_id":seg_id, "view_id":"original"})
            #tokens = sort_tokens(tokens)
            make_ltf(input_dir, output_dir, doc_id, seg_id)
            make_laf(output_dir, doc_id, seg_id)
            

if __name__ == '__main__':
    argparser = argparse.ArgumentParser()
    argparser.add_argument("inputDir", help="Directory with input LTFs")
    argparser.add_argument("outputDir", help="Directory to store the outputs")
    #argparser.add_argument("resourceID", help="ID of the resource to extract EDL annotations for")
    argparser.add_argument("--db_ip", default="127.0.0.1", help="database IP")
    argparser.add_argument("--db_port", type=int, default=27017, help="database port")
    argparser.add_argument("--db_name", default="lorelei_viewer", help="database name")
    args = argparser.parse_args()
    
    db.init(args.db_ip, args.db_port, args.db_name)
    ensure_dir(args.outputDir)
    
    get_annotated_segments(args.inputDir, args.outputDir)
