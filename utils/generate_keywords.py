#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function
from __future__ import unicode_literals
from io import open
import argparse, os, glob
from progressbar import ProgressBar, Bar, AdaptiveETA, Percentage
import lxml.etree as ET
from sklearn.feature_extraction.text import TfidfVectorizer
import numpy as np
import json

def ensure_dir(mydir):
    if not os.path.isdir(mydir):
        os.makedirs(mydir)
        
    
def go(inputDir, outputDir):

    xmlParser = ET.XMLParser(remove_blank_text=True)
    filenames = glob.glob(os.path.join(inputDir, "*.ltf.xml"))
    
    bar = ProgressBar(maxval=len(filenames), poll=1, widgets=[
        Bar('=', '[', ']'), ' ',
        Percentage(), ' ',
        AdaptiveETA()])
    
    doc_ids = []
    texts = []
    tokens_per_doc_id = {}
    
    for i, inputFilename in bar(enumerate(filenames)):
        
        basename = os.path.basename(inputFilename)
        tree = ET.parse(inputFilename, xmlParser)
        doc_id = tree.xpath(".//DOC/@id")[0]
        doc_id = doc_id.split(".")[0]
        text = [ seg.text for seg in tree.xpath(".//ORIGINAL_TEXT") ]
        tokens_per_doc_id[doc_id] = len(list(tree.xpath(".//TOKEN")))
        doc_ids.append(doc_id)
        texts.append("\n".join(text))
    
    tfv = TfidfVectorizer(max_df=0.95, min_df=2,
                                   max_features=1000,
                                   stop_words='english')
    results = tfv.fit_transform(texts).toarray()
    featureNames = tfv.get_feature_names()
    terms = results.argsort(axis=1)
    
    for doc_id, row in zip(doc_ids, terms):
        num_keywords = min(10, tokens_per_doc_id[doc_id])
        top_words = [ featureNames[item] for item in row[-10:] ]
        summary = { "doc_id": doc_id, "keywords": top_words }
        outputFilename = os.path.join(outputDir, doc_id+".keywords.json")
        with open(outputFilename, "w", encoding="utf-8") as fout:
            fout.write(json.dumps(summary, ensure_ascii=False, indent=4))
    
if __name__ == '__main__':
    argparser = argparse.ArgumentParser()
    argparser.add_argument("inputDir", help="The location of the LTF files")
    argparser.add_argument("outputDir", help="A new location for the result.")
    args = argparser.parse_args()
    ensure_dir(args.outputDir)
    #add_to_xml(args.inputDir, args.outputDir)
    go(args.inputDir, args.outputDir)
    