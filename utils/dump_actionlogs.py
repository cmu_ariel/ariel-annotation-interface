#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function
from __future__ import unicode_literals
from io import open
import argparse, glob, os, json, sys
import lxml.etree as ET
sys.path += ["..", "../lib"]
from lib import dbmanager as db
from collections import Counter
import re

from progressbar import ProgressBar, Bar, AdaptiveETA, Percentage


def log_error(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)

def get_doc_ids(res_id):
    return list(set(r["doc_id"] for r in db.resourceInfoDB.find({"res_id":res_id})))

def dump_actionlogs(res_id, output, user, action):

    doc_ids = get_doc_ids(res_id)
    db_client = db.get_client()
    logs = db_client.actionlogs.find({
        "doc_id": {"$in": doc_ids},
        "user": user,
        "action": action
    })

    with open(output, "w", encoding="utf-8") as fout:
        for l in list(logs):
            fout.write("\t".join([l["doc_id"], l["seg_id"], unicode(l["_id"].generation_time)]) + "\n")
                

if __name__ == '__main__':
    argparser = argparse.ArgumentParser()
    argparser.add_argument("output", help="output file")
    argparser.add_argument("resourceID", help="ID of the resource")
    argparser.add_argument("user", help="username of the annotator")
    argparser.add_argument("action", help="type of action to retrieve")
    argparser.add_argument("--db_ip", default="127.0.0.1", help="database IP")
    argparser.add_argument("--db_port", type=int, default=27017, help="database port")
    argparser.add_argument("--db_name", default="lorelei_viewer", help="database name")
    args = argparser.parse_args()
    
    if os.path.exists(args.output):
        sys.exit(args.output + " already exists!")
    db.init(args.db_ip, args.db_port, args.db_name)
    dump_actionlogs(args.resourceID, args.output, args.user, args.action)
