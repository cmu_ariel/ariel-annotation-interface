#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function
from __future__ import unicode_literals
from io import open
import argparse, os, glob, sys
from progressbar import ProgressBar, Bar, AdaptiveETA, Percentage
sys.path.append("/home/data/LoReHLT17/internal/Morph/Orm_ASR/v4/")
from orm_morph import parse
import epitran


epi = epitran.Epitran("orm-Latn")
g2p = epi.transliterate

def unique(lst):
    results = []
    found = set()
    for l in lst:
        if l in found:
            continue
        found.add(l)
        results.append(l)
    return results[:10]

def go(inputFilename, outputFilename):
    with open(inputFilename, "r", encoding="utf-8") as fin:
        with open(outputFilename, "w", encoding="utf-8") as fout:
            lines = list(fin.readlines())
                
            bar = ProgressBar(maxval=len(lines), poll=1, widgets=[
                Bar('=', '[', ']'), ' ',
                Percentage(), ' ',
                AdaptiveETA()])
            
            for line in bar(lines):
                line = line.strip()
                if not line:
                    fout.write("\n")
                    continue
                parts = line.split()

                if len(parts) < 2:
                    fout.write(line + "\n")
                    continue
                word = parts[0]
                freq = parts[1]
                ipa = g2p(word)
                glish = "; ".join(unique(parse(word, "natural")))
                fout.write("  %s %s\t%s\t%s\n" % (word, freq, ipa, glish))


if __name__ == '__main__':
    argparser = argparse.ArgumentParser()
    argparser.add_argument("input", help="An input space-separated file containing a 'token' row")
    argparser.add_argument("output", help="A space-separated file to hold the output.")
    args = argparser.parse_args()
    go(args.input, args.output)