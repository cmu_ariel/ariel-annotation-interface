#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function
from __future__ import unicode_literals
from io import open
import argparse, glob, os, json, sys
from progressbar import ProgressBar, Bar, AdaptiveETA, Percentage
sys.path += ["..", "../lib"]
from lib import dbmanager as db
from load_resources import TextResourceDB

"""
Watches a directory for CoNLL files to load into the interface
"""

metadata = {
    "lang_id": "",
    "res_id": "",
    "res_name": "",
    "filename": "*.conll", 
    "format": "conll_ner", 
    "user": "default",
    "res_type": "monolingual_text", 
    "type": "monolingual_text", 
    "view_id": "original",
    "view_name": "Original",
    "view_priority": 1.0,
    "token_align": True,
    "pinned": True
}


def check_files(inputDir, metadata):
    if db.get_resourceInfo({"res_id": metadata["res_id"]}):
        sys.exit("Resource ID %s already exists in the database!" % metadata["res_id"])

    input_files = glob.glob(os.path.join(inputDir, metadata["filename"]))
    if not input_files:
        sys.exit("No %s files found in %s" % (metadata["filename"], inputDir))

    for fpath in input_files:
        fname = os.path.splitext(os.path.basename(fpath))[0]
        if '.' in fname or '-' in fname:
            sys.exit("- or . not allowed in file names!")

        for doc_id in [fname, os.path.basename(fpath).replace('.', '')]:
            if db.get_resourceInfo({"doc_id": doc_id}):
                sys.exit("Doc %s already exists in the database!" % doc_id)
            if db.get_textitems({"doc_id": doc_id}):
                sys.exit("Doc %s already exists in the database!" % doc_id)
            if db.get_annotations({"doc_id": doc_id}):
                sys.exit("Doc %s already exists in the database!" % doc_id)

    return True


if __name__ == '__main__':
    argparser = argparse.ArgumentParser()
    argparser.add_argument("inputDir", help="directory containing CoNLL files")
    argparser.add_argument("uniqueId", help="unique resource ID for this doc set")
    argparser.add_argument("--lang", required=True, help="database IP")
    argparser.add_argument("--db_port", type=int, required=True, help="database port")
    argparser.add_argument("--db_ip", default="127.0.0.1", help="database IP")
    argparser.add_argument("--db_name", default="lorelei_viewer", help="database name")
    argparser.add_argument("--morph", action="store_true")
    args = argparser.parse_args()
    
    db.init(args.db_ip, args.db_port, args.db_name)
    metadata["lang_id"] = args.lang
    metadata["res_id"] = args.uniqueId
    metadata["res_name"] = args.uniqueId
    if args.morph:
        metadata["format"] = "conll_morph"
    if check_files(args.inputDir, metadata):
        print("Loading file!")
        with open(os.path.join(args.inputDir, "ariel_metadata.json"), 'w', encoding="utf-8") as outfile:
            outfile.write(unicode(json.dumps(metadata, indent=4, separators=(',', ': '), ensure_ascii=False, sort_keys=True)))
        textDB = TextResourceDB()
        textDB.loadResources(args.inputDir)        
