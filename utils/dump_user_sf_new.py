#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from io import open
import argparse, glob, os, json, sys
import lxml.etree as ET
sys.path += ["..", "../lib"]
from lib import dbmanager as db
from copy import deepcopy
from collections import defaultdict


def dump_user_sf(user):
    final_sfs = []
    doc_entities = {}

    # get all SF type and status annotations from db
    sf_annos = {"sf_need": {}, "sf_relief": {}, "sf_urgency": {}}
    for ann_type in sf_annos:
        for sf in db.annotations.find({"annotation_type": ann_type, "user": user}):
            tok_span = (sf["doc_id"], sf["seg_id"], " ".join(sf["tok_id"]))
            sf_annos[ann_type][tok_span] = sf["annotation_label"]

    for ann_type in ["sf", "sf_location"]:
        sf_annos[ann_type] = defaultdict(set)
        for sf in db.annotations.find({"annotation_type": ann_type, "user": user}):
            tok_span = (sf["doc_id"], sf["seg_id"], " ".join(sf["tok_id"]))
            sf_annos[ann_type][tok_span].add(sf["annotation_label"])

    # go through each SF "type" annotation
    for tok_span in sf_annos["sf"]:
        doc_id, seg_id, tok_id = tok_span
        tok_id = tok_id.split()

        # reconstruct text from token IDs
        tokens = [db.textitems.find_one({
            "doc_id": doc_id, 
            "seg_id": seg_id, 
            "view_id": "original", 
            "tok_ids": [token]
        })["text"] for token in tok_id]

        # create base SF without PlaceMention
        sf_base = {
            "DocumentID": doc_id,
            "SegmentID": seg_id,
            "Type": "__DEFAULT__",
            "Text": " ".join(tokens),
            "Place_KB_ID": "__DEFAULT__",
            # WARNING: default values used if status annotations not found
            "Status": sf_annos["sf_need"].get(tok_span, "__DEFAULT__"),
            "Resolution": sf_annos["sf_relief"].get(tok_span, "__DEFAULT__"),
            "Urgent": sf_annos["sf_urgency"].get(tok_span, "__DEFAULT__")
        }

        # for each location linked to the token span, create a new SF with PlaceMention
        for sf_type in sf_annos["sf"][tok_span]:
            sf_base["Type"] = sf_type
            for sf_loc in (sf_annos["sf_location"][tok_span] or ["NONE"]):
                doc_entities[(doc_id, sf_loc)] = {"mentions": set()}
                sf = deepcopy(sf_base)
                sf["Place_KB_ID"] = sf_loc
                final_sfs.append(sf)

        # for sf_loc in sf_annos["sf_location"][tok_span] or "":
            # doc_entities[(doc_id, sf_loc)] = {"mentions": set()}
            # sf_base["Place_KB_ID"] = sf_loc
            # for sf_type in sf_annos["sf"][tok_span]:
                # sf = deepcopy(sf_base)
                # sf["Type"] = sf_type
                # final_sfs.append(sf)

    for doc_id, sf_loc in doc_entities:
        # go through all mentions of the entity_id in the same document
        for mention in db.annotations.find({"user": user, "annotation_type": "edl", "annotation_label": sf_loc, "doc_id": doc_id}):
            start = db.textitems.find_one({
                "doc_id": doc_id,
                "seg_id": mention["seg_id"],
                "view_id": "original",
                "tok_ids": [mention["tok_id"][0]]
            })["start_char"]
            end = db.textitems.find_one({
                "doc_id": doc_id,
                "seg_id": mention["seg_id"],
                "view_id": "original",
                "tok_ids": [mention["tok_id"][-1]]
            })["end_char"]

            if "entity_type" not in doc_entities:
                # get the entity type (GPE or LOC) from its NER annotation
                ner_ann = db.annotations.find_one({
                    "annotation_type": "ner",
                    "doc_id": doc_id,
                    "seg_id": mention["seg_id"],
                    "tok_id": mention["tok_id"],
                    "user": user
                })
                if ner_ann:
                    doc_entities[(doc_id, sf_loc)]["entity_type"] = ner_ann["annotation_label"]
            doc_entities[(doc_id, sf_loc)]["mentions"].add((start, end))

    all_entities = {}
    for (doc_id, sf_loc), v in doc_entities.items():
        all_entities["%s:%s" % (doc_id, sf_loc)] = {
            "type": v.get("entity_type", "NOT_FOUND"),
            "mentions": list(v["mentions"])
        }
    return final_sfs, all_entities

def dump_ldc(sfs, entities, outdir):
    """
    Write SFs/mentions in LDC's tsv format
    """
    issue_headers = "user_id doc_id frame_id frame_type issue_type place_id proxy_status issue_status description".split()
    need_headers = "user_id doc_id frame_id frame_type need_type place_id proxy_status need_status urgency_status resolution_status reported_by resolved_by description".split()
    mention_headers = "doc_id entity_id mention_id entity_type mention_status start_char end_char mention_text".split()
    edl_headers = "system_run_id mention_id mention_text extents kb_id entity_type mention_type confidence".split()
    issues_dir = os.path.join(outdir, "issues")
    mentions_dir = os.path.join(outdir, "mentions")
    needs_dir = os.path.join(outdir, "needs")
    edl_file = os.path.join(outdir, "edl.tab")
    for dirpath in [outdir, issues_dir, mentions_dir, needs_dir]:
        if not os.path.exists(dirpath):
            os.makedirs(dirpath)

def fix_labels(sfs):
    label_map = {
        "util": "utils",
        "terror": "terrorism",
        "unrest": "crimeviolence"
    }
    for sf in sfs:
        sf["Type"] = label_map.get(sf["Type"], sf["Type"])
        if "Urgent" in sf:
            sf["Urgent"] = True if sf["Urgent"] == "urgent" else False
    return sfs
            
if __name__ == '__main__':
    argparser = argparse.ArgumentParser()
    argparser.add_argument("username", help="user name of annotator")
    argparser.add_argument("goldstandard", help="output file name for goldstandard json")
    argparser.add_argument("entities", help="output file name for entities json")
    argparser.add_argument("--db_ip", default="127.0.0.1", help="database IP")
    argparser.add_argument("--db_port", type=int, default=27017, help="database port")
    argparser.add_argument("--db_name", default="lorelei_viewer", help="database name")
    argparser.add_argument("--ldc_export_dir", help="empty directory to save annotations in LDC's tsv format")
    args = argparser.parse_args()

    db.init(args.db_ip, args.db_port, args.db_name)
    results, entities = dump_user_sf(args.username)
    with open(args.goldstandard, "w", encoding="utf-8") as fout:
        text = json.dumps(fix_labels(results), ensure_ascii=False, indent=4, sort_keys=True)
        fout.write(unicode(text))
    if args.ldc_export_dir:
        dump_ldc(results, entities, args.ldc_export_dir)
    with open(args.entities, "w", encoding="utf-8") as fout:
        text = json.dumps(entities, ensure_ascii=False, indent=4, sort_keys=True)
        fout.write(unicode(text))
