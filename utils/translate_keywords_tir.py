#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function
from __future__ import unicode_literals
from io import open
import argparse, glob, os, json, sys
from progressbar import ProgressBar, Bar, AdaptiveETA, Percentage
sys.path.append("/home/data/LoReHLT17/internal/Morph/Tir/v5")
from tir_morph_NARAE import best_parse



def ensure_dir(mydir):
    if not os.path.isdir(mydir):
        os.makedirs(mydir)
        

def convert(inputDir, outputDir):

    inputFilenames = glob.glob(os.path.join(inputDir, "*.keywords.json"))
    
    
    bar = ProgressBar(maxval=len(inputFilenames), poll=1, widgets=[
        Bar('=', '[', ']'), ' ',
        Percentage(), ' ',
        AdaptiveETA()])
        
    for inputFilename in bar(inputFilenames):
        basename = os.path.basename(inputFilename)
        outputFilename = os.path.join(outputDir, basename)
        with open(inputFilename, "r", encoding="utf-8") as fin:
            obj = json.load(fin)
            obj["keywords"] = [ best_parse(k, "definition") for k in obj["keywords"] ]
            with open(outputFilename, "w", encoding="utf-8") as fout:
                fout.write(json.dumps(obj, ensure_ascii=False, indent=4))


if __name__ == '__main__':
    argparser = argparse.ArgumentParser()
    argparser.add_argument("inputDir", help="Directory to store the outputs")
    argparser.add_argument("outputDir", help="Directory to store the outputs")
    args = argparser.parse_args()
    
    ensure_dir(args.outputDir)
    convert(args.inputDir, args.outputDir)
