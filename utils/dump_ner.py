#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function
from __future__ import unicode_literals
from io import open
import argparse, glob, os, json, sys
import lxml.etree as ET
sys.path += ["..", "../lib"]
from lib import dbmanager as db
from collections import Counter

from progressbar import ProgressBar, Bar, AdaptiveETA, Percentage
    
def log_error(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)

def ensure_dir(mydir):
    if not os.path.isdir(mydir):
        os.makedirs(mydir)
        
    
def get_doc_ids(res_id):
    return set(r["doc_id"] for r in db.resourceInfoDB.find({"res_id":res_id}))
    
def dump_ner(res_id, outputDir):

    doc_ids = get_doc_ids(res_id)

    bar = ProgressBar(maxval=len(doc_ids), poll=1, widgets=[
        Bar('=', '[', ']'), ' ',
        Percentage(), ' ',
        AdaptiveETA()])

    annoUsers = Counter()

    for doc_index, doc_id in enumerate(bar(doc_ids)):
    
        ner_root = ET.Element("LCTL_ANNOTATIONS", lang="amh")
        doc_node = ET.Element("DOC", id=doc_id, lang="amh")
        ner_root.append(doc_node)
        
        ner_annos = list(db.annotations.find({"doc_id":doc_id, "annotation_type":"ner"}))
        if len(ner_annos) < 1:
            continue
        
        usersFoundInDoc = set()
        
        for ner_index, ner_anno in enumerate(ner_annos):

            usersFoundInDoc.add(ner_anno["user"])
            
            #result = (ner_anno["doc_id"], ner_anno["seg_id"], " ".join(ner_anno["tok_id"]))
            #ner_annos[result] = ner_anno["annotation_label"]
            
            # find the actual tokens, for convenience
            token_texts = []
            tokens = db.get_textitems({"doc_id":doc_id, "seg_id":ner_anno["seg_id"], "tok_ids":ner_anno["tok_id"], "view_id":"original"})
            tokens.sort(key=lambda x:x["tok_offset"])
            for token in tokens:
                token_texts.append(token["text"])
            
            if not tokens:
                print("Could not find tokens in %s %s" % (doc_id, ner_anno["seg_id"]))
                continue

            start_char = tokens[0]["start_char"]
            end_char = tokens[-1]["end_char"]
            
            id = "em-%05d-%03d" % (doc_index, ner_index)
            anno = ET.Element("ANNOTATION", id=id, task="NE", seg_id=ner_anno["seg_id"])
            
            extent = ET.Element("EXTENT")
            extent.attrib["start_char"] = str(start_char)
            extent.attrib["end_char"] = str(end_char)
            extent.text = " ".join(token_texts)
            anno.append(extent)
            tag = ET.Element("TAG")
            tag.text = ner_anno["annotation_label"]
            anno.append(tag)
            doc_node.append(anno)

            annoUsers[ner_anno["user"]] += 1
            
        if not list(doc_node): # if no children
            continue
            
        if usersFoundInDoc <= set(["ner_lookup","lookup_ner", "NERLookup", "ldc", "CP1_NER"]):
            print("Only auto-users found in document %s, skipping" % doc_id)
            continue
            
        outputFilename = os.path.join(outputDir, "%s.laf.xml" % doc_id)
        with open(outputFilename, "w", encoding="utf-8") as fout:
#            text = json.dumps(results, ensure_ascii=False, indent=4)
#            fout.write(unicode(text))
            fout.write('<?xml version="1.0" encoding="UTF-8"?>' + "\n")
            fout.write('<!DOCTYPE LCTL_ANNOTATIONS SYSTEM "laf.v1.2.dtd">' + "\n")
            fout.write(ET.tostring(ner_root, encoding="unicode", pretty_print=True))

    print("Annos found = %s" % sum(annoUsers.values()))
    for user, count in annoUsers.most_common():
        print("  %s: %s" % (user, count))
            

if __name__ == '__main__':
    argparser = argparse.ArgumentParser()
    argparser.add_argument("outputDir", help="Directory to store the outputs")
    argparser.add_argument("resourceID", help="ID of the resource to extract EDL annotations for")
    argparser.add_argument("--db_ip", default="127.0.0.1", help="database IP")
    argparser.add_argument("--db_port", type=int, default=27017, help="database port")
    argparser.add_argument("--db_name", default="lorelei_viewer", help="database name")
    args = argparser.parse_args()
    
    db.init(args.db_ip, args.db_port, args.db_name)
    ensure_dir(args.outputDir)
    
    dump_ner(args.resourceID, args.outputDir)
