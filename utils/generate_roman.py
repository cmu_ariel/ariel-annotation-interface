#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function
from __future__ import unicode_literals
from io import open
import argparse, os, glob
from progressbar import ProgressBar, Bar, AdaptiveETA, Percentage
import epitran
import lxml.etree as ET

def romanize(token):
    token = token.replace("y","ü").replace("j","y").replace("t͡ʃ","ch").replace("d͡ʒ", "dj").replace("ø","ö").replace("ɛ","e")
    token = token.replace("ʁ","gh").replace("ʒ","zh").replace("ʃ","sh").replace("χ","h").replace("ŋ","ng")
    return token.replace("ə","a").replace("ɨ","i").replace("ʔ","'").replace("ɲ","ñ").replace("ʼ","'").replace("t͡s","ts")


def ensure_dir(mydir):
    if not os.path.isdir(mydir):
        os.makedirs(mydir)
        
def add_to_xml(inputDir, outputDir, lang):

    transliterate = epitran.Epitran(lang).transliterate
    xmlParser = ET.XMLParser(remove_blank_text=True)
    filenames = glob.glob(os.path.join(inputDir, "*.ltf.xml"))
    
    bar = ProgressBar(maxval=len(filenames), poll=1, widgets=[
        Bar('=', '[', ']'), ' ',
        Percentage(), ' ',
        AdaptiveETA()])
    
    for i, inputFilename in bar(enumerate(filenames)):
        
        basename = os.path.basename(inputFilename)
        
        tree = ET.parse(inputFilename, xmlParser)
        for segment in tree.xpath(".//SEG"):
            token_texts = []
            for token in segment.xpath(".//TOKEN"):
                text = unicode(token.text)
                if not text:
                    print("Warning: %s-%s-%s is empty: %s" % (basename, segment.id, token.id, text))
                    continue
                ipa = transliterate(unicode(token.text))
                token.text = romanize(ipa)
                token_texts.append(token.text)
            originalText = segment.xpath(".//ORIGINAL_TEXT")[0]
            originalText.text = " ".join(token_texts)
            
        outputFilename = os.path.join(outputDir, basename)
        with open(outputFilename, 'w', encoding="utf-8") as fout:
            fout.write(ET.tostring(tree, encoding='unicode', pretty_print=True))
        
    
if __name__ == '__main__':
    argparser = argparse.ArgumentParser()
    argparser.add_argument("inputDir", help="An input tab-separated file containing a 'token' row")
    argparser.add_argument("outputDir", help="A tab-separated file to hold the output.")
    argparser.add_argument("--lang", help="The language-script pair (e.g., eng-Latn).")
    args = argparser.parse_args()
    ensure_dir(args.outputDir)
    add_to_xml(args.inputDir, args.outputDir, args.lang)
    