#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function
from __future__ import unicode_literals
from io import open
import argparse, math, glob, os, json, sys, fnmatch, time, traceback, hashlib
from collections import Counter, defaultdict
import lxml.etree as ET
from progressbar import ProgressBar, Bar, AdaptiveETA, Percentage
from collections import defaultdict
import pystache
from lib.page_gen import PageGenerator
from lib import user_roles
from lib import dbmanager as db
#from utils.load_resources import TextResourceDB
from bson import json_util 
from gevent.wsgi import WSGIServer
from gevent import monkey; monkey.patch_socket()
from functools import wraps
from utils.send_mail import *
import pycountry
#import zmq
from flask_sendmail import Mail
import requests
from copy import deepcopy

from flask import Flask, request, Response, make_response, redirect
app = Flask(__name__)
flask_mail = Mail(app)

import logging

logger = logging.getLogger()
logger.setLevel(logging.DEBUG)
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
ch.setFormatter(formatter)
logger.addHandler(ch)

'''
None of the following is currently true

This module takes a variety of LDC/ARIEL resources, like input LTF XML, output SF JSON, etc., and displays them as HTML pages in parallel, and attempts (insofar as possible) to highlight related information on mouseover.

Its only input file is a JSON manifest that look like the following, and details the properties of the different resources (like whether they should be visible by default, what writing direction they should be displayed in, etc.).

[
    {   "name" : "Original",
        "category" : "text",
        "format" : "ltf_input",
        "locations" : ["/home/data/LoReHLT16/internal/RETOKEN/retokened_v1/setE/data/monolingual_text/ltf"],
        "visible" : true,
        "direction" : "rtl" },
        
    {   "name" : "IPA",
        "category" : "text",
        "format" : "ltf_input",
        "visible" : true,
        "locations" : ["setE/ipa"] },
    ...
]

Insofar as the LDC formats sometimes change in unpredictable ways, and some of our intermediate outputs have no single standard format, parts of this may break in the future.  You can define a new format by inheriting from ARIELResource and overloading __init__, getItemsForConversion (which takes a list of resource locations and returns a list of items to iterate over, whether that's filenames, SFs, whatever's appropriate), and convert (which converts those items into onscreen display items, subclasses of SpanItem, and puts them into a dictionary self.entities, addressable by document ID and segment ID).

The HTML is formed primarily by constructing a JSON object with the relevant information and injecting that into a big Mustache template.  The only exception is SpanItem descendents, which are tasked with creating their own HTML (in the toHTML() method).  Otherwise it'd be annoying to define new SpanItems descendents that get custom HTML; we'd have to set boolean flags and switch on those within the Mustache template.
'''


class DataConnection:

    def __init__(self, ip, port, lang, view_id):
        self.ip = ip
        self.port = port
        self.lang = lang
        self.view_id = view_id
        
    def getLangForDoc(self, doc_id):
        ris = db.get_resourceInfo({"doc_id":doc_id})
        if not ris:
            return ''
        return ris[0]["lang_id"]
    
    def getTokens(self, doc_id):
        lang = self.getLangForDoc(doc_id)
        if lang != self.lang:
            return []
        query = {"doc_id":doc_id}
        url = "http://%s:%s/get_tokens" % (self.ip, self.port)
        r = sendCurlPost(url, query, retry=False)
        if not r: 
            return []
        return r.json()["data"]
        
    def getAllResources(self):
        query = {}
        url = "http://%s:%s/get_all_resources" % (self.ip, self.port)
        r = sendCurlPost(url, query, retry=False)
        if not r:
            return []
        return r.json()["data"]
    
    def getResourcesByID(self, res_id):
        query = {"res_id":res_id}
        url = "http://%s:%s/get_resources_by_id" % (self.ip, self.port)
        r = sendCurlPost(url, query, retry=False)
        if not r:
            return []
        return r.json()["data"]
    
    def getResources(self, doc_id, view_id):
        lang = self.getLangForDoc(doc_id)
        if lang != self.lang:
            return []
        if view_id != self.view_id:
            return []
        query = {"doc_id":doc_id, "view_id":view_id}
        url = "http://%s:%s/get_resources" % (self.ip, self.port)
        r = sendCurlPost(url, query, retry=False)
        if not r:
            return []
        return r.json()["data"]
        
    def sendTranslation(self, doc_id, seg_id, tok_id, translation):
        lang = self.getLangForDoc(doc_id)
        if lang != self.lang:
            return []
        msg = { "doc_id":doc_id, 
                "seg_id":seg_id, 
                "tok_id":tok_id,
                "translation":translation }
        url = "http://%s:%s/add_translation" % (self.ip, self.port)
        r = sendCurlPost(url, msg, retry=False)
        if not r:
            return []
        return r.json()["data"]
        
    
def init_db(db_ip, db_port, db_name):
    db.init(db_ip, db_port, db_name)
    

def sendCurlPost(url, data, username='', password='', retry=True):
    headers = {'Content-type': 'application/json'}
    data = json.dumps(data)
    is_sent = False
    while not is_sent:
        try:
            if username:
                r = requests.post(url, data=data, headers=headers, auth=(self.anno_username, self.anno_password))
            else:
                r = requests.post(url, data=data, headers=headers)
            if r.status_code != 200:
                logging.error("ERROR: Status code %s received from %s" % (r.status_code, url))
            is_sent = True
            return r
        except requests.exceptions.ConnectionError:
            logging.error("Cannot connect to %s" % url)
            if retry:
                logging.error("Retrying in 10 seconds")  
                time.sleep(10)
            else:
                is_sent = True

def check_auth(username, password):
    return username == 'lorelei' and password == 'ariel'

def requires_auth(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        auth = request.authorization
        if not auth or not check_auth(auth.username, auth.password):
            return Response(
            'Could not verify your access level for that URL.\n'
            'You have to login with proper credentials', 401,
            {'WWW-Authenticate': 'Basic realm="Login Required"'})
        return f(*args, **kwargs)
    return decorated
        
'''
class AnnotationListener:

    def __init__(self, ip, port, username='', password=''):
        self.ip = ip
        self.port = port
        self.username = username
        self.password = password
        
    def notify(self, data):
        url = "http://%s:%s/receive_annotations" % (self.ip, self.port)
        sendCurlPost(url, data, self.username, self.password)
    
class AnnotationNotifier:

    def __init__(self):
        self.listeners = defaultdict(set)  # map from annotation type to listeners interested in that annotation type
        
    def registerListener(self, ip, port, anno_types, username='', password=''):
        listener = AnnotationListener(ip, port, username, password)
        for anno_type in anno_types:
            self.listeners[anno_type].add(listener)
            logging.info("Registered %s:%s as a %s listener" % (ip, port, anno_type))
            
    def broadcast(self, user, anno_types, data):
        listeners_to_notify = set()
        for anno_type in anno_types:
            listeners_to_notify |= self.listeners[anno_type]
        for listener in listeners_to_notify:
            listener.notify({"data": {
                "user":user,
                "anno_types": anno_types,
                "items": data
            }})
'''         


@app.route("/annotations/subscribe", methods=["POST"])
@requires_auth
def annotation_subscription_request():
    request_json_obj = request.get_json()
    logging.info("Received annotation listener request: %s" % request_json_obj)
    db.add_listener({
        "ip": request_json_obj["ip"],
        "port": request_json_obj["port"],
        "anno_types": request_json_obj["anno_types"],
        "username": request_json_obj["username"] if "username" in request_json_obj else '',
        "password": request_json_obj["password"] if "password" in request_json_obj else ''
    })
    
    return json.dumps({ "success":True, "data": '', "message":'' }, default=json_util.default)
        
       
def notify_listeners(user, anno_types, items):
    items = deepcopy(items)
    anno_types = set(anno_types)
    for listener in db.get_listeners():
        if not anno_types & set(listener["anno_types"]):
            continue
            
        annotations_to_send = []
        for anno_type in listener["anno_types"]:
            for item in items:
                query = { 
                    "doc_id": item["doc"],
                    "seg_id": item["seg"],
                    "annotation_type": anno_type
                }
                item["annotations"] =  db.get_annotations(query)
                item["seg"] = "segment-" + item["seg"]
            
        url = "http://%s:%s/" % (listener["ip"], listener["port"])
        payload = { "items": items, "user":user }
        sendCurlPost(url, payload, listener["username"], listener["password"])


@app.route("/")
@requires_auth
def get_index():        
    return pageGenerator.makeLogin()
        
def get_tasks_from_file(fname):
    with open(fname, 'r', encoding="utf-8") as fin:
        tasks = json.load(fin)
        for task in tasks:
            db.add_task(task)


@app.route("/languages/")
@requires_auth
def get_languages_index():
    user_id = request.args.get("user")
    logging.info("Request for resources index from %s" % user_id)
    if not user_id:
        return get_index()
    return pageGenerator.makeLanguagesIndex(user_id)
    

@app.route("/pinned/")
@requires_auth
def get_pinned_resources_index():
    user_id = request.args.get("user")
    logging.info("Request for pinned resources index from %s" % user_id)
    if not user_id:
        return get_index()
    return pageGenerator.makePinnedResourcesIndex(user_id)
        
    
@app.route("/documents/<lang_id>/<res_id>/")
@requires_auth
def get_documents_index(lang_id, res_id):
    
    user_id = request.args.get("user")
    role_id = request.args.get("role")
    
    if not user_id:
        return get_index()
    return pageGenerator.makeDocumentsIndex(lang_id, res_id, user_id, role_id, request.full_path)
  
    
def sortTokensUnique(tokens):
    tokens.sort(key=lambda x:x["tok_offset"])
    found = set()
    result = []
    for token in tokens:
        if token["tok_offset"] in found:
            continue
        found.add(token["tok_offset"])
        result.append(token)
    return result

  
def get_parallel_download():

    doc_ids = request.form.getlist("queries")
    view_ids = request.form.getlist("view_ids")
    res_id = request.form.get("res_id")
    results = []
    
    logging.info("Doc ids = %s" % doc_ids)
    logging.info("View ids = %s" % view_ids)
    
    for doc_id in doc_ids:
        seg_index = 0
        all_tokens = defaultdict(list)
        
        for token in db.get_textitems( {"doc_id": doc_id } ):
            view_id = token["view_id"]
            seg_offset = token["seg_offset"]
            all_tokens[(view_id, seg_offset)].append(token)
        
        logging.info("tokens found = %s" % len(all_tokens))
        logging.info(all_tokens.keys()[:5])
        while True:
            done = False
            segments = []
            logging.info("View ids = %s" % view_ids)
            for view_id in view_ids:
                logging.info(view_id)
                tokens = all_tokens[(view_id,seg_index)]
                logging.info(tokens)
                if not tokens:
                    done = True
                    break
                tokens = sortTokensUnique(tokens)
                segments.append(" ".join([t["text"] for t in tokens]))
            if done:
                break
            results.append(segments)
            seg_index += 1
            
        logging.info("results found = %s" % len(results))
    
    result = "\n".join([" ||| ".join(s) for s in results])
    
    response = make_response(result)
    response.headers['Content-Type'] = 'text/plain; charset=UTF-8'
    response.headers['Content-Disposition'] = "attachment; filename=parallel_%s.txt" % res_id
    return response
    
    
@app.route("/resource_action.py", methods=["POST"])
@requires_auth
def handle_resource_action():
    action = request.form.get("action")
    if action == "Download":
        return get_parallel_download()
    elif action == "Assign":
        return assign_multiple_document_task()
    elif action == "Re-assign":
        return reassign_multiple_document_task()
    elif action == "Delete":
        return delete_multiple_document_task()
        
def chunks(l, n):
    """Yield successive n-sized chunks from l."""
    for i in xrange(0, len(l), n):
        yield l[i:i + n]
        
@app.route('/tasks/new', methods=['POST'])
@requires_auth
def receive_new_todo_list():
    request_json_obj = request.get_json()
    logging.info("Received new task list from AL server: %s" % request_json_obj) 
    lang_id = request_json_obj["lang_id"]
    res_id = request_json_obj["res_id"]
    sys_id = request_json_obj["sys_id"]
    for chunk in chunks(request_json_obj['data'], 10):
        queries = []
        for item, _ in chunk:
            seg_offset = item["seg"].split("-")[1]
            queries.append(item["doc"] + "-" + seg_offset)
        query = "--".join(queries)
        logging.info(query)
        assign_document_task("any", "ner_annotator", query, lang_id, res_id, [], sys_id, False)
    return json.dumps({ "success":True, "data": '', "message":'' })
        
        
def assign_document_task(annotator_id, annotator_role, query, lang_id, res_id, ghost_annotations, assigned_by, sandboxed):
    
    if annotator_id == 'none' or annotator_role == 'none':
        return json.dumps({ "success":False, "data": '', "message":'User and role ids required to assign task.' }, default=json_util.default)    
    
    
    logging.info("Task add request for %s, %s" % (annotator_id, annotator_role))
    
    items = []
    for subquery in query.split("--"):
        parts = subquery.split("-")
        items += [{"doc": parts[0], "seg": p} for p in parts[1:]] if len(parts) > 1 else [{"doc": parts[0]}]

    
    # if ghost_user_id != "none" and ghost_role_id != "none":
        # # ghost copy the ghost user's annotations to the assigned user
        # logging.info("Ghost copying annotations from %s to %s" % (annotator_user_id, annotator_role_id))
        # for item in items:
            # if "seg" in item:
                # db.ghost_copy_annotations(item["doc"], item["seg"], annotator_user_id, annotator_role_id, ghost_user_id, ghost_role_id)
            # else:
                # db.ghost_copy_annotations(item["doc"], None, annotator_user_id, annotator_role_id, ghost_user_id, ghost_role_id) 
    
    #task_id = hashlib.sha1(query)
    db.add_task({
    #    "task_id": task_id,
        "user": annotator_id,
        "assigned_by": assigned_by,
        "role": annotator_role,
        "lang_id": lang_id,
        "res_id": res_id,
        "items": items,
        "sandboxed": sandboxed
    })
    return json.dumps({ "success":True, "data": '', "message":'' }, default=json_util.default)    


def assign_multiple_document_task():
    user_id = request.form.get("user_id")
    redirect_url = request.form.get("redirect_url")
    annotator_id = request.form.get("annotator_user_id")
    annotator_role = request.form.get("annotator_role_id")
    queries = request.form.getlist("queries")
    sandboxed = request.form.get("sandboxed") in ["true","True"]
    ghost_annotations = request.form.getlist("ghost_annotations")
    res_id = request.form.get("res_id")
    lang_id = request.form.get("lang_id")
    
    ghosts = []
    for ghost_annotation in ghost_annotations:
        parts = ghost_annotation.split(";")
        ghosts.append({"ghost_user_id":parts[0],"ghost_role_id":parts[1]})
    
    for query in queries:
        assign_document_task(annotator_id, annotator_role, query, lang_id, res_id, ghosts, user_id, sandboxed)
    
    if annotator_id == "any":
        mail_result = mailer.send_generic_task_notification_email(len(queries), server_address, args.port)
    else:
        mail_result = mailer.send_task_notification_email(annotator_id, len(queries), server_address, args.port)
    if not mail_result["success"]:
        logging.info("ERROR: %s" % mail_result["msg"])
    
    #msg = "Task(s) assigned to %s" % annotator_id
    return redirect(redirect_url)


def reassign_document_task(annotator_id, annotator_role, query, lang_id, res_id, assigned_by, sandboxed):
    
    if annotator_id == 'none' or annotator_role == 'none':
        return json.dumps({ "success":False, "data": '', "message":'User and role ids required to assign task.' }, default=json_util.default)    
    
    logging.info("Task reassign request for %s, %s" % (annotator_id, annotator_role))
    
    items = []
    for subquery in query.split("--"):
        parts = subquery.split("-")
        items += [{"doc": parts[0], "seg": p} for p in parts[1:]] if len(parts) > 1 else [{"doc": parts[0]}]

    db.delete_task({
        "role": annotator_role,
        "lang_id": lang_id,
        "res_id": res_id,
        "items": items,
        "finished": {"$ne": True}
    })
    
    db.add_task({
        "user": annotator_id,
        "assigned_by": assigned_by,
        "role": annotator_role,
        "lang_id": lang_id,
        "res_id": res_id,
        "items": items,
        "sandboxed": sandboxed
    })
    return json.dumps({ "success":True, "data": '', "message":'' }, default=json_util.default)    


def reassign_multiple_document_task():
    user_id = request.form.get("user_id")
    redirect_url = request.form.get("redirect_url")
    annotator_id = request.form.get("annotator_user_id")
    annotator_role = request.form.get("annotator_role_id")
    queries = request.form.getlist("queries")
    sandboxed = request.form.get("sandboxed") in ["true","True"]
    res_id = request.form.get("res_id")
    lang_id = request.form.get("lang_id")
    
    for query in queries:
        reassign_document_task(annotator_id, annotator_role, query, lang_id, res_id, user_id, sandboxed)
    
    return redirect(redirect_url)


def delete_document_task(annotator_id, annotator_role, query, lang_id, res_id, assigned_by, sandboxed):
    
    if annotator_id == 'none' or annotator_role == 'none':
        return json.dumps({ "success":False, "data": '', "message":'User and role ids required to assign task.' }, default=json_util.default)    
    
    logging.info("Task delete request for %s, %s" % (annotator_id, annotator_role))
    
    items = []
    for subquery in query.split("--"):
        parts = subquery.split("-")
        items += [{"doc": parts[0], "seg": p} for p in parts[1:]] if len(parts) > 1 else [{"doc": parts[0]}]
    
    db.delete_task({
        "user": annotator_id,
        "role": annotator_role,
        "lang_id": lang_id,
        "res_id": res_id,
        "items": items,
        "finished": {"$ne": True}
    })
    return json.dumps({ "success":True, "data": '', "message":'' }, default=json_util.default)    


def delete_multiple_document_task():
    user_id = request.form.get("user_id")
    redirect_url = request.form.get("redirect_url")
    annotator_id = request.form.get("annotator_user_id")
    annotator_role = request.form.get("annotator_role_id")
    queries = request.form.getlist("queries")
    sandboxed = request.form.get("sandboxed") in ["true","True"]
    res_id = request.form.get("res_id")
    lang_id = request.form.get("lang_id")
    
    for query in queries:
        delete_document_task(annotator_id, annotator_role, query, lang_id, res_id, user_id, sandboxed)
    
    return redirect(redirect_url)


@app.route("/users/")
@requires_auth
def get_user_index():
    user_id = request.args.get("user")
    if not user_id:
        return get_index()
    return pageGenerator.makeUserIndex(user_id)
  
@app.route("/tasks/")
@requires_auth
def get_tasks_index():
    user_id = request.args.get("user")
    if not user_id:
        return get_index()
    return pageGenerator.makeTasksIndex(user_id)
    

@app.route("/register/")
@requires_auth
def register_user():
    user_id = request.args.get("user")
    email = request.args.get("email")
    permissions = request.args.get("permissions")
    if not user_id:
        return get_index()
    user_info = db.get_user_info(user_id)
    if not user_info:
        user_info = {"user": user_id }
    user_info["email"] = email
    user_info["permissions"] = permissions
    user_info["subscribed"] = False
    db.update_user(user_info)
    return redirect("/tasks/?user=%s" % user_id)
        
    
@app.route("/tasks/subscribe")
@requires_auth
def tasks_subscribe():
    user_id = request.args.get("user")
    email = request.args.get("email")
    if not user_id:
        return get_index()
    user_info = db.get_user_info(user_id)
    if not user_info:
        user_info = {"user": user_id, 
                    "permissions": "intermediate" }
    user_info["email"] = email
    user_info["subscribed"] = True
    db.update_user(user_info)
    mail_result = mailer.send_subscribe_email(user_id, server_address, args.port)
    if mail_result["success"] == False:
        logging.info("ERROR: %s" % mail_result["msg"])
    return ("%s, you have choosen to receive task notifications to your email address %s.<br>" % (user_id, user_info["email"]) +
           "<a href=/tasks/unsubscribe?user=%s>Unsubscribe</a><br>" % user_id +
           "<a href='javascript:history.back();'>[Go Back]</a>")
    
@app.route("/tasks/unsubscribe")
@requires_auth
def tasks_unsubscribe():
    user_id = request.args.get("user")
    if not user_id:
        return get_index()
    user_info = db.get_user_info(user_id)
    if user_info:
        user_info["subscribed"] = False
        db.update_user(user_info)
        return ("%s, you will no longer be emailed task notifications.<br>" % user_id +
               "<a href=/tasks/subscribe?user=%s&email=%s>Re-subscribe</a><br>" % (user_id, user_info["email"]) +
               "<a href='javascript:history.back();'>[Go Back]</a>")
    else:
        return "User info not found!"
    
@app.route("/view.py")
@requires_auth
def get_view_page():
    user_id = request.args.get("user")
    assign_user = request.args.get("assign")
    role_id = request.args.get("role")
    query = request.args.get("q")
    
    # if this is a task claim, reassign it to the user
    if user_id == "any" and assign_user:
        items = get_items_from_query(query)
        db.reassign_task(assign_user, role_id, items)
        user_id = assign_user
    
    logging.info("Retrieving page for query %s" % query)
    if not user_id:
        return get_index()
    return pageGenerator.makeViewPage(query, user_id, role_id)
    

@app.route("/reassign_task.py")
@requires_auth
def reassign_task():
    user_id = request.args.get("user")
    role_id = request.args.get("role")
    query = request.args.get("q")
    
    # make sure this is a task claim

    items = get_items_from_query(query)
    db.reassign_task(user_id, role_id, items)
        
    return redirect("view.py?q=%s&user=%s&role=%s" % (query, user_id, role_id))
    
    
@app.route("/add_translation.py", methods=["POST"])
@requires_auth
def add_translation():
    data = request.form.get('data')
    data = json.loads(data)
    logging.info("Translation request from %s in role %s for %s-%s" % (data["user"], data["role"], data["doc_id"], data["seg_id"]))
    logging.info(data["translation"])
    return json.dumps({ "success":True, "data":{}, 'message':''})
    

MAX_DEFINITIONS = 4


def get_entity_links(user_id, doc_id):
    results = []
    entityLinkQuery = { "doc_id": doc_id, "annotation_type": "edl" }
    existingEntityLinkCandidates = set()
    for i, entityLink in enumerate(db.get_entity_links(entityLinkQuery)):
        id = entityLink["annotation_label"]
        if id in existingEntityLinkCandidates:
            continue
        existingEntityLinkCandidates.update([id])
        kb_entity = db.get_entity_by_id(id)
        ent_text = id
        if kb_entity:
            ent_name = kb_entity[0]["name"]
            if "country" in kb_entity[0]:
                country_code = kb_entity[0]["country"]
                try:
                    country = pycountry.countries.get(alpha_2=country_code)
                    ent_name += ", " + country.name
                except:
                    # must be a dummy code
                    pass
            ent_text += ": %s" % ent_name
        results.append({"ent_index":i, "ent_id":id, "ent_text":ent_text })
    return sorted(results, key=lambda x:x["ent_id"])


@app.route("/user_roles.py", methods=["POST"])
@requires_auth
def get_annotation_schema():
    data = request.form.get('data')
    data = json.loads(data)
    logging.info("Annotation schema request from %s in role %s for %s-%s" % (data["user"], data["role"], data["doc_id"], data["seg_id"]))
    
    #tokens = libraries[data["set_id"]].getTokens(data["doc_id"], data["seg_id"], data["tok_id"])
    annotationSchema = user_roles.getAnnotationSchema(data["user"], data["role"], data["doc_id"], data["seg_id"], data["tok_id"])
    annotationSchema["definitions"] = []
    
    #if len(tokens) < 5 and len(tokens) > 0:
    #    definitions = lexica[data["set_id"]].defineTokens(tokens)
    #    for i, (definition, penalty) in enumerate(definitions[:MAX_DEFINITIONS]):
    #        annotationSchema["definitions"].append({"def_index":i, "def_text":definition})
    
    trueEntityLinkQuery = { "user": data["user"], "doc_id": data["doc_id"], "seg_id": data["seg_id"], "tok_id":data["tok_id"], "annotation_type":"edl" }
    trueEntityLinks = db.get_entity_links(trueEntityLinkQuery)
    trueEntityLinkIDs = [t["annotation_label"] for t in trueEntityLinks]
        
    entityLinkCandidates = get_entity_links(data["user"], data["doc_id"])
    for candidate in entityLinkCandidates:
        candidate["ent_checked"] = candidate["ent_id"] in trueEntityLinkIDs
    annotationSchema["entityLinks"] = entityLinkCandidates
    
    trueSfLocationLinkQuery = { "user": data["user"], "doc_id": data["doc_id"], "seg_id": data["seg_id"], "tok_id":data["tok_id"], "annotation_type": "sf_location" }
    trueSfLocationLinkIDs = [ link["annotation_label"] for link in db.get_entity_links(trueSfLocationLinkQuery) ]
    sfLocationCandidates = get_entity_links(data["user"], data["doc_id"])
    for candidate in sfLocationCandidates:
        candidate["ent_checked"] = candidate["ent_id"] in trueSfLocationLinkIDs
    annotationSchema["sf_location_candidates"] = sfLocationCandidates
    
    try:
        return json.dumps({ "success":True, "data": annotationSchema, "message":'' })
    except:
        return json.dumps({ "success":False, "message":"Cannot retrieve annotation schema for role " + data["role"] })

        
def parseDocQuery(queryStr):
    segmentsByDoc = {}
    for subquery in queryStr.split("--"):
        parts = subquery.split("-")
        segmentsByDoc[parts[0]] = parts[1:] if len(parts) > 1 else '*'
    return segmentsByDoc

    
def add_annotation(annotationRequests):
    ''' Add an annotationRequest object to the datastore, and return all valid annotations for the requested user/role/document. '''

    for annotationRequest in annotationRequests["annotations"]:
        annotationRequest["user"] = annotationRequests["user"]
        annotationRequest["role"] = annotationRequests["role"]
        annotationRequest["user_type"] = "human"
        annotationRequest["ghost"] = False
    
        if "delete" in annotationRequest and annotationRequest["delete"]:
            delete_query = {k: annotationRequest[k] for k in ["doc_id", "seg_id", "tok_id", "annotation_type", "annotation_label"]}
            db.delete_annotations(delete_query)
        else:
            db.add_annotation(annotationRequest)
            
        if annotationRequest["annotation_type"] == "translation":
            for data_connection in data_connections:
                data_connection.sendTranslation(
                    annotationRequest["doc_id"],
                    annotationRequest["seg_id"],
                    annotationRequest["tok_id"],
                    annotationRequest["annotation_label"])
                    

def get_all_annotations(annotation_query, role_permissions):
    role = annotation_query["role"]
    visibleTypes = role_permissions[role]["view_permissions"] if role in role_permissions else []
    results = []
    
    for doc_id, seg_ids in parseDocQuery(annotation_query["query"]).items():
        if seg_ids == '*':
            db_query = { "doc_id":doc_id, 
                         "annotation_type": visibleTypes }
        else:
            
            seg_ids = ["segment-%s" % s for s in seg_ids]
            db_query = { "doc_id":doc_id, 
                         "annotation_type": visibleTypes,
                         "seg_id":seg_ids }
                         
        results += db.get_annotations(db_query)

    for annotation in results:
        annotation["anno_view_id"] = annotation["user"].lower().replace(" ","_") + "_" + annotation["annotation_type"]
    return results

        
@app.route("/add_annotation.py", methods=["POST"]) 
@requires_auth
def annotation_request_response():  
    data = request.form.get('data')
    data = json.loads(data)
    with open("config/role_permissions.json", "r", encoding="UTF-8") as fin:
        role_permissions = json.load(fin)
        
    add_annotation(data)
    responseData = {
        "annotations": get_all_annotations(data, role_permissions),
        "schemas": user_roles.get_basic_schemas()
    }
    return json.dumps({ "success":True, "data": responseData, "message":'' }, default=json_util.default)

@app.route("/log_action.py", methods=["POST"]) 
@requires_auth
def log_action():  
    data = request.form.get('data')
    data = json.loads(data)
    db.log_action(data)
    return json.dumps({"success":True, "message":''}, default=json_util.default)

    
# @app.route("/add_entity_link.py", methods=["POST"])
# @requires_auth
# def add_entity_link():
    # data = request.form.get('data')
    # data = json.loads(data)
    # assert("user" in data)
    # assert("doc_id" in data)
    # assert("seg_id" in data)
    # assert("tok_id" in data)
    # assert("annotation_type" in data)
    # assert("annotation_label" in data)
    # logging.info("Annotation received: %s from %s for %s-%s" % (data["annotation_type"], data["user"], data["doc_id"], data["seg_id"]))
    # if "delete" in data and data["delete"]:
        # db.remove_entity_link(data)
    # else:
        # db.add_entity_link(data)
    # return json.dumps({ "success":True, "data":{}, 'message':''})    
    

# @app.route("/add_task.py", methods=["POST"])
# @requires_auth
# def add_task_request():
    # ''' The front-end sends this when an admin assigns a task to a user '''
       
    # annotator_user_id = request.form.get("annotator_user_id")
    # annotator_role_id = request.form.get("annotator_role_id")
    # ghost_user_id = request.form.get("ghost_user_id")
    # ghost_role_id = request.form.get("ghost_role_id")
    # query = request.form.get("query")
    # lang_id = request.form.get("lang_id")
    # res_id = request.form.get("res_id")
    
    # if annotator_user_id == 'none' or annotator_role_id == 'none':
        # return json.dumps({ "success":False, "data": '', "message":'User and role ids required to assign task.' }, default=json_util.default)    
    
    # logging.info("Task add request for %s, %s" % (annotator_user_id, annotator_role_id))
    
    # items = []
    # for subquery in query.split("--"):
        # parts = subquery.split("-")
        # items += [{"doc": parts[0], "seg": p} for p in parts[1:]] if len(parts) > 1 else [{"doc": parts[0]}]
    
    # if ghost_user_id != "none" and ghost_role_id != "none":
        # # ghost copy the ghost user's annotations to the assigned user
        # logging.info("Ghost copying annotations from %s to %s" % (annotator_user_id, annotator_role_id))
        # for item in items:
            # if "seg" in item:
                # db.ghost_copy_annotations(item["doc"], item["seg"], annotator_user_id, annotator_role_id, ghost_user_id, ghost_role_id)
            # else:
                # db.ghost_copy_annotations(item["doc"], None, annotator_user_id, annotator_role_id, ghost_user_id, ghost_role_id)
    # db.add_task({
        # "user": annotator_user_id,
        # "role": annotator_role_id,
        # "ghost_user": ghost_user_id,
        # "ghost_role": ghost_role_id,
        # "lang_id": lang_id,
        # "res_id": res_id,
        # "items": items
    # })
    # return json.dumps({ "success":True, "data": '', "message":'' }, default=json_util.default)    
  
  
def get_items_from_query(query):
    items = []
    for subquery in query.split("--"):
        parts = subquery.split("-")
        items += [{"doc": parts[0], "seg": p} for p in parts[1:]] if len(parts) > 1 else [{"doc": parts[0]}]
    return items
    
  
@app.route("/tasks/finished")
@requires_auth
def task_finished_request():
    ''' The front-end sends this when a user clicks the "Finished" button on each page. '''
    
    
    user_id = request.args.get("user")
    role_id = request.args.get("role")
    query = request.args.get("q")
    

    
    items = get_items_from_query(query)
    logging.info("Task finished request from %s in role %s for document(s) %s" % (user_id, role_id, items) )
    
    db_query = {
        "user": user_id,
        "role": role_id,
        "items": items
    }
    
    
    with open("config/role_permissions.json", "r", encoding="UTF-8") as fin:
        role_permissions = json.load(fin)  
        task_name = role_permissions[role_id]["task_name"]
    
    anno_types = role_permissions[role_id]["modify_permissions"]
    notify_listeners(user_id, anno_types, items)
    
    # notify the assigner that the task is done
    for task in db.get_tasks(db_query):
    
        if "assigned_by" in task:
            assigner = task["assigned_by"]
            mail_result = mailer.send_task_finished_email(assigner, user_id, task_name, query)
            if mail_result["success"] == False:
                logging.info("ERROR: %s" % mail_result["msg"])
    
    # finish the task
    db.set_task_finished(db_query)
    
    return redirect("/tasks/?user=%s" % user_id)
    
    
    
@app.route("/annotations/dump", methods=["POST"])
@requires_auth
def handle_annotation_dump_request():
    anno_types = request.json["anno_types"]
    results = dump_annotation_results(anno_types)
    return json.dumps({"success":True, "data":results, "message":''}, default=json_util.default)

    
def dump_annotation_results(anno_types, finished=False):
    anno_types = request.json["anno_types"]
    results = []
    for anno_type in anno_types:
        for anno in db.get_annotations({"annotation_type":anno_type}):
            result = { k:anno[k] for k in anno if k in ["user","doc_id", "seg_id", "tok_id", "annotation_type", "annotation_label"] }
            results.append(result)
    return results


    
if __name__ == '__main__':
    argparser = argparse.ArgumentParser()
    argparser.add_argument("--manifestDir", default="config/manifests/", help="JSON manifest directory")
    argparser.add_argument("--port", type=int, default=5000, help="Port for serving the files.")
    argparser.add_argument("--db_ip", default="127.0.0.1", help="database IP")
    argparser.add_argument("--db_port", type=int, default=27017, help="database port")
    argparser.add_argument("--db_name", default="lorelei_viewer", help="database name")
    argparser.add_argument("--reload_kb", default="", help="Reload the database from specified JSON file")
    argparser.add_argument("--servers", help="JSON manifest for servers")
    args = argparser.parse_args()
    
    data_connections = []
    if args.servers:
        with open(args.servers,"r", encoding="utf-8") as fin:
            dcjsons = json.load(fin)
            for dcjson in dcjsons:
                data_connections.append(DataConnection(
                    dcjson["ip"], dcjson["port"], dcjson["lang"], dcjson["view_id"]        
                ))
    
    
    init_db(args.db_ip, args.db_port, args.db_name)
    server_address = "lor.lti.cs.cmu.edu"
    pageGenerator = PageGenerator(db, data_connections)
    mailer = Mailer(db, flask_mail)
    
    if (args.reload_kb):
        logging.info("Reloading entities")
        start = time.time()
        db.load_entities(args.reload_kb)
        end = time.time()
        logging.info("Entities loaded, elapsed time = %ss" % (end-start))
    
    #libraries = {}
            
    #get_tasks_from_file("tasks/tasks.json")        
    http_server = WSGIServer(('', args.port), app)
    http_server.serve_forever()
    
    #app.run(host='0.0.0.0', port=int(args.port))
