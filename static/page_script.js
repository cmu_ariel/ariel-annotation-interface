$( document ).ready(function() {

    var user = $("body").attr("data-user-id");
    var role = $("body").attr("data-role-id");
    
    var annotations = [];
    var annotation_view_id_whitelist = [];
    
    $("#nav-icon").click(function() {
        if ($(".view-pane").is(":visible")) {
            $(".view-pane").hide();
            //$(".user-pane").hide();
            //$(".taskdiv").show();
            //$(".donelink").show();
        } else {
            //$(".taskdiv").hide();
            //$(".user-pane").hide();
            $(".view-pane").show();
            //$(".donelink").hide();
        }
    });
    
    $("#user-icon").click(function() {
        if ($(".user-pane").is(":visible")) {
            $(".user-pane").hide();
            //$(".left_nav").hide();
            //$(".taskdiv").show();
            //$(".donelink").show();
        } else {
            //$(".taskdiv").hide();
            //$(".left_nav").hide();
            $(".user-pane").show();
            //$(".donelink").hide();
        }
    });

    function seg_click_handler(segment) {
        clear(0);
        var doc_id = segment.parent().data("doc-id");
        var seg_id = segment.parent().data("seg-id");
        if (!segment.hasClass("clicked"))
            sendMessageToServer(
                "/log_action.py", 
                {action: "seg_annotation_start", user: user, role: role, doc_id: doc_id, seg_id: seg_id},
                function() {}
            );    
        segment.addClass("bold");
        segment.addClass("clicked");
        segment.parent().addClass("segment-selected");
        segment.parent().find(".hitoken").removeClass("blur");
        var start_id = segment.parent()
        .find("span.hitoken.highlight-start[data-view_id='original']")
        .first().data("tok-id")
        var end_id = segment.parent()
        .find("span.hitoken.highlight-end[data-view_id='original']")
        .first().data("tok-id")
        console.log(segment.offset().top);
        if (!start_id || !end_id)
        {
            $([document.documentElement, document.body]).animate({
                    scrollTop: segment.offset().top
            }, 100);
            return;
        }
        AnnotationPainter.setSelection(doc_id, seg_id, "original", start_id.split(","), end_id.split(","), upCallback);
    };


    var cur_segment = null;
    $(".segmarker").click(function() {
        cur_segment = $(this);
        if (Cookies.get("type-blur") == "false")
            return;
        if ($(this).hasClass("clicked")) {
            $(this).removeClass("bold");
            $(this).removeClass("clicked");
            $(this).parent().find(".hitoken").addClass("blur");
        } else {
            //$(this).addClass("bold");
            //$(this).addClass("clicked");
            //$(this).parent().find(".hitoken").removeClass("blur");
            //var doc_id = $(this).parent().data("doc-id");
            //var seg_id = $(this).parent().data("seg-id");
            //sendMessageToServer(
                //"/log_action.py", 
                //{action: "seg_annotation_start", user: user, role: role, doc_id: doc_id, seg_id: seg_id},
                //function() {}
            //);    
            //var start_id = $(this).parent()
            //.find("span.hitoken.highlight-start[data-view_id='original']")
            //.first().data("tok-id")
            //var end_id = $(this).parent()
            //.find("span.hitoken.highlight-end[data-view_id='original']")
            //.first().data("tok-id")
            //if (!start_id || !end_id)
                //return;
            //AnnotationPainter.setSelection(doc_id, seg_id, "original", start_id.split(","), end_id.split(","), upCallback);
            seg_click_handler($(this));
        }
    });

    key('shift+down', function() {
        let selection = AnnotationPainter.getSelection();
        if (selection)
            cur_segment = $(".segmarker").filter(function(){return $(this).text() === selection.seg_id;});
        if (cur_segment === null)
            cur_segment = $(".segmarker").first();
        else {
            next_segment = cur_segment.parent().next().children(".segmarker").first();
            if (next_segment.length !== 0)
                cur_segment = next_segment;
        }
        seg_click_handler(cur_segment);
        return false;
    });

    key('shift+up', function() {
        let selection = AnnotationPainter.getSelection();
        if (selection)
            cur_segment = $(".segmarker").filter(function(){return $(this).text() === selection.seg_id;});
        if (cur_segment === null)
            return false;
        prev_segment = cur_segment.parent().prev().children(".segmarker").first();
        if (prev_segment.length === 0)
            return false;
        else
            cur_segment = prev_segment;
        seg_click_handler(cur_segment);
        return false;
    });

    $(".segmarker").hover(
        function() {
            if (Cookies.get("type-blur") == "false")
                return;
            if (!$(this).hasClass("clicked")) {
                $(this).addClass("bold");
                $(this).parent().find(".hitoken").removeClass("blur");
        }},
        function() {
            if (Cookies.get("type-blur") == "false")
                return;
            if (!$(this).hasClass("clicked")) {
                $(this).removeClass("bold");
                $(this).parent().find(".hitoken").addClass("blur");
        }}
    );

    /*
    $(".finished").click(function() {
        sendMessageToServer(
            "/task_finished.py",
            { "user": user,
              "role": role,
              "query": query,
              "set_id": set_id
            },
            function() { }
        );  
    }); */
    
    /* Here, we check the cookies to see what should and shouldn't be checked */
    $(".show-check").each( function( index, element ){
        var classList = $(this).attr("class").split(/\s+/);
        $.each(classList, function(i,c) {
            if (c !== 'show-check') {
                var cookieVal = Cookies.get(c);
                if (cookieVal == 'true') {
                    $(element).prop("checked", true);
                } else if (cookieVal == 'false') {
                    $(element).prop("checked", false);
                }
                if(!$(element).is(":checked")) {
                    $("tr."+c).hide();
                }
            }
        });
    });
    
    /* Here, we set up the "show-check" slider checkboxes so that the user can
       turn on and off different representations of the text */
    $(".show-check").click(function() {
        var that = this;
        var classList = $(this).attr("class").split(/\s+/);
        $.each(classList, function(i,c) {
            if (c !== 'show-check') {
                if($(that).is(":checked")) {
                    $("tr."+c).show(200);
                    Cookies.set(c, 'true');
                } else {
                    $("tr."+c).hide(200);
                    Cookies.set(c, 'false');
                }
            }
        });
    });

    //NOTE: this must come after the .show-check event handler
    function refreshNERlegend() {
        if (Cookies.get("type-ner-legend") == "false") {
            $(".ner-legend-pane").hide();
        } else {
            $(".ner-legend-pane").show();
        }
    }
    $("#type-ner-legend").click(refreshNERlegend);
    refreshNERlegend();

    //NOTE: this must come after the .show-check event handler
    function blurSegments() {
        if (Cookies.get("type-blur") == "false") {
            $(".hitoken").removeClass("blur")
        } else {
            $(".hitoken").addClass("blur")
        }
    }
    $("#type-blur").click(blurSegments);
    blurSegments();

    /* Here, we check the cookies to see what should and shouldn't be checked w.r.t annotations */
    $(".show-check-anno").each( function( index, element ){
        var anno_view_id = $(this).attr("data-anno-view-id");
        var cookieVal = Cookies.get(anno_view_id);
        if (cookieVal == 'true' || cookieVal == undefined) {
            showAnnoView(anno_view_id);
        } else {
            hideAnnoView(anno_view_id);
        }
        if(!$(element).is(":checked")) {
            $("tr."+c).hide();
        }
    });
    
    /* Here, we set up the "show-check-anno" slider checkboxes so that the user can
       turn on and off annotations from different users and types */
    $(".show-check-anno").click(function() {
        var that = this;
        var anno_user = $(this).attr("data-anno-user");
        var anno_type = $(this).attr("data-anno-type");
        var anno_view_id = $(this).attr("data-anno-view-id");
        if ($(this).is(":checked")) {
            showAnnoView(anno_view_id);
            Cookies.set(anno_view_id, 'true');
        } else {
            hideAnnoView(anno_view_id);
            Cookies.set(anno_view_id, 'false');
        }
    });
    
    
    
    function showAnnoView(view_id) {
        annotation_view_id_whitelist[view_id] = true;
        refreshAnnotationDisplay();
    }
    
    function hideAnnoView(view_id) {
        annotation_view_id_whitelist[view_id] = false;
        refreshAnnotationDisplay();
    }
    
    function sendMessageToServer(url, data, successCallback) {
        var jsonData = { "data": JSON.stringify(data) };
        $.post(url, jsonData, function(result) {
            result = JSON.parse(result);
            if (result.success) {
                successCallback(result.data);
            } else {
                alert("ERROR: " + result.message);
            }
        });
    }
    
    function sendAnnotationSchemaRequest(user, role, doc_id, seg_id, tok_id, view_id) {
        var annotationObject = {
            user: user,
            role: role, 
            set_id: set_id,
            doc_id: doc_id,
            seg_id: seg_id,
            tok_id: tok_id,
            view_id: view_id
        };
        
        sendMessageToServer("/user_roles.py", annotationObject, function(result) {
            annotationSchema = result;
            $.extend(annotationObject, result);
            loadAnnotationFrame(annotationObject);
        });
    }
    
    function makeOnClick(exclusive, annotationClass, annotationLabel, annotationNoneID) {
        
        if (exclusive) {
            return function() {
               var id = $(this).attr("id");
               if ($('#'+id).is(":checked")) {
                   $('input.'+annotationClass+":not(#"+id+")").attr('checked', false);
               }
            };    
        } else {
            return function() {
               var id = $(this).attr("id");
               if (id == annotationNoneID) {
                   if ($("#"+id).is(":checked")) {
                       $('input.'+annotationClass+":not(#"+id+")").attr('checked', false);
                   }
               } else {
                    $("#"+annotationNoneID).attr('checked', false);                              
               }
            };
        }
    }
    
    function sendAnnotationToServer(annotationType, annotationLabel, delete_value) {
        
        currentSelection = AnnotationPainter.getSelection();
        
        if (!currentSelection) {
            return;
        }
        
        var data = {
            "user": user,
            "role": role,
            "query": query,
            "sandboxed": sandboxed,
            "annotations": [ {
                "doc_id": currentSelection.doc_id,
                "seg_id": currentSelection.seg_id,
                "tok_id": currentSelection.tok_id,
                "annotation_type": annotationType,
                "annotation_label": annotationLabel,
                "delete": delete_value
            } ]
        };
        
        sendMessageToServer(
            "/add_annotation.py", 
            data,
            updateAnnotationDisplay
        );    
    }
        
    function updateAnnotationDisplay(result) {
        
        var annotation_names = {};
        if (!AnnotationPainter.hasColors()) {
            for (var i = 0; i < result["schemas"].length; i++) {
                var schema = result["schemas"][i];
                if (!hasKey("name_abbrev", schema)) {
                    continue;
                }
                if (hasKey("color", schema)) {
                    AnnotationPainter.setColor(schema["name_abbrev"], schema["color"])
                } else {
                    AnnotationPainter.setColor(schema["name_abbrev"], "red");
                }
                annotation_names[schema["name_abbrev"]] = schema["name"]
                if (hasKey("labels", schema)) {
                    for (var j = 0; j < schema["labels"].length; j++) {
                        var label = schema["labels"][j];
                        annotation_names[schema["name_abbrev"] + ':' + label["label_abbrev"]] = label["label"]
                        if (hasKey("color", label)) {
                            AnnotationPainter.setColor("label_" + label["label_abbrev"], label["color"])
                        } else {
                            AnnotationPainter.setColor("label_" + label["label_abbrev"], "red");
                        }
                    }
                }
            }
            if (Object.keys(annotation_names).length > 0) {
                AnnotationPainter.setAnnotationNames(annotation_names);
            }
        }
        
        annotations = result["annotations"];
        refreshAnnotationDisplay();
    }
    
    function refreshAnnotationDisplay() {
        
        var annotationsToDisplay = [];
        
        $.each(annotations, function(i, anno) {
            var anno_view_id = anno["anno_view_id"];
            if (annotation_view_id_whitelist[anno_view_id] != false) {
                annotationsToDisplay.push(anno);
            }
        });
        
        AnnotationPainter.updateAnnotations(annotationsToDisplay);
        
    }
    
    function sendInitialAnnotationRequest() {
        
        var data = {
            user: user,
            role: role,       
            query: query,
            sandboxed: sandboxed,
            annotations: []
        }
        
        sendMessageToServer(
            "/add_annotation.py", 
            data,
            updateAnnotationDisplay
        );    
    }
        
    
    /*
    function getSelectionAncestorData(dataType) {
        var ancestor = window.getSelection().getRangeAt(0).commonAncestorContainer;
        var attrName = "data-" + dataType;
        result = $(ancestor).attr(attrName);
        while (result === undefined) {
            ancestor = ancestor.parentNode;
            result = $(ancestor).attr(attrName);
        }
        return result;
    } */
    
    /*
    function getTextOfSelection() {
        var fragment = window.getSelection().getRangeAt(0).cloneContents(); 
        return fragment.textContent;
    } */
    
    
    function showAnnotationInterface(doc_id, seg_id) {
        var interlinSelector = ".interlin.segment-" + doc_id + "-" + seg_id;
        var annotatorSelection = interlinSelector + " .annotation-form";
        $(interlinSelector).addClass("segment-selected");
        $(annotatorSelection).slideDown(100, "swing", function() {
            $([document.documentElement, document.body]).animate({
                    scrollTop: $(interlinSelector + "> p.segmarker").offset().top
            }, 100);
        });
    }
    
    function hideAnnotationInterface(delay, callback) {
        if (typeof delay === undefined) delay = 100;
        callback = callback || function(){};
        $(".interlin").removeClass("segment-selected");
        $(".annotation-form").slideUp(delay, callback);
        $(".annotation-form").html('');       
    }

    $(document).keyup(function(e) {
       if (e.keyCode == 27) { // escape key maps to keycode `27`
          clear(0);
       }
    });
    
    $('body').mouseup(function() {
        if (event.which == 1) {
            clear(0);
        }
    });
    
    function clear(delay, callback) {
        if (typeof delay === undefined) delay = 100;
        callback = callback || function(){};
        AnnotationPainter.clearSelection();
        hideAnnotationInterface(delay, callback);
    }
    
    
    function makeAnnotationButtonClick(annotationType, annotationLabel) {
        return function() {
            var selector = "#" + $(this).attr("for");
            var requestDelete = $(selector).is(":checked");  
            sendAnnotationToServer(annotationType, annotationLabel, requestDelete);
        }; 
    }
    
    function loadAnnotationFrame(annotationItem) {
        //$(".annotation-form").html('');
        var template = $('#form-template').html();
        template = template.replace(/\[\[/g,"{{").replace(/\]\]/g,"}}");
        var rendered = Mustache.render(template, annotationItem);
        
        selector = ".segment-" + annotationItem.doc_id + "-" + annotationItem.seg_id + ' .annotation-form';
        $(selector).html(rendered);
        
        // set up events
        for (var i = 0; i < annotationItem.schemas.length; i++) {
            let annotationType = annotationItem.schemas[i];
            let annotationClass = "annotation_" + annotationType.name_abbrev;
            let annotationNoneID = "annotation_" + annotationType.name_abbrev + "_none";
            let funcOnClick = makeOnClick(annotationType.exclusive, annotationClass, annotationNoneID);
            $("."+annotationClass).click(funcOnClick);
            
            for (var j = 0; j < annotationType.labels.length; j++) {
                let annotationLabel = annotationType.labels[j];
                let id = "annotation_label_" + annotationType.name_abbrev + "_" + annotationLabel.label_abbrev;
                let funcAnnotationButtonClick = makeAnnotationButtonClick(annotationType.name_abbrev, annotationLabel.label_abbrev);
                $("#"+id).click(funcAnnotationButtonClick);
                if("hotkey" in annotationLabel) {
                    key.unbind(annotationLabel.hotkey);
                    key(annotationLabel.hotkey, function() {
                        $("#"+id).trigger("click");
                        var selector = "#" + $("#"+id).attr("for");
                        //console.log(selector);
                        //$(selector).trigger("click");
                        funcOnClick.call($(selector));
                    });
                }
            }
        }
        
        $('#translation_free').on("change keypress paste submit", translationChanged);
        
        
        $('.entity-link-checkbox').click(function() {
            var id = $(this).attr("id");
            /* if ($('#'+id).is(":checked")) {
               $('.entity-link-checkbox:not(#'+id+')').attr('checked', false);
            } */
            if (id == 'entity_link_free') {
                newValue = $("#entity_link_area").val();
            } else if ($('#'+id).is(":checked")) {
                newValue = $(this).attr("value");
                sendAnnotationToServer("edl", newValue, false);
            } else {
                newValue = $(this).attr("value");
                sendAnnotationToServer("edl", newValue, true);
            }
        });
        
        $('#entity_link_area').on("change keypress paste", entityLinkChanged);
           
        $('.sf-location-link-checkbox').click(function() {
            var id = $(this).attr("id");
            var newValue = $(this).attr("value");
            if ($('#'+id).is(":checked")) {
                sendAnnotationToServer("sf_location", newValue, false);
            } else {
                sendAnnotationToServer("sf_location", newValue, true);
            }
        });
           
        
        //$("#entity_link_area").mouseup(function(event) {
        //    event.stopPropagation();
        //});
        
        $(".anno-pane").mouseup(function(event) {
            event.stopPropagation();
        });
        
        var selectedAnnotations = AnnotationPainter.getSelectedAnnotations();
        $.each(selectedAnnotations, function(i, annotation) {
            var checkboxSelector = "annotation_" + annotation.annotation_type + "_" + annotation.annotation_label;
            $("#" + checkboxSelector).prop("checked", true);
        }); 
    }
    
    /**************************/
    /* Translation and entity link submission */
    /**************************/
     
    
    
    
        
    
    var entityLinkValue = "";
    function entityLinkChanged(event) {
        if (event.type == 'change' || event.keyCode === 10 || event.keyCode === 13) {     
            event.preventDefault();
            var currentVal = $(this).val();
            if (currentVal == '') {
                return;
            }
            entityLinkValue = currentVal;
            //$(".entity-link-checkbox").prop("checked", false);
            $("#entity_link_free").prop("checked", true);
            sendAnnotationToServer("edl", entityLinkValue, false);
            return;
        }
        
        var currentVal = $(this).val();
        if(currentVal == entityLinkValue) {
            return; //check to prevent multiple simultaneous triggers
        }

        entityLinkValue = currentVal;
        
    }
    
    
    var translationValue = "";
    function translationChanged(event) {
        
        
        if (event.type == 'change' || event.keyCode === 10 || event.keyCode === 13) {     
            event.preventDefault();
            var translationValue = $(this).val();
            //$("#translation_free").prop("checked", true);
            sendAnnotationToServer("translation", translationValue, false)
            //sendTranslationMessage();
            return;
        }
        
        var currentVal = $(this).val();
        if(currentVal == translationValue) {
            return; //check to prevent multiple simultaneous triggers
        }

        translationValue = currentVal;
     
    } 
    
    
    function downCallback(selection) {
        hideAnnotationInterface();
    }
    
    function upCallback(selection) {
        showAnnotationInterface(selection.doc_id, selection.seg_id);
        sendAnnotationSchemaRequest(user, role, selection.doc_id, selection.seg_id, selection.tok_id, selection.view_id);
    }
    
    AnnotationPainter.init("span.hitoken", downCallback, upCallback);
    AnnotationPainter.setSelectionColor("yellow");
    AnnotationPainter.setHighlightColor("lightyellow");
    
    var doc_id = $("body").data("doc-id");
    var set_id = $("body").attr("data-set-id");
    var query = $("body").attr("data-query");
    var sandboxed = $("body").attr("data-query");
    var tok_id = [ ];

    key('left', function() {
            var selection = AnnotationPainter.getSelection();
            if (!selection) return false;
            var prevAnnotatedSpan = AnnotationPainter.getPrevAnnotatedSpan(
                selection.doc_id,
                selection.seg_id,
                selection.view_id,
                selection.tok_id[0]
            );
            if (!prevAnnotatedSpan[0] || !prevAnnotatedSpan[1])
                return false;
            AnnotationPainter.setSelection(
                selection.doc_id,
                selection.seg_id,
                selection.view_id,
                prevAnnotatedSpan[0],
                prevAnnotatedSpan[1],
                upCallback
            );
            return false;
    });
    key('right', function() {
            var selection = AnnotationPainter.getSelection();
            if (!selection) return false;
            var nextAnnotatedSpan = AnnotationPainter.getNextAnnotatedSpan(
                selection.doc_id,
                selection.seg_id,
                selection.view_id,
                selection.tok_id[0]
            );
            if (!nextAnnotatedSpan[0] || !nextAnnotatedSpan[1])
                return false;
            AnnotationPainter.setSelection(
                selection.doc_id,
                selection.seg_id,
                selection.view_id,
                nextAnnotatedSpan[0],
                nextAnnotatedSpan[1],
                upCallback
            );
            return false;
    });
    sendInitialAnnotationRequest();

});
