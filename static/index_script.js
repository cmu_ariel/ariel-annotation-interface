
$( document ).ready(function() {

    $("#user-icon").click(function() {
        if ($(".user-pane").is(":visible")) {
            $(".user-pane").hide();
            //$(".left_nav").hide();
            //$(".interlin").show();
        } else {
            //$(".interlin").hide();
            //$(".left_nav").hide();
            $(".user-pane").show();
        }
    });
    
    $(".lang-search").on("change keyup paste", function(event) {
        $(".resource-link-div").hide();
        console.log($(this).val());
        var queries = $(this).val().toLowerCase().split(/\W+/);
        for (var i = 0; i < queries.length; i++) {
            console.log(queries[i]);
            $(".resource-link-div.lang-" + queries[i]).show();
        }
    });
    
});