

AnnotationPainter = (function() {
    

    var selector = null;
    var currentSelection = null;
    var annotations = [];
    var annotationsByToken = {};

    var colors = {};
    var annotation_names = {};
    var selectionColor = "none";
    var highlightColor = "none";
    
    
    /*****************************************/
    /* ANNOTATIONS AND ANNOTATION MANAGEMENT */
    /*****************************************/
    
    
    var annotationFunctions = {

        /*
        equals : function(other) {
            return (this.doc_id == other.doc_id &&
                    this.seg_id == other.seg_id &&
                    this.tok_id == other.tok_id &&
                    this.view_id == other.view_id &&
                    this.user == other.user &&
                    this.role == other.role &&
                    this.annotation_type == other.annotation_type &&
                    this.annotation_label == other.annotation_label);
        }, */
        
        getTokenIDs : function() {
            results = [];
            for (var i = 0; i < this.tok_id.length; i++) {
                results.push(composeTokenClass(this.doc_id, this.seg_id, this.tok_id[i]));
            }
            return results;
        },
        
        
        isVerified : function() {
            return (this.verified === undefined || this.verified);
        },
        
    }

    function show() {
        
        $(".sf-tags-pane").empty();
        $(".sf-tags-pane").hide();
        var highlightClasses = "highlight-start highlight-end highlight-white"
        for (var i = 0; i < Object.values(colors).length; i++) {
            highlightClasses = highlightClasses + " highlight-" + Object.values(colors)[i];
        }
        
        $(selector).removeClass(highlightClasses);
        $(selector).attr("title","");
        
        annotationsByToken = {};
        
        var allAnnotations = annotations.concat([currentSelection]);
        $.each(allAnnotations, function(index, annotation) {
            if (!annotation) {
                return;
            }
            $.extend(annotation, annotationFunctions);
            showAnnotation(annotation);
        });
    }

    function showAnnotation(annotation) {
        var tok_ids = annotation.getTokenIDs();
        var nodes = [];
        
        $.each(tok_ids, function(i, tok_id) {
            if (!hasKey(tok_id, annotationsByToken)) {
                annotationsByToken[tok_id] = [];
            }
            annotationsByToken[tok_id].push(annotation);
            
            var color = "none";
            if (annotation.annotation_type == "highlight") {
                color = selectionColor;
            } else if (hasKey("label_" + annotation.annotation_label, colors)) {
                color = colors["label_" + annotation.annotation_label];
            } else if (hasKey(annotation.annotation_type, colors)) {
                color = colors[annotation.annotation_type];
            }
            
            if (annotation.annotation_label != 'none') {
                var tooltip = $("." + tok_id).attr('title');
                if (tooltip != '') {
                    tooltip += ", "
                }
                $("." + tok_id).attr('title', tooltip + annotation.user + " (" + annotation.annotation_type + "): " + annotation.annotation_label)
                if (annotation.annotation_type == "sf") {
                    var sf_tags_pane = $("." + tok_id).closest(".interlin").children(".sf-tags-pane").first();
                    if (sf_tags_pane.children(".sf_" + annotation.annotation_label).length == 0) {
                        sf_tags_pane.append('<p class="sf-tags sf_' + annotation.annotation_label + '">' + 
                                annotation_names[annotation.annotation_type + ':' + annotation.annotation_label] + "</p>");
                        sf_tags_pane.show();
                    }
                }
            }
            
            nodes = nodes.concat($("." + tok_id));
            
            $("." + tok_id).addClass("highlight-" + color);
            
            if (!annotation.isVerified()) {
                $("."+ tok_id).addClass("highlight-white");
            }
        });
        
        $.each(nodes, function(i, node) {
            
            if (i == 0) {
                node.addClass("highlight-start");
            }
            if (i == nodes.length - 1) {
                node.addClass("highlight-end");
            }
        });
    }


    function getSelectedAnnotations() {
        if (!currentSelection) {
            return [];
        }
        var results = [];
        var tokenIDs = currentSelection.getTokenIDs();
        $.each(tokenIDs, function(i, id) {
            if (!hasKey(id, annotationsByToken)) {
                return;
            }
            results = results.concat(annotationsByToken[id]);
        });
        return results;
    }



    /***************************/
    /* Span and text selection */
    /***************************/

    function mouseDownSelect(event, callback) {
        
        event.stopPropagation();
        
        if (event.which != 1) {
            return;
        }
        
        
        if (!event.shiftKey || !currentSelection) {
            
            clearSelection();
            
            currentSelection = { 
                "doc_id": $(event.target).attr("data-doc-id"),
                "seg_id": $(event.target).attr("data-seg-id"),
                "tok_id": $(event.target).attr("data-tok-id").split(","),
                "view_id": $(event.target).attr("data-view_id"),
                "annotation_type": "highlight",
                "annotation_label": "none"
            }
            $.extend(currentSelection, annotationFunctions);     
            callback(currentSelection);
        } 
    }

    function getTokIDRange(doc_id, seg_id, view_id, tok_id_start, tok_id_end) {
        var startSelector = ".type-" + view_id + " ." + 
            composeTokenClass(doc_id, seg_id, tok_id_start);
        var currentNode = $(startSelector)[0];
        var currentNodeIDs = $(currentNode).attr("data-tok-id").split(",");
        var results = currentNodeIDs
        while (!(results.hasValue(tok_id_end))) {
            currentNode = currentNode.nextSibling;
            if (currentNode === null | currentNode.tagName != 'SPAN') {
                continue;
            }
            currentNodeIDs = $(currentNode).attr("data-tok-id").split(",");
            results = results.concat(currentNodeIDs);
        } 
        
        return results.getUnique();
    }

    function composeTokenClass(doc_id, seg_id, tok_id) {
        return "hlc-" + doc_id + "-" + seg_id + "-" + tok_id
    }

    function getPrevAnnotatedSpan(doc_id, seg_id, view_id, tok_id_start) {
        var startSelector = ".type-" + view_id + " ." + 
            composeTokenClass(doc_id, seg_id, tok_id_start);
        var currentNode = $(startSelector)[0].previousSibling;
        var start_tok_id = null;
        var end_tok_id = null;
        while(currentNode !== null && !$(currentNode).hasClass("highlight-end"))
            currentNode = currentNode.previousSibling;
        if (currentNode !== null && $(currentNode).hasClass("highlight-end")) {
            end_tok_id = $(currentNode).attr("data-tok-id").split(",");
            while(currentNode !== null && !$(currentNode).hasClass("highlight-start"))
                currentNode = currentNode.previousSibling;
            if (currentNode !== null && $(currentNode).hasClass("highlight-start"))
                start_tok_id = $(currentNode).attr("data-tok-id").split(",");
        }
        return [start_tok_id, end_tok_id]
    }

    function getNextAnnotatedSpan(doc_id, seg_id, view_id, tok_id_start) {
        var startSelector = ".type-" + view_id + " ." + 
            composeTokenClass(doc_id, seg_id, tok_id_start);
        var currentNode = $(startSelector)[0].nextSibling;
        var start_tok_id = null;
        var end_tok_id = null;
        while(currentNode && !$(currentNode).hasClass("highlight-start"))
            currentNode = currentNode.nextSibling;
        if (currentNode && $(currentNode).hasClass("highlight-start")) {
            start_tok_id = $(currentNode).attr("data-tok-id").split(",");
            while(currentNode && !$(currentNode).hasClass("highlight-end"))
                currentNode = currentNode.nextSibling;
            if (currentNode && $(currentNode).hasClass("highlight-end"))
                end_tok_id = $(currentNode).attr("data-tok-id").split(",");
        }
        return [start_tok_id, end_tok_id]
    }

    function setSelection(doc_id, seg_id, view_id, prev_tok_id, tok_id, callback) {
        // the following sets the browser's selection to our selection,
        // so that (for example) copy-pasting works correctly. 
        // or more correctly, at least
        var s = window.getSelection();
        s.removeAllRanges();
        var range = document.createRange();
        var startSelector = ".type-" + view_id + " ." + 
            composeTokenClass(doc_id, seg_id, prev_tok_id[0]);
        var endSelector = ".type-" + view_id + " ." + 
            composeTokenClass(doc_id, seg_id, tok_id);
        var startNode = $(startSelector)[0];
        var endNode = $(endSelector)[0];
        range.setStart(startNode, 0);
        range.setEnd(endNode, endNode.childNodes.length);
        if (range.collapsed) {
            var startNode = endNode;
            var endSelector = ".type-" + view_id + " ." + 
                composeTokenClass(doc_id, seg_id, prev_tok_id[prev_tok_id.length-1]);
            var endNode = $(endSelector)[0];
            range.setStart(startNode, 0);
            range.setEnd(endNode, endNode.childNodes.length);
        }
        s.addRange(range);
        
        tok_ids = $(startNode).attr("data-tok-id").split(",");
        tok_ids = tok_ids.concat($(endNode).attr("data-tok-id").split(","));
        tok_id_range = getTokIDRange(doc_id, seg_id, view_id, tok_ids[0], tok_ids[tok_ids.length-1]);
        
        clearSelection();
        
        currentSelection = { 
            "doc_id": doc_id,
            "seg_id": seg_id,
            "tok_id": tok_id_range,
            "view_id": view_id,
            "annotation_type": "highlight",
            "annotation_label": "none"
        }
        $.extend(currentSelection, annotationFunctions);
        show();
        callback(currentSelection);
    }
    
    function mouseUpSelect(event, callback) {
        
        event.stopPropagation();
        
        if (event.which != 1) {
            return;
        }
        
        var doc_id = $(event.target).attr("data-doc-id");
        var seg_id = $(event.target).attr("data-seg-id");
        var tok_id = $(event.target).attr("data-tok-id").split(",");
        var view_id = $(event.target).attr("data-view_id");
        if (currentSelection && currentSelection.doc_id == doc_id &&
                                currentSelection.seg_id == seg_id &&
                                currentSelection.view_id == view_id) {
            prev_tok_id = currentSelection.tok_id;
        } else {
            prev_tok_id = [tok_id];
        }
        
        setSelection(doc_id, seg_id, view_id, prev_tok_id, tok_id, callback);
    }

    function init(s, downCallback, upCallback) {
        selector = s;
        $(selector).mousedown(function(e){mouseDownSelect(e,downCallback)});
        $(selector).mouseup(function(e){mouseUpSelect(e,upCallback)});
        
        $(selector).each(function(){
            var span = this;
            var tok_ids = $(span).attr("data-tok-id").split(",");
            $.each(tok_ids, function(i, id) {
                var newClass = composeTokenClass($(span).attr("data-doc-id"), 
                                $(span).attr("data-seg-id"), id)
                $(span).addClass(newClass)
            });
        });
        
        // set up mouseover highlights
        $(document).on('mouseover', selector, function() {
           var classList = $(this).attr("class").split(/\s+/);
           var that = this;
           $.each(classList, function(i,c) {
               if (c.startsWith("hlc-")) {
                   $("."+c).addClass("highlight-" + highlightColor);
               }
           });
           return false;
        });
        
        $(document).on('mouseout', selector, function() {
            $(selector).removeClass("highlight-" + highlightColor);
        });
    }
    
    function clearSelection() {
        currentSelection = null;
        $(selector).removeClass("highlight-yellow");
        show();
    }
    
    function getSelection() {
        return currentSelection;
    }
    
    function updateAnnotations(newAnnos) {
        annotations = newAnnos;
        show();
    }
    
    function hasColors() {
        return Object.keys(colors).length != 0;
    }
    
    function setColor(annotation_type, color) {
        colors[annotation_type] = color;
    }
    
    function setSelectionColor(color) {
        selectionColor = color;
    }
    
    function setHighlightColor(color) {
        highlightColor = color;
    }
    
    function setAnnotationNames(ann_names) {
        annotation_names = ann_names;
    }
    
    
    return {  // the public interface for the AnnotationPainter
        init : init,     // set up the AnnotationPainter
        clearSelection : clearSelection,   // clear the current selection and remove highlighting
        updateAnnotations : updateAnnotations, // swap out the current annotations with a new set 
        show : show,     // display the current set of annotations
        getSelection : getSelection,           // get info on the current user selection
        getSelectedAnnotations : getSelectedAnnotations,  // get annotations corresponding to the current user selection
        getPrevAnnotatedSpan : getPrevAnnotatedSpan,
        getNextAnnotatedSpan : getNextAnnotatedSpan,
        setSelection : setSelection,
        getTokIDRange : getTokIDRange,
        hasColors : hasColors, // has the color scheme been set up yet
        setColor : setColor, // assign a color to an annotation type
        setSelectionColor : setSelectionColor,  // assign a color to the user selection
        setHighlightColor : setHighlightColor,   // assign a color to the mouseover highlight 
        setAnnotationNames : setAnnotationNames, //map of label_abbrev to labels
    };
    
})();

