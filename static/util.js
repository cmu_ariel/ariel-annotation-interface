
Array.prototype.getUnique = function(){
   var u = {}, a = [];
   for(var i = 0, l = this.length; i < l; ++i){
      if(u.hasOwnProperty(this[i])) {
         continue;
      }
      a.push(this[i]);
      u[this[i]] = 1;
   }
   return a;
}

Array.prototype.indexOf || (Array.prototype.indexOf = function(d, e) {
    var a;
    if (null == this) throw new TypeError('"this" is null or not defined');
    var c = Object(this),
        b = c.length >>> 0;
    if (0 === b) return -1;
    a = +e || 0;
    Infinity === Math.abs(a) && (a = 0);
    if (a >= b) return -1;
    for (a = Math.max(0 <= a ? a : b - Math.abs(a), 0); a < b;) {
        if (a in c && c[a] === d) return a;
        a++
    }
    return -1
});

function alphabetical(a, b)
{
     var A = a.toLowerCase();
     var B = b.toLowerCase();
     if (A < B){
        return -1;
     }else if (A > B){
       return  1;
     }else{
       return 0;
     }
}

Array.prototype.alphasort = function() {
    this.sort(alphabetical);
}

Array.prototype.randomchoice = function() {
    return this[Math.floor(Math.random()*this.length)];
}

Array.prototype.getRange = function() {
    var lowest = null;
    var highest = null;
    for (var i = 0; i < this.length; i++) {
        num = parseInt(this[i]);
        if (lowest === null || num < lowest) {
            lowest = num;
        }
        if (highest === null || num > highest) {
            highest = num;
        }
    }
    results = [];
    for (var j = lowest; j <= highest; j++) {
        results.push(j);
    }
    return results;
}

Array.prototype.hasValue = function(val) {
    return $.inArray(val, this) != -1;
}

hasKey = function(item, arr) {
    for (var key in arr) {
        if (arr.hasOwnProperty(key) && key == item) {
            return true;
        }
    }
    return false;
}
