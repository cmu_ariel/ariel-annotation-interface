
##############################
#
# Curl
#
##############################

def sendCurlPost(url, data, username='', password='', retry=True):
    headers = {'Content-type': 'application/json'}
    data = json.dumps(data)
    is_sent = False
    while not is_sent:
        try:
            if username:
                r = requests.post(url, data=data, headers=headers, auth=(self.anno_username, self.anno_password))
            else:
                r = requests.post(url, data=data, headers=headers)
            if r.status_code != 200:
                logging.error("ERROR: Status code %s received from %s" % (r.status_code, url))
            is_sent = True
            return r
        except requests.exceptions.ConnectionError:
            logging.error("Cannot connect to %s" % url)
            if retry:
                logging.error("Retrying in 10 seconds")  
                time.sleep(10)
            else:
                is_sent = True


##############################
#
# DataConnections
#
##############################

class SourceServer:
    ''' Encapsulates the information about one of this server's source servers '''
    
    def __init__(self, address):
        self.address = address
        
    def getTokens(self, doc_id):
        query = {"doc_id":doc_id}
        url = "http://%s/get_tokens" % self.address
        r = sendCurlPost(url, query, retry=False)
        if not r: 
            return []
        return r.json()["data"]
    
    def getResources(self, doc_id, view_id):
        query = {"doc_id":doc_id, "view_id":view_id}
        url = "http://%s/get_resources" % self.address
        r = sendCurlPost(url, query, retry=False)
        if not r:
            return []
        return r.json()["data"]

#############################
#
# DB queries
#
#############################

def get_textitems(query):
    _query = copy.deepcopy(query)
    if "tok_ids" in _query:
        _query["tok_ids"] = {"$in": _query["tok_ids"]}
    return list(db.textitems.find(_query)) 

def get_resource_info(query):
    return list(db.resourceInfoDB.find(query))