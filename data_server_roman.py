#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function
from __future__ import unicode_literals
from io import open
import argparse, json, requests, logging, copy
from gevent.wsgi import WSGIServer
from gevent import monkey; monkey.patch_socket()
from bson import json_util 

from pymongo import MongoClient

from flask import Flask, request, Response, make_response, redirect
app = Flask(__name__)

import epitran
from epitran.reromanize import ReRomanizer



logger = logging.getLogger()
logger.setLevel(logging.DEBUG)
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
ch.setFormatter(formatter)
logger.addHandler(ch)


VIEW_ID = "roman"
VIEW_NAME = "Roman"
VIEW_PRIORITY = 3.0
LANG = ''


#############################
#
# DB queries
#
#############################

def get_textitems(query):
    _query = copy.deepcopy(query)
    if "tok_ids" in _query:
        _query["tok_ids"] = {"$in": _query["tok_ids"]}
    return list(db.textitems.find(_query)) 

def get_resource_info(query):
    return list(db.resourceInfoDB.find(query))
    
    
    
@app.route("/add_translation", methods=["POST"]) 
def add_translation(): 
    return json.dumps({ "success":True, "data": [], "message":'' }, default=json_util.default)  
        

@app.route("/get_all_resources", methods=["POST"]) 
def get_all_resources(): 
    resourceInfos = get_resource_info({"lang_id":LANG})
    for resourceInfo in resourceInfos:
        resourceInfo["view_id"] = VIEW_ID
        resourceInfo["view_name"] = VIEW_NAME
        resourceInfo["view_priority"] = VIEW_PRIORITY
    return json.dumps({ "success":True, "data": resourceInfos, "message":'' }, default=json_util.default)
    
@app.route("/get_resources_by_id", methods=["POST"]) 
def get_resources_by_id(): 
    data = request.get_json()
    res_id = data["res_id"]
    if res_id != RES_ID:
        return json.dumps({ "success":True, "data": responseData, "message":'' }, default=json_util.default)
    return get_all_resources() 

@app.route("/get_resources", methods=["POST"]) 
def get_resources():  
    data = request.get_json()
    responseData = []
    doc_id = data["doc_id"]
    view_id = data["view_id"]
    if view_id != VIEW_ID:
        return json.dumps({ "success":True, "data": responseData, "message":'' }, default=json_util.default)
    resourceInfos = get_resource_info({"doc_id":doc_id, "view_id":"original"})
    for resourceInfo in resourceInfos:
        resourceInfo["view_id"] = VIEW_ID
        resourceInfo["view_name"] = VIEW_NAME
        resourceInfo["view_priority"] = VIEW_PRIORITY
        responseData.append(resourceInfo)
    return json.dumps({ "success":True, "data": responseData, "message":'' }, default=json_util.default)

@app.route("/get_tokens", methods=["POST"]) 
def get_tokens():  
    data = request.get_json()
    responseData = []
    doc_id = data["doc_id"]
    resourceInfos = get_resource_info({"doc_id":doc_id})
    if not resourceInfos:
        return json.dumps({ "success":True, "data": responseData, "message":'' }, default=json_util.default)
    resourceInfo = resourceInfos[0]
    if resourceInfo["lang_id"] != LANG:
        return json.dumps({ "success":True, "data": responseData, "message":'' }, default=json_util.default)
    tokens = get_textitems({"doc_id":doc_id, "view_id":"original"})
    for token in tokens:
        text = token["text"]
        text = text.replace(r"\x94","")
        token["text"] = g2p(text)
        token["view_id"] = VIEW_ID
        responseData.append(token)
    return json.dumps({ "success":True, "data": responseData, "message":'' }, default=json_util.default)
    

if __name__ == '__main__':
    argparser = argparse.ArgumentParser()
    argparser.add_argument("langScript", help="Language-script pair (e.g. 'eng-Latn')")
    argparser.add_argument("--port", type=int, default=5000, help="Port for serving the files.")
    argparser.add_argument("--db_ip", default="127.0.0.1", help="database IP")
    argparser.add_argument("--db_port", type=int, default=27017, help="database port")
    argparser.add_argument("--db_name", default="lorelei_viewer", help="database name")
    argparser.add_argument("--lang", help="lang code")
    args = argparser.parse_args()
    
    # g2p = epitran.Epitran(args.langScript).transliterate
    g2p = ReRomanizer(args.langScript, 'anglocentric').reromanize
    LANG = args.lang if args.lang else args.langScript.split("-")[0]
    
    # setup DB
    client = MongoClient(args.db_ip, args.db_port)
    db = client[args.db_name]
    
    
    http_server = WSGIServer(('', args.port), app)
    http_server.serve_forever()
    
    
