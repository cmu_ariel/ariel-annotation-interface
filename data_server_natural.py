#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function
from __future__ import unicode_literals
from io import open
import argparse, json, requests, logging, sys, os, copy, traceback
from gevent.wsgi import WSGIServer
from gevent import monkey; monkey.patch_socket()
from bson import json_util 
from watchdog.observers import Observer
from watchdog.events import FileSystemEventHandler
from pymongo import MongoClient

from flask import Flask, request, Response, make_response, redirect
app = Flask(__name__)


logger = logging.getLogger()
logger.setLevel(logging.DEBUG)
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
ch.setFormatter(formatter)
logger.addHandler(ch)

module = None
db = None

DIR = ''
VIEW_ID = "natural"
VIEW_NAME = "Natural"
VIEW_PRIORITY = 5.0
LANG = ""
TRANSLATION_FILENAME = "lexicon_supplement.txt"
SUPPLEMENTAL_TRANSLATIONS = set()

#############################
#
# DB queries
#
#############################

def get_textitems(query):
    _query = copy.deepcopy(query)
    if "tok_ids" in _query:
        _query["tok_ids"] = {"$in": _query["tok_ids"]}
    return list(db.textitems.find(_query)) 

def get_resource_info(query):
    return list(db.resourceInfoDB.find(query))
    
############################
#
# Flask routes
#
############################


@app.route("/add_translation", methods=["POST"]) 
def add_translation(): 
    annotation = request.get_json()
    translation = annotation["translation"]
    
    doc_id = annotation["doc_id"]
    resourceInfos = get_resource_info({"doc_id":doc_id})
    if not resourceInfos:
        return json.dumps({ "success":True, "data": [], "message":'' }, default=json_util.default)  
    if resourceInfos[0]["lang_id"] != LANG:
        return json.dumps({ "success":True, "data": [], "message":'' }, default=json_util.default) 
    
    tokens = get_textitems({"doc_id": doc_id,
                            "seg_id": annotation["seg_id"],
                            "tok_ids": annotation["tok_id"],
                            "view_id": "original"})
                            
    if not tokens:
        return json.dumps({ "success":True, "data": [], "message":'' }, default=json_util.default)   
    
    tokens.sort(key=lambda x:x["tok_offset"])
    texts = []
    offsetsFound = set()
    for t in tokens:
        if t["tok_offset"] in offsetsFound:
            continue
        texts.append(t["text"])
        offsetsFound.add(t["tok_offset"])
    text = " ".join(texts)
    
    if (translation, text) in SUPPLEMENTAL_TRANSLATIONS:
        return json.dumps({ "success":True, "data": [], "message":'' }, default=json_util.default) 
        
    SUPPLEMENTAL_TRANSLATIONS.add((translation, text))
    
    outputFilename = os.path.join(DIR, TRANSLATION_FILENAME)
    with open(outputFilename, "a", encoding="utf-8") as fout:
        fout.write("%s\t%s\n" % (translation,text))
    return json.dumps({ "success":True, "data": [], "message":'' }, default=json_util.default) 
    
@app.route("/get_all_resources", methods=["POST"]) 
def get_all_resources(): 
    resourceInfos = get_resource_info({"lang_id":LANG})
    for resourceInfo in resourceInfos:
        resourceInfo["view_id"] = VIEW_ID
        resourceInfo["view_name"] = VIEW_NAME
        resourceInfo["view_priority"] = VIEW_PRIORITY
    return json.dumps({ "success":True, "data": resourceInfos, "message":'' }, default=json_util.default)
    
@app.route("/get_resources_by_id", methods=["POST"]) 
def get_resources_by_id(): 
    data = request.get_json()
    res_id = data["res_id"]
    resourceInfos = get_resource_info({"res_id":res_id})
    if not resourceInfos:
        return json.dumps({ "success":True, "data": responseData, "message":'' }, default=json_util.default)
    resourceInfo = resourceInfos[0]
    if resourceInfo["lang_id"] != LANG:
        return json.dumps({ "success":True, "data": responseData, "message":'' }, default=json_util.default)
    return get_all_resources()     
    
@app.route("/get_resources", methods=["POST"]) 
def get_resources():  
    data = request.get_json()
    responseData = []
    doc_id = data["doc_id"]
    view_id = data["view_id"]
    if view_id != VIEW_ID:
        return json.dumps({ "success":True, "data": responseData, "message":'' }, default=json_util.default)
    resourceInfos = get_resource_info({"doc_id":doc_id, "view_id":"original"})
    for resourceInfo in resourceInfos:
        resourceInfo["view_id"] = VIEW_ID
        resourceInfo["view_name"] = VIEW_NAME
        resourceInfo["view_priority"] = VIEW_PRIORITY
        responseData.append(resourceInfo)
    return json.dumps({ "success":True, "data": responseData, "message":'' }, default=json_util.default)

'''
def lookup(tokens):

    sort(tokens, key=lambda x:x["seg_offset"])
    
    
def lookup_aux(tokens):
    results = set()
    for span_size in range(1,4):
        strng = [t["text"] for t in tokens[0:span_size]]
        print("[%s]" % strng)
        for
'''
    
    
@app.route("/get_tokens", methods=["POST"]) 
def get_tokens():  
    global module
    data = request.get_json()
    responseData = []
    doc_id = data["doc_id"]
    resourceInfos = get_resource_info({"doc_id":doc_id})
    if not resourceInfos:
        return json.dumps({ "success":True, "data": responseData, "message":'' }, default=json_util.default)
    resourceInfo = resourceInfos[0]
    if resourceInfo["lang_id"] != LANG:
        return json.dumps({ "success":True, "data": responseData, "message":'' }, default=json_util.default)
    tokens = get_textitems({"doc_id":doc_id, "view_id":"original"})
    for token in tokens:
        if module:
            try:
                token["text"] = module.best_parse(token["text"], "natural")
            except:
                logging.error("Error while parsing in %s" % module.__name__)
                traceback.print_exc()
        token["view_id"] = VIEW_ID
        responseData.append(token)
    return json.dumps({ "success":True, "data": responseData, "message":'' }, default=json_util.default)
    
def reload_annotations():
    SUPPLEMENTAL_TRANSLATIONS = set()
    inputFilename = os.path.join(DIR, TRANSLATION_FILENAME)
    if os.path.exists(inputFilename):
        with open(inputFilename, "r", encoding="utf-8") as fin:
            for line in fin.readlines():
                SUPPLEMENTAL_TRANSLATIONS.add(tuple(line.split("\t",1)))
            
            
class ReloadEventHandler(FileSystemEventHandler):

    def __init__(self, module):
        self.module = module
        
    def on_any_event(self, event):
        if event.src_path.endswith(".pyc") or event.is_directory:
            return
        logging.info("Change detected at %s, reloading %s" % (event.src_path, self.module.__name__))
        try:
            reload(self.module)
            reload_annotations()
            
        except:
            logging.error("Cannot reload module %s" % self.module.__name__)
            traceback.print_exc()


if __name__ == '__main__':
    argparser = argparse.ArgumentParser()
    argparser.add_argument("lang", help="Language (ISO-639-3 code)")
    argparser.add_argument("path", help="Path to morphology processor")
    argparser.add_argument("--port", type=int, default=5000, help="Port for serving the files.")
    argparser.add_argument("--db_ip", default="127.0.0.1", help="database IP")
    argparser.add_argument("--db_port", type=int, default=27015, help="database port")
    argparser.add_argument("--db_name", default="lorelei_viewer", help="database name")
    args = argparser.parse_args()
    
    LANG = args.lang
    
    # setup DB
    client = MongoClient(args.db_ip, args.db_port)
    db = client[args.db_name]
    
    # load the supplemental translations
    DIR = os.path.dirname(args.path)
    reload_annotations()
            
    # load the morphology module...
    module_name = os.path.basename(args.path).rsplit(".", 1)[0]
    sys.path.append(DIR)
    try:
        module = __import__(module_name)
    except:
        logging.error("Cannot load module %s" % module_name)
        traceback.print_exc()
        sys.exit()
        
    # ... and watch its folder
    event_handler = ReloadEventHandler(module)
    observer = Observer()
    observer.schedule(event_handler, DIR, recursive=True)
    observer.start()
    
    http_server = WSGIServer(('', args.port), app)
    http_server.serve_forever()
    
    