#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import print_function
from __future__ import unicode_literals
from io import open
import argparse, json, sys
from bson import json_util 
from gevent.wsgi import WSGIServer
from gevent import monkey; monkey.patch_socket()
import flask
app = flask.Flask(__name__)

import requests

# add by moe
import os
import sys

# method class path
sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '/home/mfang/Workspace/AL-LOREL/src/al/')))
from dataPool import DataPool
from agentRandom import AgentRandom
from agentSentUNC import AgentSentUncertainty

# the number of sentences shown per page
window_size = 100
# the original data source
data_pool_dir = os.path.join(os.path.dirname(__file__), '/somewhere/ltf/ug/')
data_pool = DataPool(data_pool_dir)

def log_error(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)
    
    
class SimpleActiveLearningServer:

    def __init__(self, ip, port, anno_ip, anno_port, anno_username, anno_password):
        ''' Initialize the server with its ip/port and the annotation server's ip/port. '''
        self.ip = ip
        self.port = port
        self.anno_ip = anno_ip
        self.anno_port = anno_port
        self.anno_username = anno_username
        self.anno_password = anno_password
        # initialise the active learning agent
        self.agentAL = AgentSentUncertainty(data_pool, window_size)
        # run active learning
        self.agentAL.run()	

    # return the ids of the unlabelled sentences that are used for labelling
    # ids=[{"doc":doc_id, "seg":seg_id},...]
    def sendWaitingList(self):
        tolabel = self.agentAL.getWaitingList()
        return tolabel   

    def sendCurlPost(self, url, data):
        ''' Send a curl-like request '''
        try:
            r = requests.post(url, 
                data=json.dumps(data), 
                headers={'Content-type': 'application/json'}, 
                auth=(self.anno_username, self.anno_password))
            if r.status_code != 200:
                log_error("ERROR: Status code %s received from %s" % (r.status_code, url))
            return r
        except requests.exceptions.ConnectionError:
            log_error("ERROR: Cannot connect to %s" % url)       
            return {}
    
    def processAnnotations(self, annotations):
        #########################################
        #
        # Do something with the annotations
        #
        #########################################
        # todo
        # currently, our model does not update; we will implement it later
        self.agentAL.receive(annotations)
        return
        
        
    def subscribeToAnnotationNotifications(self, anno_types, dump=False):
        ''' Send the annotation server a message that we're interested in annotations of the specified types
            e.g., ["ner", "edl", "sf"] '''
        data = { "ip": self.ip, "port": self.port, "anno_types": anno_types, "dump":dump }
        url = "http://%s:%s/annotations/subscribe" % (self.anno_ip, self.anno_port)
        r = self.sendCurlPost(url, data)
        if not r:
            log_error("ERROR: Failed to subscribe to annotation notifications.")
        if not dump:
            return       
        requestJSON = r.json()
        if "data" not in requestJSON:
            log_error("ERROR: Annotation dump request failed.")
            return    
        self.processAnnotations(requestJSON["data"])    
    
    def requestAnnotationDump(self, anno_types):
        ''' Request all existing annotations of the specified types.  This is mostly for when the system initializes
            and wants to know what's already there '''
            
        data = { "anno_types": anno_types }
        url = "http://%s:%s/annotations/dump" % (self.anno_ip, self.anno_port)
        r = self.sendCurlPost(url, data)
        if "annotations" not in r:
            log_error("ERROR: Annotation dump request failed.")
            return 
        requestJSON = r.json()
        if "data" not in requestJSON:
            log_error("ERROR: Annotation dump request failed.")
            return    
        self.processAnnotations(requestJSON["data"])  
            
    def handleAnnotationNotification(self, data):
        ''' Called upon receiving an annotation notification '''
        
        ###########################################
        #
        # Do something with the notification data
        #
        ###########################################
        
        #print(data.keys())
        # update three lists: labelled, unlabelled, waiting_list
        self.agentAL.receive(annotations)
        return
        
    
    def requestTask(self, lang, task, items, user="__ANY__"):
        ''' Request that a user (by default, any user) complete an annotation task on items in the query.
        '''
        data = { "user": user, "lang":lang, "task":task, "items":items}
        url = "http://%s:%s/task/create" % (self.anno_ip, self.anno_port)
        self.sendCurlPost(url, data)
        

        
@app.route("/receive_annotations", methods=["POST"])
def receiveAnnotationNotification():
    al_server.handleAnnotationNotification(flask.request.form)
    return json.dumps({"success":True, "data":'', "message":''}, default=json_util.default)    
    
    
if __name__ == '__main__':
    argparser = argparse.ArgumentParser()
    argparser.add_argument("--ip", default="localhost", help="IP for this server.")   # WSGI doesn't provide Flask with this info
    argparser.add_argument("--port", type=int, default=5012, help="Port for this server.")  # nor this
    argparser.add_argument("--anno_ip", default="localhost", help="IP for the annotation server.")
    argparser.add_argument("--anno_port", type=int, default=5003, help="Port for the annotation server.")
    argparser.add_argument("--user", default='user', help="Username for the annotation server.")
    argparser.add_argument("--password", default='password', help="Port for the annotation server.")
    args = argparser.parse_args()

    # set up the server object and subscribe to relevant task notifications
    al_server = SimpleActiveLearningServer(args.ip, args.port, args.anno_ip, args.anno_port, args.user, args.password)
    al_server.subscribeToAnnotationNotifications(["ner"], dump=True)
    
    # run the server
    http_server = WSGIServer(('', args.port), app)
    http_server.serve_forever()
