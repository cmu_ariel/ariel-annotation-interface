#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function
from __future__ import unicode_literals
from io import open
import json
from collections import defaultdict

ANNOTATION_SCHEMAS = []

try:
    with open("config/annotation_schemas.json", "r", encoding="UTF-8") as fin:
        ANNOTATION_SCHEMAS = json.load(fin)
except:
    pass

def isExclusive(annotation_type):
    for schema in ANNOTATION_SCHEMAS:
        if annotation_type == schema["name_abbrev"]:
            return "exclusive" in schema and schema["exclusive"]

def get_basic_schemas():
    with open("config/annotation_schemas.json", "r", encoding="UTF-8") as fin:
        return json.load(fin)
        

def getAnnotationSchema(user, role, doc_id, seg_id, tok_id):

    # Load configuration variables
    # (for now, we load these every call, so as not to have to restart the server every
    # time we change the config)
    with open("config/annotation_schemas.json", "r", encoding="UTF-8") as fin:
        schemas = json.load(fin)
        
    with open("config/role_permissions.json", "r", encoding="UTF-8") as fin:
        role_permissions = json.load(fin)
    
    
    if role in ["None", "", None]:
        role = "none"
    
    if role not in role_permissions:
        raise Exception("Undefined role: " + role)
        
        
    result = defaultdict(lambda:False)
    result.update(role_permissions[role])
    result["user"] = user
    result["role"] = role
    
    #for task in schemas:
    #    task_id = task["name_abbrev"]
    #    result["can_"+task_id] = False
    
    for task in result["modify_permissions"]:
        result["can_"+task] = True
    
    
    if not tok_id:
        result["can_ner"] = False
        result["can_edl"] = False
        result["can_translate"] = False
        result["can_search"] = False
        
    if not seg_id:
        result["can_sf"] = False    
    

    for schema in schemas:
        name = schema["name_abbrev"]
        schema["enabled"] = result["can_"+name]
            
    result["schemas"] = schemas
    
    return result
