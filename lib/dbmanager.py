#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pymongo
from pymongo import MongoClient
from user_roles import isExclusive
from copy import deepcopy
from io import open
import json

def init(mongodb_ip, mongodb_port, db_name):
    global client, db, resourceInfoDB, textitems, annotations, actionlogs, tasks, entityLinks, sfLocationLinks, pinnedResources, userInfo, documentKeywords, listeners
    client = MongoClient(mongodb_ip, mongodb_port)
    db = client[db_name]
    tasks = db.tasks
    textitems = db.textitems
    resourceInfoDB = db.resourceInfoDB
    pinnedResources = db.pinnedResources
    annotations = db.annotations
    actionlogs = db.actionlogs
    entityLinks = db.entityLinks
    sfLocationLinks = db.sfLocationLinks
    userInfo = db.userInfo
    documentKeywords = db.documentKeywords
    listeners = db.listeners

def get_client():
    return db
    
####################################
# Annotations
####################################

def delete_annotations(doc):
    # delete all annotations which have at least one of the tok_ids
    _doc = deepcopy(doc)
    if "delete" in _doc:
        del _doc["delete"]
    if "tok_id" in _doc:
        _doc["tok_id"] = {"$in": _doc["tok_id"]}
    return annotations.delete_many(_doc).deleted_count

def add_annotation(doc):
    # TODO: use upsert?
    if isExclusive(doc["annotation_type"]):
        # delete_annotations({k: doc[k] for k in ["user", "doc_id", "seg_id", "tok_id", "annotation_type"]})
        delete_annotations({k: doc[k] for k in ["doc_id", "seg_id", "tok_id", "annotation_type"]})
    return annotations.insert_one(doc).inserted_id

def log_action(doc):
    return actionlogs.insert_one(doc).inserted_id

def add_annotations(docs):
    for doc in docs:
        if isExclusive(doc["annotation_type"]):
            # delete_annotations({k: doc[k] for k in ["user", "doc_id", "seg_id", "tok_id", "annotation_type"]})
            delete_annotations({k: doc[k] for k in ["doc_id", "seg_id", "tok_id", "annotation_type"]})
    return annotations.insert_many(docs).inserted_ids

def get_annotations(query):
    _query = deepcopy(query)
    # for fields in query which have array of values, value of field in db should equal any value in query array OR
    # if field in db holds an array, then it should contain at least one element that matches a value in query array 
    for field in _query:
        if type(_query[field]) == list:
            _query[field] = { "$in": _query[field] }
    return list(annotations.find(_query))

def get_annotations_available_for_documents(doc_ids):
    query = { "doc_id": doc_ids }
    annotations = get_annotations(query)
    results = set()
    for annotation in annotations:
        user_id = annotation["user"]
        annotation_type = annotation["annotation_type"]
        results.update([(user_id, annotation_type)])
    results = sorted(results, key=lambda x:x[0])
    results = [ { "annotator":x[0], "annotation_type":x[1], "annotation_type_caps":x[1].upper() } for x in results ]
    return results
    
####################################
# Entity Links
####################################

def add_entity_link(doc):
    assert("annotation_type" in doc)
    assert("annotation_label" in doc)
    return annotations.insert_one(doc).inserted_id
    
def remove_entity_link(doc):
    assert("annotation_type" in doc)
    query = {k:doc[k] for k in ["user", "doc_id", "seg_id", "tok_id", "annotation_type", "annotation_label"]}
    return annotations.delete_many(query).deleted_count
    
def get_entity_links(query):
    assert("annotation_type" in query)
    return list(annotations.find(query))

    
######################################
# SF Location links
######################################

# def add_sf_location_link(doc):
    # assert("annotation_type" in doc)
    # assert(doc["annotation_type"] == 'sf_location')
    # assert("annotation_label" in doc)
    # return sfLocationLinks.insert_one(doc).inserted_id
    
# def remove_sf_location_link(doc):
    # assert("annotation_type" in doc)
    # assert(doc["annotation_type"] == 'sf_location')
    # assert("annotation_label" in doc)
    # return sfLocationLinks.remove(doc)

# def get_sf_location_links(query):
    # assert("annotation_type" in query)
    # assert(query["annotation_type"] == 'sf_location')
    # return list(sfLocationLinks.find(query))
    
#################
# Tasks
#################

def get_query_for_task(task):
    _query = {
        "user": task["user"],
        "role": task["role"],
        "items": sorted(task["items"], key=lambda x: (x["doc"], x.get("seg", "*")))
    }
    if "set" in task:
        _query["set"] = task["set"]
    return _query

def add_task(task):
    task["items"].sort(key=lambda x: (x["doc"], x.get("seg", "*"))) 
    return tasks.update(get_query_for_task(task), {"$setOnInsert": task}, upsert=True)

def get_tasks(query):
    return list(tasks.find(query))

def ghost_copy_annotations(doc_id, seg_id, user_id, role_id, ghost_user_id, ghost_role_id):
    newAnnotations = []
    query = {"doc_id":doc_id, "user":ghost_user_id, "role":ghost_role_id}
    if seg_id != None:
        query["seg_id"] = seg_id
    oldAnnotations = get_annotations(query)
    print("Ghost copying %s annotations from %s to %s" % (len(oldAnnotations), user_id, ghost_user_id))
    for oldAnno in oldAnnotations:
        newAnno = { k:oldAnno[k] for k in 
                    [ "doc_id", "seg_id", "tok_id", "start_char", "end_char", "annotation_type", "annotation_label"] }
        newAnno["user"] = user_id
        newAnno["role"] = role_id
        newAnno["ghost"] = True
        newAnnotations.append(newAnno)
    if newAnnotations:
        add_annotations(newAnnotations)
    
def set_task_finished(task, finished=True):
    for t in tasks.find(get_query_for_task(task)):
        tasks.update(t, {"$set": {"finished": finished}})
    
    
def reassign_task(user, role, items):
    _query = { "user": "any", "role": role, "items": items }
    tasks.update(_query, {"$set": {"user":user }})

def delete_task(task):
    task["items"].sort(key=lambda x: (x["doc"], x.get("seg", "*"))) 
    return tasks.remove(task)

def add_resourceInfo(resourceInfo):
    if "pinned" in resourceInfo and resourceInfo["pinned"]:
        pinnedResources.update(resourceInfo, {"$setOnInsert": resourceInfo}, upsert=True)
    return resourceInfoDB.update(resourceInfo, {"$setOnInsert": resourceInfo}, upsert=True)

def get_resourceInfo(query):
    return list(resourceInfoDB.find(query)) 
    
def get_all_pinned_resources():
    return list(pinnedResources.find({}))

def remove_resource(query):
    ris = get_resourceInfo(query)
    doc_ids = set(ri["doc_id"] for ri in ris)
    for doc_id in doc_ids:
        print("Removing %s" % doc_id)
        subquery = { "doc_id": doc_id }
        remove_textitems(subquery)
    print("Removed %s documents" % len(doc_ids))
    pinnedResources.remove(query)
    return resourceInfoDB.remove(query)
    
#def add_textitem(textitem):
#    return textitems.update(textitem, {"$setOnInsert": textitem}, upsert=True)

def index_resource_info():
    return resourceInfoDB.create_index("res_id")

def remove_textitems(query):
    return textitems.remove(query)
    
def index_textitems():
    # textitem_idx = {"text":1, "view_id":1, "start_char":1, "tok_offset":1, "end_char":1, "seg_offset":1, "tok_ids":1, "seg_id":1, "doc_id":1}
    textitem_idx = [("view_id", pymongo.ASCENDING), 
                    ("start_char", pymongo.ASCENDING), 
                    ("end_char", pymongo.ASCENDING), 
                    ("seg_id", pymongo.ASCENDING), 
                    ("doc_id", pymongo.ASCENDING)]
    if "textitems" not in db.collection_names() or db.textitems.count() == 0:
        print("Creating textitems index.")
        db.textitems.create_index(textitem_idx, unique=True)
    return textitems.create_index("doc_id")

def add_textitems(textitem_list):
    if "textitems" not in db.collection_names() or db.textitems.count() == 0:
        index_textitems()
    return textitems.insert_many(textitem_list, ordered=False).inserted_ids
    
def get_textitems(query):
    _query = deepcopy(query)
    if "tok_ids" in _query:
        _query["tok_ids"] = {"$in": _query["tok_ids"]}
    return list(textitems.find(_query)) 

def get_entity_by_id(ent_id):
    query = { "id": ent_id }
    return list(db.entity_kb.find(query))
    
def load_entities(filename, limit=0):
    ''' Load a JSON knowledge base into the viewer as the kb '''
    db.entity_kb.drop()
    with open(filename,"r", encoding="utf-8") as fin:
        entities = json.load(fin)
    if limit:
        db.entity_kb.insert_many(entities[:limit]).inserted_ids
    else:
        db.entity_kb.insert_many(entities).inserted_ids    
    return db.entity_kb.create_index("id")
    
####################################
# Full documents
####################################    
    
def add_document(doc):
    return db.documents.insert_one(doc).inserted_id
    
def get_document(query):
    return db.documents.find(query)
    
####################################
# Keywords
####################################    

def add_document_keywords(objs):
    return documentKeywords.insert_many(objs).inserted_ids
    
def get_document_keywords(query):
    return documentKeywords.find_one(query)

####################################
# Users
####################################

def get_all_users():
    return list(userInfo.find({}))

def add_user(user):
    # example: {"user": "some_username", "email": "email@cmu.edu", "subscribed": False}
    if get_user_info(user["user"]):
        return None
    return userInfo.insert_one(user).inserted_id

    
def update_user(user):
    # updates user if he already exists else inserts new one
    return userInfo.update({"user": user["user"]}, user, upsert=True)

def get_user_info(user_id):
    return userInfo.find_one({"user": user_id})
    
def get_users(query):
    return userInfo.find(query)

def remove_user(user_id):
    return userInfo.delete_one({"user": user_id})
    
####################################
# Listeners
####################################

def add_listener(doc):
    return listeners.update(doc, {"$setOnInsert": doc}, upsert=True)
    
def get_listeners():
    return listeners.find({})
