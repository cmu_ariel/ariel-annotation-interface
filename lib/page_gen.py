from __future__ import print_function
from __future__ import unicode_literals
from io import open
import pystache, iso639, json, unicodedata, sys
from collections import defaultdict, Counter

sys.path.append("rmi")
from ariel_rmi import *

class PageGenerator:
    ''' The PageGenerator makes a request from the database and converts it into an HTML page '''
    
    def __init__(self, db, data_connections):
        self.db = db
        self.data_connections = data_connections
        #self.data_edifice = DataEdifice()
        self.templates = {
            "login": "templates/login.html",
            "tasks": "templates/task_index_template.html",
            "documents": "templates/doc_index_template.html",
            "view": "templates/page_template.html",
            "languages": "templates/lang_index_template.html",
            "pinned_resources": "templates/pinned_resource_template.html",
            "users": "templates/user_index_template.html"
        }
        
        
    def addUserPermissions(self, results, user_id):
        user_info = self.db.get_user_info(user_id)
        if "permissions" in user_info:
            results["is_intermediate"] = user_info["permissions"] in [ "intermediate", "admin" ]
            results["is_admin"] = user_info["permissions"] == "admin"
        return results
        

    def render(self, template_id, object):
        with open(self.templates[template_id], "r", encoding="utf-8") as templateFile:
            template = templateFile.read()
            return pystache.render(template, object)
        
    def makeLogin(self):  
        results = {
            "possible_users": self.db.get_all_users()
        }
        results["users_exist"] = len(results["possible_users"]) > 0
        return self.render("login",results)  
    
    def getResName(self, lang_id, res_id):
        for ri in self.db.get_resourceInfo({"lang_id": lang_id, "res_id": res_id}):
            return ri["res_name"]
        return "Warning: Set %s not found" % res_id
    
    def makeUserIndex(self, user_id):
    
        results = { "user_id": user_id, "users": []}
        results = self.addUserPermissions(results, user_id)
        for user in self.db.get_users({}):
            results["users"].append( { "user_id": user["user"], 
                              "permissions": user["permissions"],
                              "subscribed": user["subscribed"],
                              "is_basic": user["permissions"] == "basic",
                              "is_intermediate": user["permissions"] == "intermediate",
                              "is_admin": user["permissions"] == "admin" } )
        return self.render("users", results)    
        
    
    def makeTasksIndex(self, user_id):
    
        result = { "user_id": user_id }
        result = self.addUserPermissions(result, user_id)
    
        with open("config/role_permissions.json", "r", encoding="UTF-8") as fin:
            role_permissions = json.load(fin)
            
        tasks = self.db.get_tasks({"user": user_id})
        tasks += self.db.get_tasks({"user":"any"})
        for task in tasks:
            lang_id = task["lang_id"]
            langInfo = iso639.languages.get(part3=lang_id[:3])
            task["lang_name"] = unicode(langInfo.name, encoding="utf-8")
            res_id = task["res_id"]
            task["task_name"] = role_permissions[task["role"]]["task_name"]
            task["task_desc"] = role_permissions[task["role"]]["task_desc"]
            if "finished" not in task:
                task["finished"] = False
            task["incomplete"] = not task["finished"]
            task["unclaimed"] = task["user"] == "any"       
            task["claimed"] =  task["user"] == user_id    
            task["docs"] = []
            counter = {}
            for item in task["items"]:
                if item["doc"] not in counter:
                    counter[item["doc"]] = 0
                if "seg" not in item:
                    counter[item["doc"]] = 'All'
                elif counter[item["doc"]] != 'All':
                    counter[item["doc"]] += 1
            for doc, seg_count in counter.items():
                task["docs"].append( { "doc_id": doc, "seg_count": seg_count } )
            task["query"] = self.constructDocQuery(task["items"])
            task["sandboxed"] = task["sandboxed"] if "sandboxed" in task else False
        result["tasks"] = tasks
        return self.render("tasks", result)  
    
    def makeDocumentsIndex(self, lang_id, res_id, user_id, role_id, self_url):
    
        results = { "user_id": user_id, 
                    "role_id": role_id,
                    "lang_id": lang_id, 
                    "lang_id_caps": lang_id.upper(),
                    "res_id": res_id, 
                    "res_id_caps": res_id.upper(), 
                    "self_url": self_url,
                    "documents": [],
                    "views": [] }
        results = self.addAllRoles(results, role_id)
        results = self.addUserPermissions(results, user_id)
        documents = defaultdict(list)
        documentsPerView = defaultdict(set)
        viewNames = defaultdict(str)
              
        with open("config/role_permissions.json", "r", encoding="UTF-8") as fin:
            role_permissions = json.load(fin)  
        
        results["possible_users"] = self.db.get_all_users()
        results["possible_roles"] = [ { "role_id": role_id, "task_name": role_permissions[role_id]["task_name"] }
                                                for role_id in role_permissions if role_id not in [ "admin", "viewer" ] ]        
        
        resourceInfos = self.db.get_resourceInfo({"res_id": res_id })
        
        #for ds in self.data_edifice.getSources():
        #    resourceInfos += ds.get_docs({"res_id":res_id})
        
        for ri in resourceInfos:
            doc_id = ri["doc_id"]
            view_id = ri["view_id"]
            view_name = ri["view_name"]
            documents[doc_id].append(ri)
            documentsPerView[view_id].update([doc_id])            
            viewNames[view_id] = view_name
        view_ids = sorted(viewNames.keys())
        results["views"] = [ { "view_id": view_id, 
                            "view_name": viewNames[view_id] } for view_id in view_ids ]
        
        doc_keys = documents.keys()
        doc_ids = sorted(doc_keys)
                    
        
        db_tasks = self.db.get_tasks({"res_id": res_id, "lang_id": lang_id, "finished": {"$ne": True}})
        tasks = defaultdict(set)
        for ts in db_tasks:
            role = ts["role"][:-len("_annotator")] if ts["role"].endswith("_annotator") else ts["role"]
            annotator = role + "/" + ts["user"]
            for item in ts["items"]:
                tasks[item["doc"]].add(annotator)

        db_anns = self.db.get_annotations({ "doc_id": doc_ids })
        annotations = defaultdict(set)
        for ann in db_anns:
            annotator = ann["annotation_type"] + "/" + ann["user"]
            annotations[ann["doc_id"]].add(annotator)

        for doc_id in doc_ids:
            doc = { "doc_id": doc_id, "views": [], "hasKeywords": False, "keywords": [] }
            
            # add keywords if available
            keywords = self.db.get_document_keywords({"doc_id":doc_id})
            if keywords:
                doc["hasKeywords"] = True
                doc["keywords"] = keywords["keywords"]

            # show assigned tasks if available
            if tasks[doc_id]:
                doc["hasTasksAssigned"] = True
                doc["tasks"] = list(tasks[doc_id])
            
            # show available annotations
            if annotations[doc_id]:
                doc["hasAnnotations"] = True
                doc["annotations"] = list(annotations[doc_id])
            
            # add available views
            for view_id in view_ids:
                doc["views"].append({ "view_id": view_id,
                        "hasView": doc_id in documentsPerView[view_id] })
            results["documents"].append(doc)
                
        
        # results["available_annotations"] = self.db.get_annotations_available_for_documents(documents.keys())
        
        # results["ghosts_available"] = len(results["available_annotations"]) > 0        
        
        return self.render("documents", results)            

    def parseDocQuery(self, queryStr):
        segmentsByDoc = {}
        for subquery in queryStr.split("--"):
            parts = subquery.split("-")
            if len(parts) > 1:
                segmentsByDoc[parts[0]] = [int(p) for p in parts[1:]]
            else:
                segmentsByDoc[parts[0]] = '*'
        return segmentsByDoc

        
    def constructDocQuery(self, query):
        segmentsByDoc = {}
        for item in query:
            if "doc" not in item:
                continue
            doc_id = item["doc"]
            if "seg" not in item:
                segmentsByDoc[doc_id] = '*'
            elif doc_id in segmentsByDoc and segmentsByDoc[doc_id] == '*':
                continue
            elif doc_id not in segmentsByDoc:
                segmentsByDoc[doc_id] = [ item["seg"] ]
            elif item["seg"] not in segmentsByDoc[doc_id]:
                segmentsByDoc[doc_id].append(item["seg"])
        
        results = []
        
        doc_ids = sorted(segmentsByDoc.keys())
        
        for doc_id in doc_ids:
            seg_ids = segmentsByDoc[doc_id]
            results.append( doc_id + '-' + "-".join(seg_ids) if seg_ids != '*' else doc_id )
        
        return "--".join(results)
                
    def addAllRoles(self, results, role_id):
        results["all_roles"] = []
        
        with open("config/role_permissions.json", "r", encoding="UTF-8") as fin:
            role_permissions = json.load(fin) 
            for role in sorted(role_permissions.keys()):
                role_name = role_permissions[role]["role_name"]
                results["all_roles"].append({ "role_id": role, 
                                                "role_name": role_name, 
                                                "selected": role == role_id,
                                                "not_selected": role != role_id })
                
        return results
    
    def makeViewPage(self, query, user_id, role_id, task_id=None):
        
        with open("config/role_permissions.json", "r", encoding="UTF-8") as fin:
            role_permissions = json.load(fin)    
            
        if role_id not in role_permissions:
            role_id = "viewer"
        
        results = { 
            "task_id": task_id,
            "user_id": user_id,
            "role_id": role_id,
            "query": query,
            "title": "Query results" if '-' in query else query,
            "views": [],
            #"resourceCategories": self.makeCheckboxes(),
            "documents": [],
            "task_name": role_permissions[role_id]["task_name"],
            "role_name": role_permissions[role_id]["role_name"],
            "task_desc": role_permissions[role_id]["task_desc"],
        }
        
        results = self.addAllRoles(results, role_id)
        results = self.addUserPermissions(results, user_id)
        
        results["possible_users"] = self.db.get_all_users()
        results["possible_roles"] = [ { "role_id": role_id, "task_name": role_permissions[role_id]["task_name"] }
                                                for role_id in role_permissions if role_id not in [ "admin", "viewer" ] ]
        
        # parse the query
        segmentsByDoc = self.parseDocQuery(query)   
        #print(segmentsByDoc)
        
        # see what annotations are available
        results["anno_views"] = []
        visibleTypes = role_permissions[role_id]["view_permissions"] if role_id in role_permissions else []
        for doc_id, seg_ids in segmentsByDoc.items():
            if seg_ids == '*':
                db_query = { "doc_id":doc_id, 
                             "annotation_type": visibleTypes }
            else:
                db_query = { "doc_id":doc_id,
                             "annotation_type": visibleTypes,
                             "seg_offset":seg_ids }
            
            annotations = self.db.get_annotations(db_query)
            for annotation in annotations:
                anno_view = { "user": annotation["user"], 
                                "anno_type": annotation["annotation_type"],
                                "anno_view_name": annotation["user"] + ": " + annotation["annotation_type"],
                                "anno_view_id": annotation["user"].lower().replace(" ","_") + "_" + annotation["annotation_type"] }
                if annotation["user"] != user_id and anno_view not in results["anno_views"]:
                    results["anno_views"].append(anno_view)
            
         
        doc_ids = sorted(segmentsByDoc.keys())
        view_ids = []
        view_priorities = {}
        view_names = {}
        for doc_id in doc_ids:
            ris = self.db.get_resourceInfo({"doc_id":doc_id})
            for dc in self.data_connections:
                ris += dc.getResources(doc_id, "original")
            if not ris:
                continue
            ri = ris[0]
            results["lang_id"] = ri["lang_id"]
            results["res_id"] = ri["res_id"]
            segments = defaultdict(lambda:defaultdict(dict))
            seg_ids = {}
            allTokens = self.db.get_textitems( {"doc_id": doc_id } )
            for dc in self.data_connections:
                allTokens += dc.getTokens(doc_id)
            for token in allTokens:
                seg_offset = token["seg_offset"]
                if segmentsByDoc[doc_id] != "*" and seg_offset not in segmentsByDoc[doc_id]:
                    continue
                tok_offset = token["tok_offset"]
                view_id = token["view_id"]
                #view_ids.append(view_id)
                segments[seg_offset][view_id][tok_offset] = token
                seg_ids[seg_offset] = token["seg_id"]
                if "token_align" in token and token["token_align"]:
                    token["tok_ids"] = token["tok_ids"]
                    token["tok_ids_str"] = ",".join(token["tok_ids"])
                else:
                    token["tok_ids"] = []
                    token["tok_ids_str"] = ''
                #view_names[view_id] = token["view_name"]
                
                if view_id not in view_ids:
                    view_ids.append(view_id)
                    ri = self.db.get_resourceInfo({"doc_id":doc_id, "view_id": view_id})
                    for dc in self.data_connections:
                        ri += dc.getResources(doc_id, view_id)
                    view_names[view_id] = ri[0]["view_name"] if ri else view_id
                    view_priorities[view_id] = ri[0]["view_priority"] if ri else 5.0
            
            view_ids.sort(key=lambda x:view_priorities[x])
            
            seg_offsets = sorted(segments.keys())
            
            doc_result = { "doc_id": doc_id, "segments": [] }
            for seg_offset in seg_offsets:
                seg_result = { "doc_id": doc_id,
                        "seg_id": seg_ids[seg_offset],
                        "views": [] }
                        
                for i, view_id in enumerate(view_ids):
                    if view_id not in segments[seg_offset].keys():
                        continue
                        
                    view_result = { "view_name": view_names[view_id], 
                             "view_id": view_id, 
                             "i": i,
                             "direction": "ltr", 
                             "tokens": [] }
                             
                    tok_offsets = sorted(segments[seg_offset][view_id].keys())
                    viewDirectionCounter = Counter()
                    viewDirectionCounter["ltr"] += 1  # bias towards LTR
                    
                    for tok_offset in tok_offsets:
                        tok_result = segments[seg_offset][view_id][tok_offset]
                        tokDirectionCounter = Counter()
                        tokDirectionCounter["ltr"] += 1  # bias towards LTR
                        for c in tok_result["text"]:
                            if unicodedata.bidirectional(c) == 'L':
                                viewDirectionCounter["ltr"] += 1
                                tokDirectionCounter["ltr"] += 1 
                            elif unicodedata.bidirectional(c) in ['R', 'AL']:
                                viewDirectionCounter["rtl"] += 1
                                tokDirectionCounter["rtl"] += 1 
                        #tok_result["direction"] = tokDirectionCounter.most_common()[0][0]
                        view_result["tokens"].append(tok_result)
                    direction = viewDirectionCounter.most_common()[0][0]
                    view_result["direction"] = direction
                    for token in view_result["tokens"]:
                        token["direction"] = direction
                    seg_result["views"].append(view_result)
                doc_result["segments"].append(seg_result)
            results["documents"].append(doc_result)
    
    
        for view_id in view_ids:
            results["views"].append({ "view_id": view_id,
                                 "view_name": view_names[view_id] })
                                 
        results["available_annotations"] = self.db.get_annotations_available_for_documents(doc_ids)
        
        results["ghosts_available"] = len(results["available_annotations"]) > 0    
        
        return self.render("view", results)
        
    def makeLanguagesIndexAux(self, pinned=False):
    
        resNames = defaultdict(lambda:defaultdict(lambda:defaultdict(str)))
        resources = defaultdict(lambda:defaultdict(lambda:defaultdict(set)))
        numDocsByRes = defaultdict(lambda:defaultdict(lambda:defaultdict(set)))
        
        resourceInfos = self.db.get_all_pinned_resources() if pinned else self.db.get_resourceInfo({})
        
        #for ds in self.data_edifice.getSources():
        #    resourceInfos += ds.get_docs()
        
        
        for dc in self.data_connections:
            resourceInfos += dc.getAllResources()
        
        for ri in resourceInfos:
            lang_id = ri["lang_id"]
            res_id = ri["res_id"]
            res_name = ri["res_name"]
            res_type_name = ri["res_type_name"]
            view_id = ri["view_id"]
            view_name = ri["view_name"]
            doc_id = ri["doc_id"]
            resNames[lang_id][res_type_name][res_id] = res_name
            resources[lang_id][res_type_name][res_id].update([view_name])
            numDocsByRes[lang_id][res_type_name][res_id].update([doc_id])
             
        langNodes = []
        for lang_id in sorted(resources.keys()):
            resTypeNodes = []
            for res_type_name in sorted(resources[lang_id].keys()):
                resNodes = []
                for res_id in sorted(resources[lang_id][res_type_name].keys()):
                    res_name = resNames[lang_id][res_type_name][res_id]
                    view_names = sorted(resources[lang_id][res_type_name][res_id])
                    numDocs = len(numDocsByRes[lang_id][res_type_name][res_id])
                    resNodes.append({ "res_id": res_id, "res_name": res_name, "num_docs":numDocs, "views": view_names })
                resTypeNodes.append({ "res_type_name": res_type_name, "resources": resNodes })
                
            try:
                langInfo = iso639.languages.get(part3=lang_id[:3])
                langName = unicode(langInfo.name, encoding="utf-8")
                langShortCode = langInfo.part1 if langInfo.part1 else lang_id
            except:
                langName = 'Unknown'
                langShortCode = lang_id
            langNodes.append({"lang_id": lang_id, 
                            "lang_id_upper": lang_id.upper(),
                            "lang_name": langName,
                            "lang_name_lower": langName.lower(),
                            "lang_short_code": langShortCode,
                            "resource_types":resTypeNodes})
        
        return langNodes
    
    
    def makeLanguagesIndex(self, user_id):
        langNodes = self.makeLanguagesIndexAux()
        result = { "user_id": user_id, "langs": langNodes }
        result = self.addUserPermissions(result, user_id)
        return self.render("languages", result)

    def makePinnedResourcesIndex(self, user_id):
        langNodes = self.makeLanguagesIndexAux(True)
        result = { "user_id": user_id, "langs": langNodes }
        result = self.addUserPermissions(result, user_id)
        return self.render("pinned_resources", result)
