#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function
from __future__ import unicode_literals
from io import open
import argparse, json, requests, logging, sys, os, copy, traceback, glob
from gevent.wsgi import WSGIServer
from gevent import monkey; monkey.patch_socket()
from bson import json_util 
import lxml.etree as ET

from flask import Flask, request, Response, make_response, redirect
app = Flask(__name__)

logger = logging.getLogger()
logger.setLevel(logging.DEBUG)
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
ch.setFormatter(formatter)
logger.addHandler(ch)

DIR = ''
RES_NAME = ''
RES_ID = ''
RES_TYPE = ''
RES_TYPE_NAME = ''
LANG = ''
VIEW_NAME = ''
VIEW_ID = ''
VIEW_PRIORITY = 0.0
TOKEN_ALIGN = True
PINNED = True
DOC_IDS = set()


def build_resource_json(doc_id):
    return {
        "lang_id": LANG,
        "res_name": RES_NAME,
        "res_id": RES_ID,
        "res_type": RES_TYPE,
        "res_type_name": RES_TYPE_NAME,
        "view_name": VIEW_NAME,
        "view_id": VIEW_ID,
        "view_priority": VIEW_PRIORITY,
        "token_align": TOKEN_ALIGN,
        "pinned": PINNED,
        "doc_id": doc_id}


@app.route("/get_all_resources", methods=["POST"]) 
def get_all_resources(): 
    responseData = [build_resource_json(doc_id) for doc_id in DOC_IDS]
    return json.dumps({ "success":True, "data": responseData, "message":'' }, 
        default=json_util.default)


@app.route("/get_resources_by_id", methods=["POST"]) 
def get_resources_by_id(): 
    data = request.get_json()
    res_id = data["res_id"]
    if res_id != RES_ID:
        return json.dumps({ "success":True, "data": [], "message":'' }, default=json_util.default)
    return get_all_resources() 
    
@app.route("/get_resources", methods=["POST"]) 
def get_resources():  
    data = request.get_json()
    responseData = []
    doc_id = data["doc_id"]
    view_id = data["view_id"]
    if view_id != VIEW_ID:
        return json.dumps({ "success":True, "data": responseData, "message":'' }, default=json_util.default)
    responseData = [ build_resource_json(doc_id) ]
    return json.dumps({ "success":True, "data": responseData, "message":'' }, default=json_util.default)    
    
@app.route("/get_tokens", methods=["POST"]) 
def get_tokens():  
    data = request.get_json()
    tokens = []
    doc_id = data["doc_id"]
    if doc_id not in DOC_IDS:
        return json.dumps({ "success":True, "data": tokens, "message":'' }, default=json_util.default)
        
    filename = os.path.join(DIR, "%s.ltf.xml" % doc_id)
    
    tree = ET.parse(filename)
    
    for segOffset, segmentNode in enumerate(tree.xpath(".//SEG")):
        seg_id = segmentNode.attrib["id"]
        for tokenOffset, t in enumerate(segmentNode.xpath(".//TOKEN")):
            tokens.append({
                "doc_id": doc_id,
                "view_id": VIEW_ID,
                "seg_id": seg_id,
                "seg_offset": segOffset,
                "tok_offset": tokenOffset,
                "text": t.text if t.text else ' ',
                "tok_ids": t.attrib["id"].split(),
                "start_char": int(t.attrib["start_char"]) if "start_char" in t.attrib else -1,
                "end_char": int(t.attrib["end_char"]) if "end_char" in t.attrib else -1,
                "token_align": TOKEN_ALIGN
            })
            
    return json.dumps({ "success":True, "data": tokens, "message":'' }, default=json_util.default)
    

if __name__ == '__main__':
    argparser = argparse.ArgumentParser()
    argparser.add_argument("dir", help="Directory to serve files from")
    argparser.add_argument("lang", help="Source language of the files.  If the resource is, say, Turkish-English parallel text, use 'tur' for both")
    argparser.add_argument("--viewname",default="Original", help="What to call this view in the knowledge base [\"Original\"]")
    argparser.add_argument("--resname",default="Uncategorized", help="What to call this resource in the knowledge base [\"Uncategorized\"]")
    argparser.add_argument("--rescat",default="Monolingual Text", help="What kind of resource (monolingual, parallel, etc.) is this [\"monolingual text\"]")
    argparser.add_argument("--priority",default="0.0", help="Where to display this view compared to other views")
    argparser.add_argument("--token_align",default=True)
    argparser.add_argument("--pinned",default=True)
    argparser.add_argument("--port", type=int, default=5000, help="Port for serving the files.")
    args = argparser.parse_args()
    
    DIR = args.dir
    LANG = args.lang
    VIEW_NAME = args.viewname
    VIEW_ID = VIEW_NAME.lower().replace("-","_").replace(" ","_").replace(".","_")
    RES_NAME = args.resname
    RES_ID = RES_NAME.lower().replace("-","_").replace(" ","_").replace(".","_")
    RES_TYPE_NAME = args.rescat
    RES_TYPE = RES_TYPE_NAME.lower().replace("-","_").replace(" ","_").replace(".","_")
    VIEW_PRIORITY = args.priority
    TOKEN_ALIGN = True
    PINNED = True
    
    for filename in glob.glob(os.path.join(args.dir, "*.ltf.xml")):
        doc_id = os.path.basename(filename).split(".")[0]
        DOC_IDS.add(doc_id)
    
    http_server = WSGIServer(('', args.port), app)
    http_server.serve_forever()
    