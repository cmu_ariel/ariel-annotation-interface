#!/bin/bash

[[ $# -ne 1 ]] && { echo "Usage: $0 3-letter-lang-iso-code"; exit 1; }
lang=$1

cd $(dirname $(readlink -f $0))/../
mkdir -p logs

source scripts/config.sh

mkdir -p /home/zsheikh/snowmass/mongodb/data/db_$lang

source activate python2
set -x
/home/zsheikh/snowmass/mongodb/bin/mongod --dbpath /home/zsheikh/snowmass/mongodb/data/db_$lang \
  --port $db_port &> logs/log.mongodb.$db_port &
sleep 2
nohup python2 ./viewer.py \
  --db_name "lorelei_viewer_$lang" \
  --db_port $db_port \
  --port $viewer_port &> logs/log.viewer.$viewer_port &
set +x

echo "http://miami.lti.cs.cmu.edu:$viewer_port"
