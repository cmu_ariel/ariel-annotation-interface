echo "lang is $lang"
case $lang in
  "alb"|"8000")
    db_port=27000
    viewer_port=8000
    lang="alb"
    ;;
  "hin"|"8001")
    db_port=27001
    viewer_port=8001
    lang="hin"
    ;;
  "ind"|"8002")
    db_port=27002
    viewer_port=8002
    lang="ind"
    ;;
  "spa"|"8003")
    db_port=27003
    viewer_port=8003
    lang="spa"
    ;;
  "tha"|"8004")
    db_port=27004
    viewer_port=8004
    lang="tha"
    ;;
  "uig"|"8006")
    db_port=27006
    viewer_port=8006
    lang="uig"
    ;;
  "ori"|"8007")
    db_port=27007
    viewer_port=8007
    lang="ori"
    ;;
  "ilo"|"8008")
    db_port=27008
    viewer_port=8008
    lang="ilo"
    ;;
  "ori_dev"|"8011")
    db_port=27011
    viewer_port=8011
    lang="ori_dev"
    ;;
  "ilo_dev"|"8012")
    db_port=27012
    viewer_port=8012
    lang="ilo_dev"
    ;;
  "tpi"|"8014")
    db_port=27014
    viewer_port=8014
    lang="tpi"
    ;;
  "gre"|"8015")
    db_port=27015
    viewer_port=8015
    lang="gre"
    ;;
  *)
    echo "Lang not supported!"
    exit 1
    ;;
esac
