#!/bin/bash

[[ $# -ne 2 ]] && { echo "Usage: $0 resource_id output_dir/"; exit 1; }

resource_id=$1
output_dir=$2
db_port=27026
lang="sin"

cd $(dirname $(readlink -f $0))/../utils/
./dump_conll.py $output_dir $resource_id --db_port $db_port --db_name lorelei_viewer_$lang
  #--auto_users ldc default --include_users NI AL lori david2 cmalaviya Peter
