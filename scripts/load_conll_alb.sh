#!/bin/bash

[[ $# -ne 2 ]] && { echo "Usage: $0 unique_resource_id input_conll_dir/"; exit 1; }

resource_id=$1
input_dir=$(readlink -f $2)

db_port=27000
lang="alb"

cd $(dirname $(readlink -f $0))/../utils/
set -x
python2 load_conll_lorehlt18.py --lang $lang --db_port $db_port --db_name lorelei_viewer_$lang $input_dir $resource_id
