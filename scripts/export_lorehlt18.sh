#!/bin/bash

[[ $# -ne 3 ]] && { echo "Usage: $0 port_number resource_id output_dir/"; exit 1; }

port_no=$1
resource_id=$2
output_dir=$(readlink -f $3)

case $port_no in
  "8095"|"kin")
    db_port=27025
    lang="kin"
    ;;
  "8096"|"sin")
    db_port=27026
    lang="sin"
    ;;
  "8097"|"kin_NI")
    db_port=27027
    lang="kin_NI"
    ;;
  "8098"|"sin_NI")
    db_port=27028
    lang="sin_NI"
    ;;
  *)
    echo "Invalid port_number!"
    exit 1
    ;;
esac

cd $(dirname $(readlink -f $0))/../utils/
python2 ./dump_conll.py $output_dir $resource_id --db_port $db_port --db_name lorelei_viewer_$lang
  #--auto_users ldc default --include_users NI AL
