#!/bin/bash

[ $# -ne 2 ] && { echo "Usage: $0 lang resource_dir"; exit 1; }
lang=$1
resource_dir=$(readlink -ve $2) || exit 1

script_dir=$(dirname $(readlink -f $0))
source $script_dir/config.sh

cd $script_dir/../utils/
source activate python2
set -x
python load_resources.py --db_port $db_port --db_name lorelei_viewer_$lang $resource_dir
