#!/bin/bash
# load the data using load_resources.py, see example at /usr0/home/zsheikh/langs/som/
# start mongodb (see run_db_ben.sh)
#nohup python2 ./data_server_ipa.py ben-Beng --db_port 27019 --port 5118 &> log.data_server_ipa.5118 &
#nohup python2 ./viewer.py --db_port 27019 --port 8088 --servers config/server_ben.json &> log.viewer.8088 &
nohup python2 ./viewer.py --db_port 27020 --port 8089 &> log.viewer.8089 &
