#!/bin/bash -x

db_port=27023
viewer_port=8092
lang="ben_test"

cd $(dirname $(readlink -f $0))/../
mkdir -p logs

mkdir -p /home/zsheikh/snowmass/mongodb/data/db_$lang
/home/zsheikh/snowmass/mongodb/bin/mongod --dbpath /home/zsheikh/snowmass/mongodb/data/db_$lang \
  --port $db_port &> logs/log.mongodb.$db_port &
sleep 2

# change next 2 lines if needed
nohup python2 ./data_server_ipa.py ben-Beng-red \
  --db_name "lorelei_viewer_$lang" \
  --db_port $db_port \
  --port 5118 &> logs/log.data_server_ipa.5118 &
nohup python2 ./data_server_roman.py ben-Beng-red \
  --db_name "lorelei_viewer_$lang" \
  --db_port $db_port \
  --port 5120 &> logs/log.data_server_roman.5120 &
nohup python2 ./viewer.py --servers config/server_ben.json \
  --db_name "lorelei_viewer_$lang" \
  --db_port $db_port \
  --port $viewer_port &> logs/log.viewer.$viewer_port &
