#!/bin/bash -x

db_port=27000
viewer_port=8000
lang="alb"

cd $(dirname $(readlink -f $0))/../
mkdir -p logs

mkdir -p /home/zsheikh/snowmass/mongodb/data/db_$lang
/home/zsheikh/snowmass/mongodb/bin/mongod --dbpath /home/zsheikh/snowmass/mongodb/data/db_$lang \
  --port $db_port &> logs/log.mongodb.$db_port &
sleep 2

nohup python2 ./viewer.py \
  --db_name "lorelei_viewer_$lang" \
  --db_port $db_port \
  --port $viewer_port &> logs/log.viewer.$viewer_port &
