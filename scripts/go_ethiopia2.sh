#!/bin/bash
./data_server_ipa.py tir-Ethi-pp --db_port 27015 --port 5116 &
./data_server_ipa.py orm-Latn --db_port 27015 --port 5117 &
./data_server_ipa.py amh-Ethi-pp --db_port 27015 --port 5120 &
./viewer.py --db_port 27015 --port 8080