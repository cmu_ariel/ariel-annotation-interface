#!/bin/bash

# Valid port numbers:
# kin non-native annotators:  8095 (these have IPA/romanization/word-to-word translations)
# sin non-native annotators:  8096 (these have IPA/romanization/word-to-word translations)
# kin Native Informants:  8097
# sin Native Informants:  8098

[[ $# -ne 3 ]] && { echo "Usage: $0 port_number unique_resource_id input_conll_dir/"; exit 1; }

port_no=$1
resource_id=$2
input_dir=$(readlink -f $3)

case $port_no in
  "8095"|"kin")
    db_port=27025
    lang="kin"
    ;;
  "8096"|"sin")
    db_port=27026
    lang="sin"
    ;;
  "8097"|"kin_NI")
    db_port=27027
    lang="kin_NI"
    ;;
  "8098"|"sin_NI")
    db_port=27028
    lang="sin_NI"
    ;;
  *)
    echo "Invalid port_number!"
    exit 1
    ;;
esac

cd $(dirname $(readlink -f $0))/../utils/
set -x
python2 load_conll_lorehlt18.py --lang ${lang:0:3} --db_port $db_port --db_name lorelei_viewer_$lang $input_dir $resource_id
