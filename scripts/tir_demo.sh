#!/bin/bash


db_port=27005
viewer_port=8005
lang="tirdemo"
ipa_port=5116
roman_port=5117
cd $(dirname $(readlink -f $0))/../
mkdir -p logs

mkdir -p /home/zsheikh/snowmass/mongodb/data/db_$lang

set -x
pgrep -af db_$lang || \
  /home/zsheikh/snowmass/mongodb/bin/mongod --dbpath /home/zsheikh/snowmass/mongodb/data/db_$lang \
  --port $db_port &> logs/log.mongodb.$db_port &
sleep 2
pgrep -af "db_port $db_port"
pkill -f "db_port $db_port"
nohup python2 ./data_server_ipa.py tir-Ethi \
  --db_port $db_port --db_name "lorelei_viewer_$lang" --port $ipa_port \
  &> logs/log.data_server_ipa.$ipa_port &
nohup python2 ./data_server_roman.py tir-Ethi \
  --db_port $db_port --db_name "lorelei_viewer_$lang" --port $roman_port \
  &> logs/log.data_server_roman.$roman_port &
nohup python2 ./viewer.py \
  --db_name "lorelei_viewer_$lang" \
  --db_port $db_port \
  --servers config/server_tir.json \
  --port $viewer_port &> logs/log.viewer.$viewer_port &
set +x

echo "http://miami.lti.cs.cmu.edu:$viewer_port"
