#!/bin/bash
python2 ./data_server_xsampa_ipa.py tir --db_port 27001 --port 5998 &
python2 ./data_server_natural.py tir /home/data/LoReHLT17/internal/Morph/Tir_ASR/v5/tir_morph.py --db_port 27001 --port 5997 &
python2 ./data_server_xsampa_ipa.py orm --db_port 27001 --port 5996 &
python2 ./data_server_natural.py orm /home/data/LoReHLT17/internal/Morph/Orm_ASR/v4/orm_morph.py --db_port 27001 --port 5995 &
python2 ./viewer.py --db_port 27001 --port 5999 --servers config/server_tir_asr.json