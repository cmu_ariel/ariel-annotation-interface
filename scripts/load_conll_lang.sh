#!/bin/bash

[[ $# -lt 3 ]] && { echo "Usage: $0 3-letter-lang-iso-code unique_resource_id input_conll_dir/ [--morph]"; exit 1; }

lang=$1
resource_id=$2
input_dir=$(readlink -f $3)
morph=$4

sdir=$(dirname $(readlink -f $0))
source $sdir/config.sh

cd $sdir/../utils/
source activate python2
set -x
python2 load_conll_lorehlt18.py --lang $lang --db_port $db_port --db_name lorelei_viewer_$lang $morph $input_dir $resource_id
