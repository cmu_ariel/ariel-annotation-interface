#!/bin/bash -x

db_port=27025
lang="kin"

cd $(dirname $(readlink -f $0))/../utils/
python load_resources.py --db_port $db_port --db_name lorelei_viewer_$lang $1
