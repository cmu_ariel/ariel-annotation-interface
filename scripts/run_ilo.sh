#!/bin/bash

db_port=27012
viewer_port=8012
lang="ilo_dev"

epitran_code="ilo-Latn"
ipa_port=7118
rom_port=7119

source activate python2
set -x 

cd $(dirname $(readlink -f $0))/../
mkdir -p logs

mkdir -p /home/zsheikh/snowmass/mongodb/data/db_$lang
/home/zsheikh/snowmass/mongodb/bin/mongod --dbpath /home/zsheikh/snowmass/mongodb/data/db_$lang \
  --port $db_port &> logs/log.mongodb.$db_port &
sleep 2

# change next 2 lines if needed
nohup python2 ./data_server_ipa.py $epitran_code \
  --lang $lang \
  --db_name "lorelei_viewer_$lang" \
  --db_port $db_port \
  --port $ipa_port &> logs/log.data_server_ipa.$ipa_port &
nohup python2 ./data_server_roman.py $epitran_code \
  --lang $lang \
  --db_name "lorelei_viewer_$lang" \
  --db_port $db_port \
  --port $rom_port &> logs/log.data_server_roman.$rom_port &
nohup python2 ./viewer.py --servers config/server_${lang}.json \
  --db_name "lorelei_viewer_$lang" \
  --db_port $db_port \
  --port $viewer_port &> logs/log.viewer.$viewer_port &
