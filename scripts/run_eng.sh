#!/bin/bash -x

db_port=27022
viewer_port=8091
../mongodb/bin/mongod --dbpath ../mongodb/data/db_eng/ --port $db_port &>/dev/null &
sleep 2
nohup python2 ./viewer.py --db_port $db_port --port $viewer_port &> log.viewer.$viewer_port &
