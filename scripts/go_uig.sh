#!/bin/bash
# load the data using load_resources.py, see example at /usr0/home/zsheikh/langs/som/
# start mongodb (see run_db_uig.sh)
#nohup python2 ./data_server_ipa.py uig-Beng-red --db_port 27019 --port 5118 &> log.data_server_ipa.5118 &
nohup python2 ./viewer.py --db_port 27021 --port 8090 --servers config/server_uig.json &> log.viewer.8090 &
