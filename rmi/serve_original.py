#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function
from __future__ import unicode_literals
from io import open
import argparse, json, requests, logging, sys, os, copy, traceback, glob
#from gevent.wsgi import WSGIServer
#from gevent import monkey; monkey.patch_socket()
#from bson import json_util 
import lxml.etree as ET

import Pyro4
from ariel_rmi import *

logger = logging.getLogger()
logger.setLevel(logging.DEBUG)
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
ch.setFormatter(formatter)
logger.addHandler(ch)

DIR = ''
RES_NAME = ''
RES_ID = ''
RES_TYPE = ''
RES_TYPE_NAME = ''
LANG = ''
VIEW_NAME = ''
VIEW_ID = ''
VIEW_PRIORITY = 0.0
TOKEN_ALIGN = True
PINNED = True
DOC_IDS = set()
DOCS = []

@Pyro4.expose
class DocumentServer(object):

    #def get_res_ids_by_lang(self, lang):
    #    return [ RES_ID ] if lang == LANG else []
        
    def get_docs(self, query={}):
        if not query:   # for efficiency's sake
            return DOCS
        q = query.viewitems()
        return [ d for d in DOCS if q < d.viewitems() ]
    
    def get_tokens(self, doc_id):
        if doc_id not in DOC_IDS:
            return []
            
        filename = os.path.join(DIR, "%s.ltf.xml" % doc_id)
        
        tree = ET.parse(filename)
        tokens = []       
        for segOffset, segmentNode in enumerate(tree.xpath(".//SEG")):
            seg_id = segmentNode.attrib["id"]
            for tokenOffset, t in enumerate(segmentNode.xpath(".//TOKEN")):
                tokens.append({
                    "doc_id": doc_id,
                    "view_id": VIEW_ID,
                    "seg_id": seg_id,
                    "seg_offset": segOffset,
                    "tok_offset": tokenOffset,
                    "text": t.text if t.text else ' ',
                    "tok_ids": t.attrib["id"].split(),
                    "start_char": int(t.attrib["start_char"]) if "start_char" in t.attrib else -1,
                    "end_char": int(t.attrib["end_char"]) if "end_char" in t.attrib else -1,
                    "token_align": TOKEN_ALIGN
                })
        return tokens

        
    def fill_query(self, query):
        ''' A query is a JSON document of the following form:
            [ { "doc_id": "SAJFDLASKJGDLAKJG",
                "views": [
                    { "view_id": "dasifjafd",
                      "segments": [
                        { "seg_id": "segment-0" },
                        { "seg_id": "segment-1" }
                        ...
                    ] }
                    ...
                ]
             ...
            ]
    
            Querying a data source fills out this structure with tokens and some additional metadata like
            the view priority (in what order to present these to a user).  If view_id or seg_id is "*", 
            all views/segments are returned.
        ''' 
        query = copy.deepcopy(query)
        for doc in query:
            doc_id = doc["doc_id"]
            filename = os.path.join(DIR, "%s.ltf.xml" % doc_id)
            if not os.path.exists(filename):
                continue
            tree = ET.parse(filename)
            for view in doc["views"]:
                if view["view_id"] not in ["original","*"]:
                    continue
                view["view_priority"] = VIEW_PRIORITY
                view["token_align"] = TOKEN_ALIGN
                for seg_index, seg in enumerate(view["segments"]):
                    seg_id = seg["seg_id"]
                    seg["seg_index"] = seg_index
                    seg_node = tree.xpath(".//SEG[@id=%s" % seg)
                    seg["tokens"] = []
                    for tok_index, t in enumerate(segmentNode.xpath(".//TOKEN")):
                        seg["tokens"].append({
                            "tok_index": tok_index,
                            "text": t.text if t.text else ' ',
                            "tok_ids": t.attrib["id"].split(),
                            "start_char": int(t.attrib["start_char"]) if "start_char" in t.attrib else -1,
                            "end_char": int(t.attrib["end_char"]) if "end_char" in t.attrib else -1,
                        })
            return query
                            
                

if __name__ == '__main__':
    argparser = argparse.ArgumentParser()
    argparser.add_argument("dir", help="Directory to serve files from")
    argparser.add_argument("lang", help="Source language of the files.  If the resource is, say, Turkish-English parallel text, use 'tur' for both")
    argparser.add_argument("--viewname",default="Original", help="What to call this view in the knowledge base [\"Original\"]")
    argparser.add_argument("--resname",default="Uncategorized", help="What to call this resource in the knowledge base [\"Uncategorized\"]")
    argparser.add_argument("--rescat",default="Uncategorized", help="What kind of resource (monolingual, parallel, etc.) is this [\"monolingual text\"]")
    argparser.add_argument("--priority",default="0.0", help="Where to display this view compared to other views")
    argparser.add_argument("--token_align",default=True)
    argparser.add_argument("--pinned",default=True)
    #argparser.add_argument("--port", type=int, default=5000, help="Port for serving the files.")
    args = argparser.parse_args()
    
    DIR = args.dir
    LANG = args.lang
    VIEW_NAME = args.viewname
    VIEW_ID = VIEW_NAME.lower().replace("-","_").replace(" ","_").replace(".","_")
    RES_NAME = args.resname
    RES_ID = RES_NAME.lower().replace("-","_").replace(" ","_").replace(".","_")
    RES_TYPE_NAME = args.rescat
    RES_TYPE = RES_TYPE_NAME.lower().replace("-","_").replace(" ","_").replace(".","_")
    VIEW_PRIORITY = args.priority
    TOKEN_ALIGN = True
    PINNED = True
    
    for filename in glob.glob(os.path.join(args.dir, "*.ltf.xml")):
        doc_id = os.path.basename(filename).split(".")[0]
        DOC_IDS.add(doc_id)
        DOCS.append({
            "lang_id": LANG,
            "res_id": RES_ID,
            "res_name": RES_NAME,
            "res_type": RES_TYPE,
            "res_type_name": RES_TYPE_NAME,
            "doc_id": doc_id,
            "view_id": VIEW_ID,
            "view_name": VIEW_NAME,
            "view_priority": VIEW_PRIORITY,
            "pinned": PINNED,
            "token_align": TOKEN_ALIGN        
        })
        
    metadata_supplied = set(["data.%s.original"])
    with ServerWrapper(DocumentServer, metadata_supplied) as s:
        s.serve()


    