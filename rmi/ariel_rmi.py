# -*- coding: utf-8 -*-

import logging, itertools, random
import Pyro4
from itertools import chain
from collections import defaultdict

flatten = chain.from_iterable

class DataEdifice(object):

    def __init__(self, query={}):
        self.nameserver = Pyro4.locateNS()
        self.metadata = set(["dataserver"])
        if "lang_id" in self.query:
            self.metadata.add("lang.%s" % query["lang_id"])
        if "view_id" in self.query:
            self.metadata.add("view.%s" % query["view_id"])
        
    def get_servers(self):
        serverAddresses = self.nameserver.list(metadata_all=self.metadata)
        return [Pyro4.Proxy(uri) for uri in serverAddresses.values()]
        
    def get_docs(self, query={}):
        return flatten(p.get_docs(query) for p in self.get_servers())
        
    def get_tokens(self, doc_id):
        return flatten(p.get_tokens(doc_id) for p in self.get_servers())
        

class ServerWrapper(object):

    def __init__(self, cls, metadata):
        self.name = "ariel.document_server_%09d" % random.randint(0, 999999999)
        self.cls = cls
        self.metadata = metadata
        self.daemon = Pyro4.Daemon()
        self.nameserver = Pyro4.locateNS() 
        self.uri = ''
    
    def __enter__(self):
        self.uri = self.daemon.register(self.cls)   
        self.nameserver.register(self.name, self.uri, metadata=self.metadata)
        logging.info("Serving at %s" % self.name)
        return self
        
    def serve(self):
        self.daemon.requestLoop()   
    
    def __exit__(self, type, value, traceback):
        logging.info("Closing %s" % self.name)
        self.nameserver.remove(self.name)
    