#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function
from __future__ import unicode_literals
from io import open
import argparse, json, requests, logging, sys, os, copy, traceback, glob, random
#from gevent.wsgi import WSGIServer
#from gevent import monkey; monkey.patch_socket()
#from bson import json_util 
import lxml.etree as ET

import Pyro4
from ariel_rmi import *
from panphon.xsampa import XSampa

logger = logging.getLogger()
logger.setLevel(logging.DEBUG)
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
ch.setFormatter(formatter)
logger.addHandler(ch)

LANG = ''
VIEW_NAME = 'IPA'
VIEW_ID = 'ipa'
VIEW_PRIORITY = 4.0
ORIGIN_VIEW_ID = 'xsampa'
TOKEN_ALIGN = True

DATA_EDIFICE = DataEdifice({"lang_id":LANG,"view_id":ORIGIN_VIEW_ID})
DEXSAMPA = XSampa().convert

@Pyro4.expose
class DocumentServer(object):

    def get_docs(self):
        results = []
        for doc in DATA_EDIFICE.get_docs():
            doc["view_name"] = VIEW_NAME
            doc["view_id"] = VIEW_ID
            doc["view_priority"] = VIEW_PRIORITY
            doc["token_align"] = TOKEN_ALIGN
            results.append(doc)
        return results
        
    def get_tokens(self, doc_id):
        filename = os.path.join(DIR, "%s.ltf.xml" % doc_id)
        
        results = []
        for token in DATA_EDIFICE.get_tokens(doc_id):
            token["text"] = DEXSAMPA(token["text"])
            token["view_id"] = VIEW_ID
            results.append(token)
        return tokens


if __name__ == '__main__':
    argparser = argparse.ArgumentParser()
    argparser.add_argument("lang", help="Source language of the files.  If the resource is, say, Turkish-English parallel text, use 'tur' for both")
    args = argparser.parse_args()
    
    LANG = args.lang
        
    server_metadata = set(["dataserver", "lang.%s" % LANG, "view.%s" % VIEW_ID])
    with ServerWrapper(DocumentServer, server_metadata) as s:
        s.serve()


    