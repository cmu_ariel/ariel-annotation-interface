#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function
from __future__ import unicode_literals
from io import open
import argparse, math, glob, os, json, sys, fnmatch, time
from collections import Counter, defaultdict
import lxml.etree as ET
from progressbar import ProgressBar, Bar, AdaptiveETA, Percentage
#from collections import defaultdict
#import pystache
#from utils.load_resources import TextResourceDB
#from bson import json_util 
from gevent.wsgi import WSGIServer
#from functools import wraps
from pymongo import MongoClient
from ipaline import *


from flask import Flask, request, Response
app = Flask(__name__)
app.debug = True

if app.debug:
    print("Enabling bug mode")
    from werkzeug.debug import DebuggedApplication
    app.wsgi_app = DebuggedApplication(app.wsgi_app, True)
    
def compare(w1, w2):
    comparer = Aline(w1,w2)
    return comparer.get_normalized_similarity()

def init_db(mongodb_ip, mongodb_port, db_name):
    global client, db, resourceInfoDB, textitems, annotations, tasks
    client = MongoClient(mongodb_ip, mongodb_port)
    db = client[db_name]
    
    
def load_entities(filename, limit=0):
    db.entities.drop()
    with open(filename,"r", encoding="utf-8") as fin:
        entities = json.load(fin)
    if limit:
        return db.entities.insert_many(entities[:limit]).inserted_ids
    else:
        return db.entities.insert_many(entities).inserted_ids
    
    
def search(needle, country=""):
    needle = needle.lower()
    needleAline = AlineString(needle)
    results = Counter()
    if country:
        entities = db.entities.find({"country":"US"})
    else:
        entities = db.entities.find({})     
        
    print("Total entities = %s" % entities.count())
    bar = ProgressBar(maxval=100000, poll=1, widgets=[
                Bar('=', '[', ']'), ' ',
                Percentage(), ' ',
                AdaptiveETA()])
                   
    for entity in bar(entities[:100000]):
        w2 = entity["name"].lower()
        if w2 in results:
            continue
        w2Aline = AlineString(w2)
        if len(needle) <= len(w2):
            w1Aline = needleAline
        else:
            w1Aline, w2Aline = w2Aline, needleAline
        similarity = compare(w1Aline, w2Aline)
        results[w2] = similarity
    return results
    

if __name__ == '__main__':
    argparser = argparse.ArgumentParser()
    argparser.add_argument("--port", type=int, default=5005, help="Port for serving the files.")
    argparser.add_argument("--db_ip", default="127.0.0.1", help="database IP")
    argparser.add_argument("--db_port", type=int, default=27020, help="database port")
    argparser.add_argument("--db_name", default="lorelei_kbsearch", help="database name")
    argparser.add_argument("--reload", default="", help="Reload the database from specified JSON file")
    args = argparser.parse_args()
    
    init_db(args.db_ip, args.db_port, args.db_name)
    
    if (args.reload):
        print("Reloading entities")
        start = time.time()
        load_entities(args.reload)
        end = time.time()
        print("Entities loaded, elapsed time = %ss" % (end-start))
        
    print("Iterating")
    start = time.time()
    result = search("Miami", "US")
    end = time.time()
    print("Elapsed time = %ss" % (end-start))
    
    print("Getting most common")
    start = time.time()
    print(result.most_common(10))
    end = time.time()
    print("Elapsed time = %ss" % (end-start))
        
    
    #http_server = WSGIServer(('', args.port), app)
    #http_server.serve_forever()
    #app.run(host='0.0.0.0', port=int(args.port))

    
    
    