#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys, copy
reload(sys)
sys.setdefaultencoding('utf-8')
#import psyco    #DC added
#psyco.full()    #DC added

from util import *
from types import *
from math import log



class AlineString(object):
        def __init__(self, input_string):
            self.input_string = input_string
            self.feature_vector = self._translate_string(input_string)

        def _translate_string(self, input_string):
            output_vector = []

##DC replaces one line. 
            for i in range(len(input_string)):
#                if input_string[i] >= 'a' and input_string[i] <= 'z': #DC removes
                if aline_letters.has_key(input_string[i]):             #DC adds
                    output_vector.append(copy.copy(aline_letters[input_string[i]]))
                elif aline_modifiers.has_key(input_string[i]):
                    output_vector[len(output_vector) - 1][aline_modifiers[input_string[i]]['feature']] = aline_modifiers[input_string[i]]['value']
                if aline_letters.has_key(input_string[i]) or aline_modifiers.has_key(input_string[i]):  #DC adds
		    output_vector[len(output_vector) - 1]["input_string"] = output_vector[len(output_vector) - 1].get("input_string", "") + input_string[i]
		#else: print "Ignoring unknown character: ", input_string[i]	#DC added
            return output_vector

        def get_feature_vector(self):
            return self.feature_vector
	
	def __len__(self):
		return len(self.feature_vector)
	
	def __getitem__(self, index):
            if index >= 1:
		return self.feature_vector[index - 1]
            else:
                return {'null': 1, 'input_string': 'NaS'}
	
	def __repr__(self):
		string = ""
		for i in self.feature_vector:
			string += i['input_string']
		return string
        
        def __iter__(self):
            for i in self.feature_vector:
                yield i


class Aline(object):


	r_c = ['syllabic', 'manner', 'place', 'voiced', 'aspirated', 'nasalized', 'lateralized', 'palatalized',  'velarized', 'pharyngealized', 'rounded', 'peripheral', 'glottalized']
	r_v = ['aperture', 'position', 'syllabic', 'lateralized', 'nasalized', 'palatalized', 'retroflexed', 'creaky', 'breathy', 'rounded', 'voiced', 'long', 'glottalized']


	c_skip = -1000
	c_sub = 3500
	c_exp = 4500
	c_vwl = 1000
	epsilon = 0

	feature_salience = {'manner': 50, 
			    'place': 40, 
			    'voiced': 10, 
			    'aspirated': 5,
			    'syllabic': 10, 
			    'nasalized': 10,   #which
			    'lateralized': 5, 
			    'palatalized': 5, 
			    'velarized': 5, 
			    'pharyngealized': 5, 
			    'retroflexed': 5, 
			    'rounded': 5, 
			    'creaky': 10, 
			    'breathy': 10, 
			    'long': 5,
			    'glottalized': 10,
			    'centralized': 5,
			    'aperture': 20,
			    'position': 30,
			    'peripheral': 0, 
			    }


	def __init__(self, x, y, pretty_print = True):
		self.pretty_print = pretty_print
		self.found = False
		self.x = x
		self.y = y
		self.S = None
	
	def sigma_skip(self, p, output = False):
		self.output_func(output, "c_skip: %s" % (self.c_skip, ))
		return self.c_skip
	
	def sigma_sub(self, p, q, output = False):
		self.output_func(output, "c_sub: %s" % (self.c_sub, ))
		s_d = self.delta(p, q, output)
		s_v_p = self.vowel_cost(p, output)
		s_v_q = self.vowel_cost(q, output)
		self.output_func(output, "c_sub: %s s_d: %s s_v_p: %s s_v_q: %s total cost: %d" % (self.c_sub, s_d, s_v_p, s_v_q, self.c_sub - s_d - s_v_p - s_v_q))
		return int(self.c_sub - s_d - s_v_p - s_v_q)
	
	def sigma_exp(self, p, q1, q2, output = False):
		if q1.has_key('null') or q2.has_key('null'):
			return -Infinity
		s_d_p_q1 = self.delta(p, q1)
		s_d_p_q2 = self.delta(p, q2)
		if s_d_p_q1 == 0 or s_d_p_q2 == 0:
			return -Infinity
		return int(self.c_exp - s_d_p_q1 - s_d_p_q2 - self.vowel_cost(p) - max(self.vowel_cost(q1), self.vowel_cost(q2)))
	
	def vowel_cost(self, p, output = False):
		if p.get('vowel', 0) == 1:
			self.output_func(output, "c_vwl: %s" % (self.c_vwl, ))
			return self.c_vwl
		else:
			self.output_func(output, "c_vwl: 0")
			return 0

	def delta(self, p, q, output = False):
		if (p.has_key('vowel') and p['vowel'] == 1) and (q.has_key('vowel') and q['vowel'] == 1):
			self.output_func(output, "delta features: r_v")
			features = self.r_v
		else:
			self.output_func(output, "delta features: r_c")
			features = self.r_c
		out = 0
		for feature in features:
			out += self.diff(p, q, feature, output) * self.feature_salience[feature]
		return out

	def diff(self, p, q, feature, output = False):
		self.output_func(output, "feature: %s p[feature]: %s q[feature]: %s feature_salience: %s" % (feature, p.get(feature, 0), q.get(feature, 0), self.feature_salience[feature]))
		temp = sys.maxint
		p_val = p.get(feature, 0)
		q_val = q.get(feature, 0)
		if type(p_val) is not ListType and type(p_val) is not TupleType:
			p_vals = (p_val, )
		else:
			p_vals = p_val
		if type(q_val) is not ListType and type(q_val) is not TupleType:
			q_vals = (q_val, )
		else:
			q_vals = q_val
		for i in p_vals:
			for j in q_vals:
				if temp > abs(i - j):
					self.output_func(output, "feature %s i: %s j: %s abs(i - j): %s" % (feature, i, j, abs(i - j)))
					temp = abs(i - j)
		self.output_func(output, "feature: %s final temp: %s" % (feature, temp))
		return temp

	def output_func(self, output, string):
		if output:
			print string
			
	
	def compute_similarity(self):
		if self.S is not None:
			return
		self.S = AlineMatrix(self.x, self.y)
		for i in xrange(1, len(self.x) + 1):
			for j in xrange(1, len(self.y) + 1):
				self.S[i][j] = max(self.S[i-1][j] + self.sigma_skip(self.x[i]),
						   self.S[i][j-1] + self.sigma_skip(self.y[j]),
						   self.S[i-1][j-1] + self.sigma_sub(self.x[i], self.y[j]),
						   self.S[i-1][j-2] + self.sigma_exp(self.x[i], self.y[j-1],self.y[j]),
						   self.S[i-2][j-1] + self.sigma_exp(self.y[j], self.x[i-1], self.x[i]),
						   0)  #above 2 are "squash"


		self.T = (1-self.epsilon) * max([ max(w) for w in self.S ])
		self.out = []
		self.out_score = (max([ max(row) for row in self.S ]) / 100)
		return

	def get_similarity(self):
		if self.S is None:
			self.compute_similarity()
		return self.out_score

	def compute_self_similarity(self):
		if self.x.input_string != self.y.input_string:
			self.self_similarity = -Infinity;
			return
		sum = 0
		for i in xrange(1, len(self.x) + 1):
			sum += self.sigma_sub(self.x[i], self.y[i])
		self.self_similarity = sum / 100
		return

	def get_self_similarity(self):
		# Only meaningful for self comparisons
		self.compute_self_similarity()
		return self.self_similarity

	def get_normalized_similarity(self):
		AlineX = Aline(self.x, self.x)
		AlineY = Aline(self.y, self.y)
		s = self.get_similarity()
		s1 = AlineX.get_self_similarity()
		s2 = AlineY.get_self_similarity()
		return float((2*s))/float((s1+s2))

	def get_distance(self):
		return 1 - self.get_normalized_similarity()

	def get_geographic_distance(self):
		return -1 * log(self.get_normalized_similarity())

	def print_similarity(self):
		self.retrieve_alignment()
		print self.S
		self._print_alignment(self.out)
		print "Similarity score: %s" % (self.get_similarity(), )
		print "Normalized similarity: %s" % (self.get_normalized_similarity(), )
		print
#		print "Distance: %s"  % (self.get_distance(), )
#		print "Geographic Distance: %s"  % (self.get_geographic_distance(), )
	def retrieve_alignment(self):
		if self.S is None:
			self.compute_similarity()
		for i in range(1, self.S.getX()):
			for j in range(1, self.S.getY()):
				if(self.S[i][j] >= self.T):
					self.retrieve(self.x, self.y, i, j,0)

	def get_alignment(self):
		self.retrieve_alignment()
		return self._format_alignment(self.out)

	def retrieve(self, x, y, i, j, s, out = []):
		output = False
		if self.found == True:
			return
		if(self.S[i][j] == 0):
			self.out = copy.deepcopy(out)
			self.found = True
		else:
			if (i == 0) or (self.S[i-1][j-1] + self.sigma_sub(x[i], y[j], output) + s >= self.T):
				out.append([x[i]['input_string'], y[j]['input_string']])
				self.retrieve(x, y, i-1, j-1, s + self.sigma_sub(x[i], y[j]), out)
				out.pop()
			if self.S[i][j-1] + self.sigma_skip(y[j], output) + s >= self.T:
				out.append(["-", y[j]['input_string']])
				self.retrieve(x, y, i, j-1, s + self.sigma_skip(y[j]), out)
				out.pop()
			if self.S[i-1][j-2] + self.sigma_exp(x[i], y[j-1], y[j]) + s >= self.T:
				out.append([x[i]['input_string'] + " <", y[j-1]['input_string'] + " " + y[j]['input_string']])
				self.retrieve(x, y, i-1, j-2, self.sigma_exp(x[i], y[j-1], y[j]) + s, out)
				out.pop()
			if (j == 0) or (self.S[i-1][j] + self.sigma_skip(x[i], output) + s >= self.T):
				out.append([x[i]['input_string'], "-"])
				self.retrieve(x, y, i-1, j, s + self.sigma_skip(x[i]), out)
				out.pop()
			if self.S[i-2][j-1] + self.sigma_exp(y[j], x[i-1], x[i]) + s >= self.T:
				out.append([x[i-1]['input_string'] + " " + x[i]['input_string'], y[j]['input_string'] + " <"])
				self.retrieve(x, y, i-2, j-1, s + self.sigma_exp(y[j], x[i-1], x[i]), out)
				out.pop()
				
	def _format_alignment(self, alignment):
		alignment.reverse()
		out_string_1 = "|"   #DC changed from "" to "|"
		out_string_2 = "|"
		for align in alignment:   #DC changed " " to "|"  three times, added one more
			out_string_1 += align[0] 
			if len(align[1]) > len(align[0]) and self.pretty_print:
				out_string_1 += " " * (abs(len(align[1]) - len(align[0])) + 1)
				out_string_1 += "|"  #DC added
			else:
				out_string_1 += "|"
			out_string_2 += align[1] 
			if len(align[0]) > len(align[1]) and self.pretty_print:
				out_string_2 += "| " * (abs(len(align[1]) - len(align[0])) + 1)
			else:
				out_string_2 += "|"
		return (out_string_1, out_string_2)
	
	def _print_alignment(self, alignment):
		(out_string_1, out_string_2) =  self._format_alignment(alignment)
		print "S1:", out_string_1    #DC added label "S1:"
		print "S2:", out_string_2    #DC added label "S2:"

if __name__ == '__main__':
	from optparse import OptionParser
	parser = OptionParser()
	parser.add_option("-f", "--file", dest="filename")
	parser.add_option("-a", "--asjp", action="store_true", dest="asjp")
	(options, args) = parser.parse_args()
	if options.asjp:
		string_class = ASJPString
	else:
		string_class = AlineString
	strings = []
	if options.filename:
		f = open(options.filename, "r")
		for line in f:
			if line.rstrip() != "":
				parts = line.rstrip().split('\t')
				aDC = parts[0].decode('utf-8')		#DC adds
				bDC = parts[1].decode('utf-8')
				strings.append((aDC, bDC))
				#strings.append((parts[0], parts[1]))	#DC removes
				
	else:
		aDC = args[0].decode('utf-8')		#DC adds
		bDC = args[1].decode('utf-8')
		strings.append((aDC, bDC))
		#strings.append((args[0], args[1]))	#DC removes

	for string_tuple in strings:
		x = string_class(string_tuple[0])
		y = string_class(string_tuple[1])
		a = Aline(x, y)
		print "Input: %s %s" % (string_tuple[0], string_tuple[1])  #DC adds label "Input: "
		a.print_similarity()


##########################IPAutil.py
#encoding: utf-8
#TODO add trill and central for ASJP!?!?
import re
Infinity = float('inf')




multivalued_features = {'place': {'labio-velar': 100,
				  'bilabial': 95,
				  'labio-dental': 90,
				  'dental': 85,
				  'alveolar': 80,
				  'retroflex': 75,
				  'post-alveolar': 70,
				  'alveolo-palatal': 65,
				  'palatal': 60,
				  'palato-velar': 50,
				  'velar': 40,
				  'uvular': 35,
				  'pharyngeal': 20,
				  'epiglottal': 10,
				  'glottal': 0
                                  },
			'manner': {'nasal': 100,
				   'implosive': 90,
				   'plosive': 85,
				   'affricate': 80,
				   'fricative': 70,
				   'tap-flap': 60,
                                   'trill': 55,
				   'approximant': 50,
                                   'vowel': 50,
				   'close': 40,
				   'near-close': 37,
				   'close-mid': 33,
				   'mid': 30,
				   'open-mid': 27,
				   'near-open': 23,
				   'open': 20
                                   },
			'aperture': {'close': 40,
				 'near-close': 37,
				 'close-mid': 33,
				 'mid': 30,
				 'open-mid': 27,
				 'near-open': 23,
				 'open': 20
                                 },
			'position': {'front': 60,
				 'near-front': 55,
				 'central': 50,
				 'near-back': 45,
				 'back': 40
                                 }
		}


aline_modifiers = { 
		u'\u02B0': {'feature': 'aspirated', 'value': 10},
		u'\u0324': {'feature': 'breathy', 'value': 10},
		u'\u0330': {'feature': 'creaky', 'value': 10},
		u'\u0303': {'feature': 'nasalized', 'value': 10},
		u'\u0308': {'feature': 'centralized', 'value': 0},
		u'\u0329': {'feature': 'syllabic', 'value': 10},
		u'\u032F': {'feature': 'syllabic', 'value': 0},  #i.e. non-syllabbic
		u'\u0325': {'feature': 'voiced', 'value': 0},      #i.e. not voiced, ring below
		u'\u030A': {'feature': 'voiced', 'value': 0},      #i.e. not voiced, ring above
		u'\u02B0': {'feature': 'aspirated', 'value': 10},
		u'\u02B7': {'feature': 'rounded', 'value': 10},
		u'\u02B2': {'feature': 'palatalized', 'value': 10},
		u'\u02E0': {'feature': 'velarized', 'value': 10},
		u'\u02E4': {'feature': 'pharyngealized', 'value': 10},
		u'\u02E1': {'feature': 'lateralized', 'value': 10},
		u'\u207F': {'feature': 'nasalized', 'value': 10},
		u'\u02D0': {'feature': 'long', 'value': 10},
		u'\u0241': {'feature': 'glottalized', 'value': 10},  #note capital glottal
		'?': {'feature': 'glottalized', 'value': 10},  #note capital glottal
		':': {'feature': 'long', 'value': 10}
}


aline_letters = { 
		u'ɯ': {'place': multivalued_features['place']['velar'],
			'manner': multivalued_features['manner']['vowel'],
			'aperture': multivalued_features['aperture']['close'],
			'position': multivalued_features['position']['back'],
			'vowel':1,
			'voiced':10,
			'rounded': 0,
			'syllabic':10,
			'retroflexed':0,
			'nasalized':0,
			'creaky':0,
			'breathy':0,
			'lateralized':0,
			'peripheral':0,
			'glottalized':0,
			'long':0},  
		u'u': {'place': multivalued_features['place']['velar'],
			'manner': multivalued_features['manner']['vowel'],
			'aperture': multivalued_features['aperture']['close'],
			'position': multivalued_features['position']['back'],
			'vowel':1,
			'voiced':10,
			'rounded': 10,
			'syllabic':10,
			'retroflexed':0,
			'nasalized':0,
			'creaky':0,
			'breathy':0,
			'lateralized':0,
			'peripheral':0,
			'glottalized':0,
			'long':0},  
		u'ɤ': {'place': multivalued_features['place']['velar'],
			'manner': multivalued_features['manner']['vowel'],
			'aperture': multivalued_features['aperture']['close-mid'],
			'position': multivalued_features['position']['back'],
			'vowel':1,
			'voiced':10,
			'rounded': 0,
			'syllabic':10,
			'retroflexed':0,
			'nasalized':0,
			'creaky':0,
			'breathy':0,
			'lateralized':0,
			'peripheral':0,
			'glottalized':0,
			'long':0},  
		u'ɑ': {'place': multivalued_features['place']['velar'],
			'manner': multivalued_features['manner']['vowel'],
			'aperture': multivalued_features['aperture']['open'],
			'position': multivalued_features['position']['back'],
			'vowel':1,
			'voiced':10,
			'rounded': 0,
			'syllabic':10,
			'retroflexed':0,
			'nasalized':0,
			'creaky':0,
			'breathy':0,
			'lateralized':0,
			'peripheral':0,
			'glottalized':0,
			'long':0},  
		u'ɒ': {'place': multivalued_features['place']['velar'],
			'manner': multivalued_features['manner']['vowel'],
			'aperture': multivalued_features['aperture']['open'],
			'position': multivalued_features['position']['back'],
			'vowel':1,
			'voiced':10,
			'rounded': 10,
			'syllabic':10,
			'retroflexed':0,
			'nasalized':0,
			'creaky':0,
			'breathy':0,
			'lateralized':0,
			'peripheral':0,
			'glottalized':0,
			'long':0},  
		u'o': {'place': multivalued_features['place']['velar'],
			'manner': multivalued_features['manner']['vowel'],
			'aperture': multivalued_features['aperture']['close-mid'],
			'position': multivalued_features['position']['back'],
			'vowel':1,
			'voiced':10,
			'rounded': 10,
			'syllabic':10,
			'retroflexed':0,
			'nasalized':0,
			'creaky':0,
			'breathy':0,
			'lateralized':0,
			'peripheral':0,
			'glottalized':0,
			'long':0},  
		u'ɔ': {'place': multivalued_features['place']['velar'],
			'manner': multivalued_features['manner']['vowel'],
			'aperture': multivalued_features['aperture']['open-mid'],
			'position': multivalued_features['position']['back'],
			'vowel':1,
			'voiced':10,
			'rounded': 10,
			'syllabic':10,
			'retroflexed':0,
			'nasalized':0,
			'creaky':0,
			'breathy':0,
			'lateralized':0,
			'peripheral':0,
			'glottalized':0,
			'long':0},  
		u'ʌ': {'place': multivalued_features['place']['velar'],
			'manner': multivalued_features['manner']['vowel'],
			'aperture': multivalued_features['aperture']['open-mid'],
			'position': multivalued_features['position']['back'],
			'vowel':1,
			'voiced':10,
			'rounded': 0,
			'syllabic':10,
			'retroflexed':0,
			'nasalized':0,
			'creaky':0,
			'breathy':0,
			'lateralized':0,
			'peripheral':0,
			'glottalized':0,
			'long':0},  
		u'ɨ': {'place': multivalued_features['place']['palato-velar'],
			'manner': multivalued_features['manner']['vowel'],
			'aperture': multivalued_features['aperture']['close'],
			'position': multivalued_features['position']['central'],
			'vowel':1,
			'voiced':10,
			'rounded': 0,
			'syllabic':10,
			'retroflexed':0,
			'nasalized':0,
			'creaky':0,
			'breathy':0,
			'lateralized':0,
			'peripheral':0,
			'glottalized':0,
			'long':0},  
		u'ʉ': {'place': multivalued_features['place']['palato-velar'],
			'manner': multivalued_features['manner']['vowel'],
			'aperture': multivalued_features['aperture']['close'],
			'position': multivalued_features['position']['central'],
			'vowel':1,
			'voiced':10,
			'rounded': 10,
			'syllabic':10,
			'retroflexed':0,
			'nasalized':0,
			'creaky':0,
			'breathy':0,
			'lateralized':0,
			'peripheral':0,
			'glottalized':0,
			'long':0},  
		u'ɘ': {'place': multivalued_features['place']['palato-velar'],
			'manner': multivalued_features['manner']['vowel'],
			'aperture': multivalued_features['aperture']['close-mid'],
			'position': multivalued_features['position']['central'],
			'vowel':1,
			'voiced':10,
			'rounded': 0,
			'syllabic':10,
			'retroflexed':0,
			'nasalized':0,
			'creaky':0,
			'breathy':0,
			'lateralized':0,
			'peripheral':0,
			'glottalized':0,
			'long':0},  
		u'ə': {'place': multivalued_features['place']['palato-velar'],
			'manner': multivalued_features['manner']['vowel'],
			'aperture': multivalued_features['aperture']['mid'],
			'position': multivalued_features['position']['central'],
			'vowel':1,
			'voiced':10,
			'rounded': 0,
			'syllabic':10,
			'retroflexed':0,
			'nasalized':0,
			'creaky':0,
			'breathy':0,
			'lateralized':0,
			'peripheral':0,
			'glottalized':0,
			'long':0},  
		u'ɐ': {'place': multivalued_features['place']['palato-velar'],
			'manner': multivalued_features['manner']['vowel'],
			'aperture': multivalued_features['aperture']['near-open'],
			'position': multivalued_features['position']['central'],
			'vowel':1,
			'voiced':10,
			'rounded': 0,
			'syllabic':10,
			'retroflexed':0,
			'nasalized':0,
			'creaky':0,
			'breathy':0,
			'lateralized':0,
			'peripheral':0,
			'glottalized':0,
			'long':0},  
		u'ᴀ': {'place': multivalued_features['place']['palato-velar'],
			'manner': multivalued_features['manner']['vowel'],
			'aperture': multivalued_features['aperture']['open'],
			'position': multivalued_features['position']['central'],
			'vowel':1,
			'voiced':10,
			'rounded': 0,
			'syllabic':10,
			'retroflexed':0,
			'nasalized':0,
			'creaky':0,
			'breathy':0,
			'lateralized':0,
			'peripheral':0,
			'glottalized':0,
			'long':0},  
		u'ɜ': {'place': multivalued_features['place']['palato-velar'],
			'manner': multivalued_features['manner']['vowel'],
			'aperture': multivalued_features['aperture']['open-mid'],
			'position': multivalued_features['position']['central'],
			'vowel':1,
			'voiced':10,
			'rounded': 0,
			'syllabic':10,
			'retroflexed':0,
			'nasalized':0,
			'creaky':0,
			'breathy':0,
			'lateralized':0,
			'peripheral':0,
			'glottalized':0,
			'long':0},  
		u'ɞ': {'place': multivalued_features['place']['palato-velar'],
			'manner': multivalued_features['manner']['vowel'],
			'aperture': multivalued_features['aperture']['open-mid'],
			'position': multivalued_features['position']['central'],
			'vowel':1,
			'voiced':10,
			'rounded': 10,
			'syllabic':10,
			'retroflexed':0,
			'nasalized':0,
			'creaky':0,
			'breathy':0,
			'lateralized':0,
			'peripheral':0,
			'glottalized':0,
			'long':0},  
		u'ɵ': {'place': multivalued_features['place']['palato-velar'],
			'manner': multivalued_features['manner']['vowel'],
			'aperture': multivalued_features['aperture']['open-mid'],
			'position': multivalued_features['position']['central'],
			'vowel':1,
			'voiced':10,
			'rounded': 10,
			'syllabic':10,
			'retroflexed':0,
			'nasalized':0,
			'creaky':0,
			'breathy':0,
			'lateralized':0,
			'peripheral':0,
			'glottalized':0,
			'long':0},  
		u'i': {'place': multivalued_features['place']['palatal'],
			'manner': multivalued_features['manner']['vowel'],
			'aperture': multivalued_features['aperture']['close'],
			'position': multivalued_features['position']['front'],
			'vowel':1,
			'voiced':10,
			'rounded': 0,
			'syllabic':10,
			'retroflexed':0,
			'nasalized':0,
			'creaky':0,
			'breathy':0,
			'lateralized':0,
			'peripheral':0,
			'glottalized':0,
			'long':0},  
		u'y': {'place': multivalued_features['place']['palatal'],
			'manner': multivalued_features['manner']['vowel'],
			'aperture': multivalued_features['aperture']['close'],
			'position': multivalued_features['position']['front'],
			'vowel':1,
			'voiced':10,
			'rounded': 10,
			'syllabic':10,
			'retroflexed':0,
			'nasalized':0,
			'creaky':0,
			'breathy':0,
			'lateralized':0,
			'peripheral':0,
			'glottalized':0,
			'long':0},  
		u'e': {'place': multivalued_features['place']['palatal'],
			'manner': multivalued_features['manner']['vowel'],
			'aperture': multivalued_features['aperture']['close-mid'],
			'position': multivalued_features['position']['front'],
			'vowel':1,
			'voiced':10,
			'rounded': 0,
			'syllabic':10,
			'retroflexed':0,
			'nasalized':0,
			'creaky':0,
			'breathy':0,
			'lateralized':0,
			'peripheral':0,
			'glottalized':0,
			'long':0},  
		u'ᴇ': {'place': multivalued_features['place']['palatal'],
			'manner': multivalued_features['manner']['vowel'],
			'aperture': multivalued_features['aperture']['mid'],
			'position': multivalued_features['position']['front'],
			'vowel':1,
			'voiced':10,
			'rounded': 0,
			'syllabic':10,
			'retroflexed':0,
			'nasalized':0,
			'creaky':0,
			'breathy':0,
			'lateralized':0,
			'peripheral':0,
			'glottalized':0,
			'long':0},  
		u'æ': {'place': multivalued_features['place']['palatal'],
			'manner': multivalued_features['manner']['vowel'],
			'aperture': multivalued_features['aperture']['near-open'],
			'position': multivalued_features['position']['front'],
			'vowel':1,
			'voiced':10,
			'rounded': 0,
			'syllabic':10,
			'retroflexed':0,
			'nasalized':0,
			'creaky':0,
			'breathy':0,
			'lateralized':0,
			'peripheral':0,
			'glottalized':0,
			'long':0},  
		u'a': {'place': multivalued_features['place']['palato-velar'],
			'manner': multivalued_features['manner']['vowel'],
			'aperture': multivalued_features['aperture']['open'],
			'position': multivalued_features['position']['central'],
			'vowel':1,
			'voiced':10,
			'rounded': 0,
			'syllabic':10,
			'retroflexed':0,
			'nasalized':0,
			'creaky':0,
			'breathy':0,
			'lateralized':0,
			'peripheral':0,
			'glottalized':0,
			'long':0},  
		u'œ': {'place': multivalued_features['place']['palatal'],
			'manner': multivalued_features['manner']['vowel'],
			'aperture': multivalued_features['aperture']['open'],
			'position': multivalued_features['position']['near-front'],
			'vowel':1,
			'voiced':10,
			'rounded': 10,
			'syllabic':10,
			'retroflexed':0,
			'nasalized':0,
			'creaky':0,
			'breathy':0,
			'lateralized':0,
			'peripheral':0,
			'glottalized':0,
			'long':0},  
		u'ɛ': {'place': multivalued_features['place']['palatal'],
			'manner': multivalued_features['manner']['vowel'],
			'aperture': multivalued_features['aperture']['open-mid'],
			'position': multivalued_features['position']['front'],
			'vowel':1,
			'voiced':10,
			'rounded': 0,
			'syllabic':10,
			'retroflexed':0,
			'nasalized':0,
			'creaky':0,
			'breathy':0,
			'lateralized':0,
			'peripheral':0,
			'glottalized':0,
			'long':0},  
		u'ø': {'place': multivalued_features['place']['palatal'],
			'manner': multivalued_features['manner']['vowel'],
			'aperture': multivalued_features['aperture']['open-mid'],
			'position': multivalued_features['position']['near-front'],
			'vowel':1,
			'voiced':10,
			'rounded': 10,
			'syllabic':10,
			'retroflexed':0,
			'nasalized':0,
			'creaky':0,
			'breathy':0,
			'lateralized':0,
			'peripheral':0,
			'glottalized':0,
			'long':0},  
		u'ɶ': {'place': multivalued_features['place']['palatal'],
			'manner': multivalued_features['manner']['vowel'],
			'aperture': multivalued_features['aperture']['open-mid'],
			'position': multivalued_features['position']['near-front'],
			'vowel':1,
			'voiced':10,
			'rounded': 10,
			'syllabic':10,
			'retroflexed':0,
			'nasalized':0,
			'creaky':0,
			'breathy':0,
			'lateralized':0,
			'peripheral':0,
			'glottalized':0,
			'long':0},  
		u'ᴜ': {'place': multivalued_features['place']['velar'],
			'manner': multivalued_features['manner']['vowel'],
			'aperture': multivalued_features['aperture']['near-close'],
			'position': multivalued_features['position']['near-back'],
			'vowel':1,
			'voiced':10,
			'rounded': 10,
			'syllabic':10,
			'retroflexed':0,
			'nasalized':0,
			'creaky':0,
			'breathy':0,
			'lateralized':0,
			'peripheral':0,
			'glottalized':0,
			'long':0},  
		u'ʊ': {'place': multivalued_features['place']['velar'],
			'manner': multivalued_features['manner']['vowel'],
			'aperture': multivalued_features['aperture']['near-close'],
			'position': multivalued_features['position']['near-back'],
			'vowel':1,
			'voiced':10,
			'rounded': 10,
			'syllabic':10,
			'retroflexed':0,
			'nasalized':0,
			'creaky':0,
			'breathy':0,
			'lateralized':0,
			'peripheral':0,
			'glottalized':0,
			'long':0},  
		u'ɿ': {'place': multivalued_features['place']['palatal'],
			'manner': multivalued_features['manner']['vowel'],
			'aperture': multivalued_features['aperture']['close'],
			'position': multivalued_features['position']['near-front'],
			'vowel':1,
			'voiced':10,
			'rounded': 0,
			'syllabic':10,
			'retroflexed':0,
			'nasalized':0,
			'creaky':0,
			'breathy':0,
			'lateralized':0,
			'peripheral':0,
			'glottalized':0,
			'long':0},  
		u'ʅ': {'place': multivalued_features['place']['palatal'],
			'manner': multivalued_features['manner']['vowel'],
			'aperture': multivalued_features['aperture']['close'],
			'position': multivalued_features['position']['near-front'],
			'vowel':1,
			'voiced':10,
			'rounded': 0,
			'syllabic':10,
			'retroflexed':10,
			'nasalized':0,
			'creaky':0,
			'breathy':0,
			'lateralized':0,
			'peripheral':0,
			'glottalized':0,
			'long':0},  
		u'ɪ': {'place': multivalued_features['place']['palatal'],
			'manner': multivalued_features['manner']['vowel'],
			'aperture': multivalued_features['aperture']['near-close'],
			'position': multivalued_features['position']['near-front'],
			'vowel':1,
			'voiced':10,
			'rounded': 0,
			'syllabic':10,
			'retroflexed':0,
			'nasalized':0,
			'creaky':0,
			'breathy':0,
			'lateralized':0,
			'peripheral':0,
			'glottalized':0,
			'long':0},  
		u'ʏ': {'place': multivalued_features['place']['palatal'],
			'manner': multivalued_features['manner']['vowel'],
			'aperture': multivalued_features['aperture']['near-close'],
			'position': multivalued_features['position']['near-front'],
			'vowel':1,
			'voiced':10,
			'rounded': 10,
			'syllabic':10,
			'retroflexed':0,
			'nasalized':0,
			'creaky':0,
			'breathy':0,
			'lateralized':0,
			'peripheral':0,
			'glottalized':0,
			'long':0},  
		u'ɚ': {'place': multivalued_features['place']['palato-velar'],
			'manner': multivalued_features['manner']['vowel'],
			'aperture': multivalued_features['aperture']['mid'],
			'position': multivalued_features['position']['central'],
			'vowel':1,
			'voiced':10,
			'rounded': 0,
			'syllabic':10,
			'retroflexed':10,
			'nasalized':0,
			'creaky':0,
			'breathy':0,
			'lateralized':0,
			'peripheral':0,
			'glottalized':0,
			'long':0},  
		u'b': {'place': multivalued_features['place']['bilabial'],
			'manner': multivalued_features['manner']['plosive'],
			'voiced': 10,
			'syllabic':0,
			'nasalized':0,
			'peripheral':20,
			'rounded': 0,
			'glottalized':0,
			'lateralized':0,
			'aspirated':0},  
		u'ʙ': {'place': multivalued_features['place']['bilabial'],
			'manner': multivalued_features['manner']['trill'],
			'voiced': 10,
			'syllabic':0,
			'nasalized':0,
			'peripheral':20,
			'rounded': 0,
			'glottalized':0,
			'lateralized':0,
			'aspirated':0},  
		u'ɓ': {'place': multivalued_features['place']['bilabial'],
			'manner': multivalued_features['manner']['implosive'],
			'voiced': 10,
			'syllabic':0,
			'nasalized':0,
			'peripheral':20,
			'rounded': 0,
			'glottalized':0,
			'lateralized':0,
			'aspirated':0},  
		u'c': {'place': multivalued_features['place']['palatal'],
			'manner': multivalued_features['manner']['plosive'],
			'voiced': 0,
			'syllabic':0,
			'nasalized':0,
			'peripheral':0,
			'rounded': 0,
			'glottalized':0,
			'lateralized':0,
			'aspirated':0},  
		u'ç': {'place': multivalued_features['place']['palatal'],
			'manner': multivalued_features['manner']['fricative'],
			'voiced': 0,
			'syllabic':0,
			'nasalized':0,
			'peripheral':0,
			'rounded': 0,
			'glottalized':0,
			'lateralized':0,
			'aspirated':0},  
		u'ɕ': {'place': multivalued_features['place']['alveolo-palatal'],
			'manner': multivalued_features['manner']['fricative'],
			'voiced': 0,
			'syllabic':0,
			'nasalized':0,
			'peripheral':0,
			'rounded': 0,
			'glottalized':0,
			'lateralized':0,
			'aspirated':0},  
		u'd': {'place': multivalued_features['place']['alveolar'],
			'manner': multivalued_features['manner']['plosive'],
			'voiced': 10,
			'syllabic':0,
			'nasalized':0,
			'peripheral':0,
			'rounded': 0,
			'glottalized':0,
			'lateralized':0,
			'aspirated':0},  
		u'ɗ': {'place': multivalued_features['place']['alveolar'],
			'manner': multivalued_features['manner']['implosive'],
			'voiced': 10,
			'syllabic':0,
			'nasalized':0,
			'peripheral':0,
			'rounded': 0,
			'glottalized':0,
			'lateralized':0,
			'aspirated':0},  
		u'ð': {'place': multivalued_features['place']['dental'],
			'manner': multivalued_features['manner']['fricative'],
			'voiced': 10,
			'syllabic':0,
			'nasalized':0,
			'peripheral':0,
			'rounded': 0,
			'glottalized':0,
			'lateralized':0,
			'aspirated':0},  
		u'ʤ': {'place': multivalued_features['place']['post-alveolar'],
			'manner': multivalued_features['manner']['affricate'],
			'voiced': 10,
			'syllabic':0,
			'nasalized':0,
			'peripheral':0,
			'rounded': 0,
			'glottalized':0,
			'lateralized':0,
			'aspirated':0},  
		u'ɖ': {'place': multivalued_features['place']['retroflex'],
			'manner': multivalued_features['manner']['plosive'],
			'voiced': 10,
			'syllabic':0,
			'nasalized':0,
			'peripheral':0,
			'rounded': 0,
			'glottalized':0,
			'lateralized':0,
			'aspirated':0},  
		u'ʣ': {'place': multivalued_features['place']['alveolar'],
			'manner': multivalued_features['manner']['affricate'],
			'voiced': 10,
			'syllabic':0,
			'nasalized':0,
			'peripheral':0,
			'rounded': 0,
			'glottalized':0,
			'lateralized':0,
			'aspirated':0},  
		u'ʥ': {'place': multivalued_features['place']['alveolo-palatal'],
			'manner': multivalued_features['manner']['affricate'],
			'voiced': 10,
			'syllabic':0,
			'nasalized':0,
			'peripheral':0,
			'rounded': 0,
			'glottalized':0,
			'lateralized':0,
			'aspirated':0},  
		u'ȡ': {'place': multivalued_features['place']['alveolo-palatal'],
			'manner': multivalued_features['manner']['plosive'],
			'voiced': 10,
			'syllabic':0,
			'nasalized':0,
			'peripheral':0,
			'rounded': 0,
			'glottalized':0,
			'lateralized':0,
			'aspirated':0},  
		u'f': {'place': multivalued_features['place']['labio-dental'],
			'manner': multivalued_features['manner']['fricative'],
			'voiced': 0,
			'syllabic':0,
			'nasalized':0,
			'peripheral':20,
			'rounded': 0,
			'glottalized':0,
			'lateralized':0,
			'aspirated':0},  
		u'ɢ': {'place': multivalued_features['place']['uvular'],
			'manner': multivalued_features['manner']['plosive'],
			'voiced': 10,
			'syllabic':0,
			'nasalized':0,
			'peripheral':20,
			'rounded': 0,
			'glottalized':0,
			'lateralized':0,
			'aspirated':0},  
		u'ʛ': {'place': multivalued_features['place']['uvular'],
			'manner': multivalued_features['manner']['implosive'],
			'voiced': 10,
			'syllabic':0,
			'nasalized':0,
			'peripheral':20,
			'rounded': 0,
			'glottalized':0,
			'lateralized':0,
			'aspirated':0},  
		u'ɠ': {'place': multivalued_features['place']['velar'],
			'manner': multivalued_features['manner']['implosive'],
			'voiced': 10,
			'syllabic':0,
			'nasalized':0,
			'peripheral':20,
			'rounded': 0,
			'glottalized':0,
			'lateralized':0,
			'aspirated':0},  
		u'ɣ': {'place': multivalued_features['place']['velar'],
			'manner': multivalued_features['manner']['fricative'],
			'voiced': 10,
			'syllabic':0,
			'nasalized':0,
			'peripheral':20,
			'rounded': 0,
			'glottalized':0,
			'lateralized':0,
			'aspirated':0},  
		u'ɡ': {'place': multivalued_features['place']['velar'],
			'manner': multivalued_features['manner']['plosive'],
			'voiced': 10,
			'syllabic':0,
			'nasalized':0,
			'peripheral':20,
			'rounded': 0,
			'glottalized':0,
			'lateralized':0,
			'aspirated':0},  
		u'g': {'place': multivalued_features['place']['velar'],
			'manner': multivalued_features['manner']['plosive'],
			'voiced': 10,
			'syllabic':0,
			'nasalized':0,
			'peripheral':20,
			'rounded': 0,
			'glottalized':0,
			'lateralized':0,
			'aspirated':0},  
		u'ʜ': {'place': multivalued_features['place']['epiglottal'],
			'manner': multivalued_features['manner']['fricative'],
			'voiced': 0,
			'syllabic':0,
			'nasalized':0,
			'peripheral':0,
			'rounded': 0,
			'glottalized':0,
			'lateralized':0,
			'aspirated':0},  
		u'h': {'place': multivalued_features['place']['glottal'],
			'manner': multivalued_features['manner']['fricative'],
			'voiced': 0,
			'syllabic':0,
			'nasalized':0,
			'peripheral':0,
			'rounded': 0,
			'glottalized':0,
			'lateralized':0,
			'aspirated':0},  
		u'ɦ': {'place': multivalued_features['place']['glottal'],
			'manner': multivalued_features['manner']['fricative'],
			'voiced': 10,
			'syllabic':0,
			'nasalized':0,
			'peripheral':0,
			'rounded': 0,
			'glottalized':0,
			'lateralized':0,
			'aspirated':0},  
		u'ɥ': {'place': multivalued_features['place']['palatal'],
			'manner': multivalued_features['manner']['approximant'],
			'voiced': 10,
			'syllabic':0,
			'nasalized':0,
			'peripheral':0,
			'rounded': 10,
			'glottalized':0,
			'lateralized':0,
			'aspirated':0},  
		u'ɧ': {'place': multivalued_features['place']['palato-velar'],
			'manner': multivalued_features['manner']['fricative'],
			'voiced': 0,
			'syllabic':0,
			'nasalized':0,
			'peripheral':20,
			'rounded': 0,
			'glottalized':0,
			'lateralized':0,
			'aspirated':0},  
		u'j': {'place': multivalued_features['place']['palatal'],
			'manner': multivalued_features['manner']['approximant'],
			'voiced': 10,
			'syllabic':0,
			'nasalized':0,
			'peripheral':0,
			'rounded': 0,
			'glottalized':0,
			'lateralized':0,
			'aspirated':0},  
		u'ɟ': {'place': multivalued_features['place']['palatal'],
			'manner': multivalued_features['manner']['plosive'],
			'voiced': 10,
			'syllabic':0,
			'nasalized':0,
			'peripheral':0,
			'rounded': 0,
			'glottalized':0,
			'lateralized':0,
			'aspirated':0},  
		u'ʄ': {'place': multivalued_features['place']['palatal'],
			'manner': multivalued_features['manner']['implosive'],
			'voiced': 10,
			'syllabic':0,
			'nasalized':0,
			'peripheral':0,
			'rounded': 0,
			'glottalized':0,
			'lateralized':0,
			'aspirated':0},  
		u'ʝ': {'place': multivalued_features['place']['palatal'],
			'manner': multivalued_features['manner']['fricative'],
			'voiced': 10,
			'syllabic':0,
			'nasalized':0,
			'peripheral':0,
			'rounded': 0,
			'glottalized':0,
			'lateralized':0,
			'aspirated':0},  
		u'k': {'place': multivalued_features['place']['velar'],
			'manner': multivalued_features['manner']['plosive'],
			'voiced': 0,
			'syllabic':0,
			'nasalized':0,
			'peripheral':20,
			'rounded': 0,
			'glottalized':0,
			'lateralized':0,
			'aspirated':0},  
		u'l': {'place': multivalued_features['place']['alveolar'],
			'manner': multivalued_features['manner']['approximant'],
			'voiced': 10,
			'syllabic':0,
			'nasalized':0,
			'peripheral':0,
			'rounded': 0,
			'glottalized':0,
			'lateralized':10,
			'aspirated':0},  
		u'ʟ': {'place': multivalued_features['place']['velar'],
			'manner': multivalued_features['manner']['approximant'],
			'voiced': 10,
			'syllabic':0,
			'nasalized':0,
			'peripheral':20,
			'rounded': 0,
			'glottalized':0,
			'lateralized':10,
			'aspirated':0},  
		u'ɫ': {'place': multivalued_features['place']['alveolar'],
			'manner': multivalued_features['manner']['approximant'],
			'voiced': 10,
			'syllabic':0,
			'nasalized':0,
			'peripheral':0,
			'rounded': 0,
			'glottalized':0,
			'lateralized':10,
			'aspirated':0},  
		u'ɭ': {'place': multivalued_features['place']['retroflex'],
			'manner': multivalued_features['manner']['approximant'],
			'voiced': 10,
			'syllabic':0,
			'nasalized':0,
			'peripheral':0,
			'rounded': 0,
			'glottalized':0,
			'lateralized':10,
			'aspirated':0},  
		u'ɬ': {'place': multivalued_features['place']['alveolar'],
			'manner': multivalued_features['manner']['fricative'],
			'voiced': 0,
			'syllabic':0,
			'nasalized':0,
			'peripheral':0,
			'rounded': 0,
			'glottalized':0,
			'lateralized':10,
			'aspirated':0},  
		u'ɮ': {'place': multivalued_features['place']['alveolar'],
			'manner': multivalued_features['manner']['fricative'],
			'voiced': 10,
			'syllabic':0,
			'nasalized':0,
			'peripheral':0,
			'rounded': 0,
			'glottalized':0,
			'lateralized':10,
			'aspirated':0},  
		u'ȴ': {'place': multivalued_features['place']['alveolo-palatal'],
			'manner': multivalued_features['manner']['approximant'],
			'voiced': 10,
			'syllabic':0,
			'nasalized':0,
			'peripheral':0,
			'rounded': 0,
			'glottalized':0,
			'lateralized':10,
			'aspirated':0},  
		u'm': {'place': multivalued_features['place']['bilabial'],
			'manner': multivalued_features['manner']['nasal'],
			'voiced': 10,
			'syllabic':0,
			'nasalized':0,
			'peripheral':20,
			'rounded': 0,
			'glottalized':0,
			'lateralized':0,
			'aspirated':0},  
		u'ɱ': {'place': multivalued_features['place']['labio-dental'],
			'manner': multivalued_features['manner']['nasal'],
			'voiced': 10,
			'syllabic':0,
			'nasalized':0,
			'peripheral':20,
			'rounded': 0,
			'glottalized':0,
			'lateralized':0,
			'aspirated':0},  
		u'ɰ': {'place': multivalued_features['place']['velar'],
			'manner': multivalued_features['manner']['approximant'],
			'voiced': 10,
			'syllabic':0,
			'nasalized':0,
			'peripheral':20,
			'rounded': 0,
			'glottalized':0,
			'lateralized':0,
			'aspirated':0},  
		u'n': {'place': multivalued_features['place']['alveolar'],
			'manner': multivalued_features['manner']['nasal'],
			'voiced': 10,
			'syllabic':0,
			'nasalized':0,
			'peripheral':0,
			'rounded': 0,
			'glottalized':0,
			'lateralized':0,
			'aspirated':0},  
		u'ɴ': {'place': multivalued_features['place']['uvular'],
			'manner': multivalued_features['manner']['nasal'],
			'voiced': 10,
			'syllabic':0,
			'nasalized':0,
			'peripheral':20,
			'rounded': 0,
			'glottalized':0,
			'lateralized':0,
			'aspirated':0},  
		u'ɲ': {'place': multivalued_features['place']['palatal'],
			'manner': multivalued_features['manner']['nasal'],
			'voiced': 10,
			'syllabic':0,
			'nasalized':0,
			'peripheral':0,
			'rounded': 0,
			'glottalized':0,
			'lateralized':0,
			'aspirated':0},  
		u'ɳ': {'place': multivalued_features['place']['retroflex'],
			'manner': multivalued_features['manner']['nasal'],
			'voiced': 10,
			'syllabic':0,
			'nasalized':0,
			'peripheral':0,
			'rounded': 0,
			'glottalized':0,
			'lateralized':0,
			'aspirated':0},  
		u'ȵ': {'place': multivalued_features['place']['alveolo-palatal'],
			'manner': multivalued_features['manner']['nasal'],
			'voiced': 10,
			'syllabic':0,
			'nasalized':0,
			'peripheral':0,
			'rounded': 0,
			'glottalized':0,
			'lateralized':0,
			'aspirated':0},  
		u'ŋ': {'place': multivalued_features['place']['velar'],
			'manner': multivalued_features['manner']['nasal'],
			'voiced': 10,
			'syllabic':0,
			'nasalized':0,
			'peripheral':20,
			'rounded': 0,
			'glottalized':0,
			'lateralized':0,
			'aspirated':0},  
		u'p': {'place': multivalued_features['place']['bilabial'],
			'manner': multivalued_features['manner']['plosive'],
			'voiced': 0,
			'syllabic':0,
			'nasalized':0,
			'peripheral':20,
			'rounded': 0,
			'glottalized':0,
			'lateralized':0,
			'aspirated':0},  
		u'ɸ': {'place': multivalued_features['place']['bilabial'],
			'manner': multivalued_features['manner']['fricative'],
			'voiced': 0,
			'syllabic':0,
			'nasalized':0,
			'peripheral':20,
			'rounded': 0,
			'glottalized':0,
			'lateralized':0,
			'aspirated':0},  
		u'q': {'place': multivalued_features['place']['uvular'],
			'manner': multivalued_features['manner']['plosive'],
			'voiced': 0,
			'syllabic':0,
			'nasalized':0,
			'peripheral':20,
			'rounded': 0,
			'glottalized':0,
			'lateralized':0,
			'aspirated':0},  
		u'r': {'place': multivalued_features['place']['alveolar'],
			'manner': multivalued_features['manner']['trill'],
			'voiced': 10,
			'syllabic':0,
			'nasalized':0,
			'peripheral':0,
			'rounded': 0,
			'glottalized':0,
			'lateralized':0,
			'aspirated':0},  
		u'ʀ': {'place': multivalued_features['place']['uvular'],
			'manner': multivalued_features['manner']['trill'],
			'voiced': 10,
			'syllabic':0,
			'nasalized':0,
			'peripheral':20,
			'rounded': 0,
			'glottalized':0,
			'lateralized':0,
			'aspirated':0},  
		u'ɽ': {'place': multivalued_features['place']['retroflex'],
			'manner': multivalued_features['manner']['tap-flap'],
			'voiced': 10,
			'syllabic':0,
			'nasalized':0,
			'peripheral':0,
			'rounded': 0,
			'glottalized':0,
			'lateralized':0,
			'aspirated':0},  
		u'ɹ': {'place': multivalued_features['place']['alveolar'],
			'manner': multivalued_features['manner']['approximant'],
			'voiced': 10,
			'syllabic':0,
			'nasalized':0,
			'peripheral':0,
			'rounded': 0,
			'glottalized':0,
			'lateralized':0,
			'aspirated':0},  
		u'ʁ': {'place': multivalued_features['place']['uvular'],
			'manner': multivalued_features['manner']['fricative'],
			'voiced': 10,
			'syllabic':0,
			'nasalized':0,
			'peripheral':20,
			'rounded': 0,
			'glottalized':0,
			'lateralized':0,
			'aspirated':0},  
		u'ɺ': {'place': multivalued_features['place']['alveolar'],
			'manner': multivalued_features['manner']['tap-flap'],
			'voiced': 10,
			'syllabic':0,
			'nasalized':0,
			'peripheral':0,
			'rounded': 0,
			'glottalized':0,
			'lateralized':10,
			'aspirated':0},  
		u'ɻ': {'place': multivalued_features['place']['retroflex'],
			'manner': multivalued_features['manner']['approximant'],
			'voiced': 10,
			'syllabic':0,
			'nasalized':0,
			'peripheral':0,
			'rounded': 0,
			'glottalized':0,
			'lateralized':0,
			'aspirated':0},  
		u'ɾ': {'place': multivalued_features['place']['alveolar'],
			'manner': multivalued_features['manner']['tap-flap'],
			'voiced': 10,
			'syllabic':0,
			'nasalized':0,
			'peripheral':0,
			'rounded': 0,
			'glottalized':0,
			'lateralized':0,
			'aspirated':0},  
		u's': {'place': multivalued_features['place']['alveolar'],
			'manner': multivalued_features['manner']['fricative'],
			'voiced': 0,
			'syllabic':0,
			'nasalized':0,
			'peripheral':0,
			'rounded': 0,
			'glottalized':0,
			'lateralized':0,
			'aspirated':0},  
		u'ʂ': {'place': multivalued_features['place']['retroflex'],
			'manner': multivalued_features['manner']['fricative'],
			'voiced': 0,
			'syllabic':0,
			'nasalized':0,
			'peripheral':0,
			'rounded': 0,
			'glottalized':0,
			'lateralized':0,
			'aspirated':0},  
		u'ʃ': {'place': multivalued_features['place']['post-alveolar'],
			'manner': multivalued_features['manner']['fricative'],
			'voiced': 0,
			'syllabic':0,
			'nasalized':0,
			'peripheral':0,
			'rounded': 0,
			'glottalized':0,
			'lateralized':0,
			'aspirated':0},  
		u't': {'place': multivalued_features['place']['alveolar'],
			'manner': multivalued_features['manner']['plosive'],
			'voiced': 0,
			'syllabic':0,
			'nasalized':0,
			'peripheral':0,
			'rounded': 0,
			'glottalized':0,
			'lateralized':0,
			'aspirated':0},  
		u'ʈ': {'place': multivalued_features['place']['retroflex'],
			'manner': multivalued_features['manner']['plosive'],
			'voiced': 0,
			'syllabic':0,
			'nasalized':0,
			'peripheral':0,
			'rounded': 0,
			'glottalized':0,
			'lateralized':0,
			'aspirated':0},  
		u'ʧ': {'place': multivalued_features['place']['post-alveolar'],
			'manner': multivalued_features['manner']['affricate'],
			'voiced': 0,
			'syllabic':0,
			'nasalized':0,
			'peripheral':0,
			'rounded': 0,
			'glottalized':0,
			'lateralized':0,
			'aspirated':0},  
		u'ʨ': {'place': multivalued_features['place']['alveolo-palatal'],
			'manner': multivalued_features['manner']['affricate'],
			'voiced': 0,
			'syllabic':0,
			'nasalized':0,
			'peripheral':0,
			'rounded': 0,
			'glottalized':0,
			'lateralized':0,
			'aspirated':0},  
		u'ȶ': {'place': multivalued_features['place']['alveolo-palatal'],
			'manner': multivalued_features['manner']['plosive'],
			'voiced': 0,
			'syllabic':0,
			'nasalized':0,
			'peripheral':0,
			'rounded': 0,
			'glottalized':0,
			'lateralized':0,
			'aspirated':0},  
		u'ʦ': {'place': multivalued_features['place']['alveolar'],
			'manner': multivalued_features['manner']['affricate'],
			'voiced': 0,
			'syllabic':0,
			'nasalized':0,
			'peripheral':0,
			'rounded': 0,
			'glottalized':0,
			'lateralized':0,
			'aspirated':0},  
		u'v': {'place': multivalued_features['place']['labio-dental'],
			'manner': multivalued_features['manner']['fricative'],
			'voiced': 10,
			'syllabic':0,
			'nasalized':0,
			'peripheral':20,
			'rounded': 0,
			'glottalized':0,
			'lateralized':0,
			'aspirated':0},  
		u'ʋ': {'place': multivalued_features['place']['labio-dental'],
			'manner': multivalued_features['manner']['approximant'],
			'voiced': 10,
			'syllabic':0,
			'nasalized':0,
			'peripheral':20,
			'rounded': 0,
			'glottalized':0,
			'lateralized':0,
			'aspirated':0},  
		u'w': {'place': multivalued_features['place']['labio-velar'],
			'manner': multivalued_features['manner']['approximant'],
			'voiced': 10,
			'syllabic':0,
			'nasalized':0,
			'peripheral':20,
			'rounded': 0,
			'glottalized':0,
			'lateralized':0,
			'aspirated':0},  
		u'ʍ': {'place': multivalued_features['place']['labio-velar'],
			'manner': multivalued_features['manner']['approximant'],
			'voiced': 0,
			'syllabic':0,
			'nasalized':0,
			'peripheral':20,
			'rounded': 0,
			'glottalized':0,
			'lateralized':0,
			'aspirated':0},  
		u'x': {'place': multivalued_features['place']['velar'],
			'manner': multivalued_features['manner']['fricative'],
			'voiced': 0,
			'syllabic':0,
			'nasalized':0,
			'peripheral':20,
			'rounded': 0,
			'glottalized':0,
			'lateralized':0,
			'aspirated':0},  
		u'ʎ': {'place': multivalued_features['place']['palatal'],
			'manner': multivalued_features['manner']['approximant'],
			'voiced': 10,
			'syllabic':0,
			'nasalized':0,
			'peripheral':0,
			'rounded': 0,
			'glottalized':0,
			'lateralized':10,
			'aspirated':0},  
		u'z': {'place': multivalued_features['place']['alveolar'],
			'manner': multivalued_features['manner']['fricative'],
			'voiced': 10,
			'syllabic':0,
			'nasalized':0,
			'peripheral':0,
			'rounded': 0,
			'glottalized':0,
			'lateralized':0,
			'aspirated':0},  
		u'ʐ': {'place': multivalued_features['place']['retroflex'],
			'manner': multivalued_features['manner']['fricative'],
			'voiced': 10,
			'syllabic':0,
			'nasalized':0,
			'peripheral':0,
			'rounded': 0,
			'glottalized':0,
			'lateralized':0,
			'aspirated':0},  
		u'ʑ': {'place': multivalued_features['place']['alveolo-palatal'],
			'manner': multivalued_features['manner']['fricative'],
			'voiced': 10,
			'syllabic':0,
			'nasalized':0,
			'peripheral':0,
			'rounded': 0,
			'glottalized':0,
			'lateralized':0,
			'aspirated':0},  
		u'ʒ': {'place': multivalued_features['place']['post-alveolar'],
			'manner': multivalued_features['manner']['fricative'],
			'voiced': 10,
			'syllabic':0,
			'nasalized':0,
			'peripheral':0,
			'rounded': 0,
			'glottalized':0,
			'lateralized':0,
			'aspirated':0},  
		u'ʔ': {'place': multivalued_features['place']['glottal'],
			'manner': multivalued_features['manner']['plosive'],
			'voiced': 0,
			'syllabic':0,
			'nasalized':0,
			'peripheral':0,
			'rounded': 0,
			'glottalized':10,
			'lateralized':0,
			'aspirated':0},  
		u'ʕ': {'place': multivalued_features['place']['pharyngeal'],
			'manner': multivalued_features['manner']['fricative'],
			'voiced': 10,
			'syllabic':0,
			'nasalized':0,
			'peripheral':0,
			'rounded': 0,
			'glottalized':0,
			'lateralized':0,
			'aspirated':0},  
		u'ʡ': {'place': multivalued_features['place']['epiglottal'],
			'manner': multivalued_features['manner']['plosive'],
			'voiced': 0,
			'syllabic':0,
			'nasalized':0,
			'peripheral':0,
			'rounded': 0,
			'glottalized':0,
			'lateralized':0,
			'aspirated':0},  
		u'ʢ': {'place': multivalued_features['place']['epiglottal'],
			'manner': multivalued_features['manner']['fricative'],
			'voiced': 10,
			'syllabic':0,
			'nasalized':0,
			'peripheral':0,
			'rounded': 0,
			'glottalized':0,
			'lateralized':0,
			'aspirated':0},  
		u'ⱱ': {'place': multivalued_features['place']['labio-dental'],
			'manner': multivalued_features['manner']['tap-flap'],
			'voiced': 10,
			'syllabic':0,
			'nasalized':0,
			'peripheral':20,
			'rounded': 0,
			'glottalized':0,
			'lateralized':0,
			'aspirated':0},  
		u'β': {'place': multivalued_features['place']['bilabial'],
			'manner': multivalued_features['manner']['fricative'],
			'voiced': 10,
			'syllabic':0,
			'nasalized':0,
			'peripheral':20,
			'rounded': 0,
			'glottalized':0,
			'lateralized':0,
			'aspirated':0},  
		u'θ': {'place': multivalued_features['place']['dental'],
			'manner': multivalued_features['manner']['fricative'],
			'voiced': 0,
			'syllabic':0,
			'nasalized':0,
			'peripheral':0,
			'rounded': 0,
			'glottalized':0,
			'lateralized':0,
			'aspirated':0},  
		u'χ': {'place': multivalued_features['place']['uvular'],
			'manner': multivalued_features['manner']['fricative'],
			'voiced': 0,
			'syllabic':0,
			'nasalized':0,
			'peripheral':20,
			'rounded': 0,
			'glottalized':0,
			'lateralized':0,
			'aspirated':0},  
		u'ћ': {'place': multivalued_features['place']['pharyngeal'],
			'manner': multivalued_features['manner']['fricative'],
			'voiced': 0,
			'syllabic':0,
			'nasalized':0,
			'peripheral':0,
			'rounded': 0,
			'glottalized':0,
			'lateralized':0,
			'aspirated':0},  
		u'я': {'place': multivalued_features['place']['epiglottal'],
			'manner': multivalued_features['manner']['trill'],
			'voiced': 10,
			'syllabic':0,
			'nasalized':0,
			'peripheral':0,
			'rounded': 0,
			'glottalized':0,
			'lateralized':0,
			'aspirated':0}
	}


class MatrixRow(object):
    def __init__(self, row_length):
        self.row = [ 0 for i in range(row_length)]
        
    def __getitem__(self, index):
        if index < 0:
            return -Infinity
        else:
            return self.row[index]
        
    def __setitem__(self, index, value):
        if index >= 0:
            self.row[index] = value

    def __iter__(self):
        for y in self.row:
            yield y

class Matrix(object):
    def __init__(self, x, y, output_type=None):
        self.rows = []
        self.x_length = x
        self.y_length = y
        for i in range(self.x_length):
            self.rows.append(MatrixRow(self.y_length))
        self.output_type = output_type
            
    def __repr__(self):
        out_string = ""
        if self.output_type == 'r':
            out_string = "c("
        for x in range(self.x_length):
            for y in range(self.y_length):
                if self.output_type == 'r':
                    out_string += "%s, " % self.rows[x][y]
                else:
                    out_string += "%s " % self.rows[x][y]
            out_string = out_string.strip() + "\n"
        out_string = out_string.strip()
        if self.output_type == 'r':
            out_string = re.sub(',$', ')', out_string)
        return out_string

    def __getitem__(self, index):
        if index < 0:
            return [ -Infinity for y in range(self.y_length) ]
        else:
            return self.rows[index]

    def __iter__(self):
        for x in self.rows:
            yield x

    def getX(self):
        return self.x_length

    def getY(self):
        return self.y_length

class AlineMatrix(Matrix):
    def __init__(self, x, y, output_type=None):
        self.x = x
        self.y = y
        super(AlineMatrix, self).__init__(len(x) + 1, len(y) + 1, output_type)

    def __repr__(self):
        out_string = super(AlineMatrix, self).__repr__()
        out_parts = out_string.split("\n")
        # how much space we need for each letter on the y axis
        space = max([len(x['input_string']) for x in self.y]) + 1 

        top_line = " " * (space + 2)
        for i in self.x:
            top_line += i['input_string'] + " "
        new_array = [top_line]
        index = 0

        for j in out_parts:
            if index > 0:
                j = self.y[index]['input_string'] + " " * (space - (len(self.y[index]['input_string']))) + j 
            else:
                j = " " * space + j
            new_array.append(j)
            index += 1
        new_out_string = "\n".join(new_array)
        return new_out_string 